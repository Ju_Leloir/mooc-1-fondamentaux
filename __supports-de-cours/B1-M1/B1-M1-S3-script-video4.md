# MOOC NSI - fondamentaux. 
## Transcription de la vidéo

## 1.1.3.4 : Nombres signés

Dans les vidéos précédentes, nous avons vu qu'on peut représenter un nombre en base 2.

Si ce nombre est un nombre entier positif, nous obtenons une représentation binaire, car on n'utilise que les symboles 0 et 1.

Si le nombre est négatif, par contre, il faut trouver un moyen de se défaire du signe "-" pour avoir une représentation binaire.

Dans cette vidéo, nous allons étudier le principe du complément à deux qui nous servira à représenter des nombres négatifs.
 
Voici le principe général, pour les nombres entiers:

- si nous avons un nombre positif, nous le représentons en base 2

- si nous avons un nombre négatif, 
  nous commençons par calculer son opposé (on laisse donc tomber le signe moins)
  puis nous exprimons cet opposé en binaire
  puis nous calculons le complément à deux de cette représentation binaire.

On voit donc que le calcul du complément à deux, que nous allons bientôt expliquer, remplace l'ajout d'un signe - pour indiquer que le nombre est négatif.

Le complément à deux, quant à lui, est simple à calculer:

- on inverse tous les bits de la représentation binaire (on remplace les 0 par des 1 et les 1 par des 0)

- puis on ajoute 1.

Considérons maintenant un exemple. Supposons qu'on veuille représenter les nombres 42 et -42, sur 8 bits.

Pour le nombre 42, qui est positif, on utilise la représentation en base 2 habituelle:

0010 1010

Pour -42, qui est négatif, on part de la représentation en base 2 de 42

On en inverse tous les bits

On y ajoute 1

Et on obtient: 1101 0110

Sur cet exemple, on peut voir une propriété importante de cette représentation: le bit de poids fort vaut 0 quand le nombre est positif, et 1 quand il est négatif. On dit qu'il se comporte comme un bit de signe.

Voyons maintenant un autre avantage de cette représentation: on peut continuer à effectuer des additions de manière habituelle, le résultat restera correct.

Calculons par exemple 14 - 42. Cela revient à faire la somme de 14 et de -42. 

On procède colonne par colonne comme d'habitude, et on obtient:
1110 0100

Comme le bit de poids fort est à 1, on a un résultat négatif. 

Inversons le complément à 2 pour savoir de quel nombre il s'agit: 
On commence par soustraire 1
Puis on inverse tous les bits

On obtient 
0001 1100
qui est la représentation de 28.

Le résultat est donc bien le complément à deux de 28, c'est-à-dire -28. 

En utilisant le complément à deux pour les nombres négatifs, on a associé toute une série de représentations binaires à des nombres négatifs, alors qu'on les utilisait avant pour des nombres positifs. Voyons cela en détail...

Si on a des nombres sur 8 bits, on 2^8 = 256 possibilités.

Si on décide de ne manipuler que des nombres positifs, on représente donc tous les nombres de 0 à 255. Dans ce cas on parle de représentation "non-signée"

Avec une représentation signée en complément à deux, tous les nombres dont le bit de poids fort vaut 1 sont maintenant des nombres négatifs. On peut donc représenter tous les nombres de 0 à 127, mais aussi ceux de -128 à -1. Au total, on a toujours 256 valeurs différentes qui sont représentées.

On peut alors se demander comment savoir si une représentation binaire qui commence par 1 doit être interprétée comme un grand nombre positif ou comme un nombre négatif ? 

Malheureusement, il n'y a rien dans la représentation binaire qui nous permet de le savoir. C'est le programme qui a la  responsabilité d'interpréter les données correctement. Certaines langages de programmation (comme le C ou la Java) ont d'ailleurs des types spéciaux pour indiquer si les données sont signées ou pas. En Python, c'est automatique !
