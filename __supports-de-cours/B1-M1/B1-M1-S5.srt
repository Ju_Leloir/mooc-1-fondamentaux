1
00:00:00,554 --> 00:00:01,738
Dans cette vidéo,

2
00:00:01,838 --> 00:00:02,779
nous allons nous intéresser

3
00:00:02,879 --> 00:00:05,297
à la représentation des contenus multimédias,

4
00:00:05,637 --> 00:00:08,060
les images, le son et les vidéos.

5
00:00:08,535 --> 00:00:09,568
Le principe général

6
00:00:09,668 --> 00:00:11,297
sera toujours le même qu'avec les caractères : 

7
00:00:11,397 --> 00:00:12,872
représenter les éléments de l'image,

8
00:00:12,972 --> 00:00:14,128
du son ou de la vidéo

9
00:00:14,228 --> 00:00:15,999
à l'aide de nombres binaires.

10
00:00:16,411 --> 00:00:19,248
Commençons par les images bitmap.

11
00:00:19,670 --> 00:00:21,010
Cette famille d'images

12
00:00:21,110 --> 00:00:22,097
est celle qui est utilisée

13
00:00:22,197 --> 00:00:23,156
quand on fait des photos

14
00:00:23,256 --> 00:00:24,571
à l'aide de son téléphone mobile

15
00:00:24,671 --> 00:00:26,219
ou d'un appareil photo numérique.

16
00:00:26,593 --> 00:00:27,991
Une image bitmap

17
00:00:28,091 --> 00:00:29,765
est constituée d'une grille d'éléments

18
00:00:29,865 --> 00:00:31,291
qu'on appelle des pixels.

19
00:00:31,391 --> 00:00:33,172
Il s'agit de l'abréviation de l'anglais

20
00:00:33,272 --> 00:00:34,376
picture elements

21
00:00:34,476 --> 00:00:35,720
c'est-à-dire éléments d'image.

22
00:00:36,075 --> 00:00:37,991
Chaque pixel est un point de l'image

23
00:00:38,091 --> 00:00:39,862
et il possède une couleur unique.

24
00:00:40,940 --> 00:00:41,993
Naturellement,

25
00:00:42,093 --> 00:00:43,562
plus il y a de pixels dans l'image

26
00:00:43,662 --> 00:00:44,964
et plus l'image est précise.

27
00:00:45,064 --> 00:00:46,022
Le nombre de pixels,

28
00:00:46,122 --> 00:00:47,491
ou de mégapixels,

29
00:00:47,591 --> 00:00:49,476
affichés par un écran de télévision

30
00:00:49,576 --> 00:00:51,280
ou capturés par un appareil photo numérique

31
00:00:51,380 --> 00:00:53,483
est donc un argument de vente important.

32
00:00:54,942 --> 00:00:56,514
Pour exprimer notre image

33
00:00:56,614 --> 00:00:58,057
sous forme d'une suite de nombres binaires,

34
00:00:58,157 --> 00:00:59,911
il faut donc associer un nombre

35
00:01:00,011 --> 00:01:01,547
à la couleur de chaque pixel.

36
00:01:02,027 --> 00:01:03,547
Pour ce faire, on peut utiliser

37
00:01:03,647 --> 00:01:05,241
l'encodage RVB

38
00:01:05,341 --> 00:01:06,741
pour rouge vert bleu

39
00:01:06,841 --> 00:01:08,421
ou RGB en anglais.

40
00:01:08,724 --> 00:01:11,072
Cela consiste à décomposer chaque couleur

41
00:01:11,172 --> 00:01:13,638
en une combinaison de rouge, de vert et de bleu.

42
00:01:13,738 --> 00:01:15,010
Ensuite on exprime en binaire

43
00:01:15,110 --> 00:01:17,791
l'intensité de chacune de ces trois couleurs dans le mélange.

44
00:01:18,060 --> 00:01:20,721
Par exemple, le turquoise en haut des slides

45
00:01:20,821 --> 00:01:24,686
de rouge avec une intensité de 30,6 %,

46
00:01:25,003 --> 00:01:28,268
de vert avec une intensité de 64,7 %

47
00:01:28,368 --> 00:01:31,725
et de bleu avec une intensité de 61,6 %.

48
00:01:32,338 --> 00:01:34,730
On peut exprimer chacun de ces trois pourcentages

49
00:01:34,830 --> 00:01:37,818
sous 8 bits, où 100 % correspond à la valeur maximum

50
00:01:37,918 --> 00:01:39,102
soit tous les bits à 1.

51
00:01:39,426 --> 00:01:41,427
Cela nous donne un nombre sur 3 octets

52
00:01:41,527 --> 00:01:42,650
qui caractérise la couleur.

53
00:01:42,750 --> 00:01:44,078
En hexadécimal, dans notre exemple,

54
00:01:44,178 --> 00:01:46,791
ce serait 4EA59D.

55
00:01:47,203 --> 00:01:49,401
Ensuite, pour représenter toute l'image,

56
00:01:49,501 --> 00:01:51,267
il suffit d'énumérer la suite des valeurs

57
00:01:51,367 --> 00:01:52,820
qui correspond à tous ses pixels.

58
00:01:53,108 --> 00:01:55,102
Notons enfin qu'un fichier image

59
00:01:55,202 --> 00:01:56,610
peut contenir d'autres informations

60
00:01:56,710 --> 00:01:58,686
comme le modèle d'appareil photo utilisé

61
00:01:58,786 --> 00:01:59,950
ou la date de prise de vue.

62
00:02:00,050 --> 00:02:01,658
Ces informations sont en général

63
00:02:01,758 --> 00:02:03,788
appelées les données exif.

64
00:02:04,397 --> 00:02:06,515
Intéressons-nous maintenant à la question du son.

65
00:02:06,615 --> 00:02:08,106
Un son est une onde sonore

66
00:02:08,206 --> 00:02:10,106
comme on en voit un exemple sur le transparent.

67
00:02:10,518 --> 00:02:12,472
Pour en obtenir une représentation informatique,

68
00:02:12,572 --> 00:02:13,886
il faut passer par deux étapes.

69
00:02:13,986 --> 00:02:16,187
La première est l'échantillonnage.

70
00:02:16,287 --> 00:02:18,367
Cela revient à mesurer de manière régulière

71
00:02:18,467 --> 00:02:19,800
l'intensité du signal sonore.

72
00:02:20,086 --> 00:02:22,247
Chaque mesure est appelée un échantillon

73
00:02:22,347 --> 00:02:24,477
et la fréquence à laquelle cet échantillonnage a lieu

74
00:02:24,577 --> 00:02:25,614
est mesurée en hertz.

75
00:02:25,714 --> 00:02:27,284
Par exemple, sur les disques compacts,

76
00:02:27,384 --> 00:02:30,484
on utilise une fréquence de 44,1 kilohertz

77
00:02:30,584 --> 00:02:33,446
ce qui signifie qu'on mesure un échantillon du signal sonore

78
00:02:33,546 --> 00:02:35,069
44 100 fois par seconde.

79
00:02:35,397 --> 00:02:37,380
La seconde étape est la quantification

80
00:02:37,480 --> 00:02:40,071
qui consiste à transformer chaque échantillon en un nombre entier.

81
00:02:40,357 --> 00:02:42,511
Pour ce faire, on fixe une résolution

82
00:02:42,611 --> 00:02:44,083
qui indique sur combien de bits

83
00:02:44,183 --> 00:02:45,779
sera représenté chaque échantillon.

84
00:02:46,139 --> 00:02:48,355
Par exemple, pour les disques compacts à nouveau,

85
00:02:48,455 --> 00:02:50,178
on a des échantillons sur 16 bits.

86
00:02:50,278 --> 00:02:52,847
Cela signifie que l'on peut mesurer 2 exposant 16

87
00:02:52,947 --> 00:02:55,706
soit 65 536 valeurs différentes.

88
00:02:55,933 --> 00:02:58,613
Naturellement, une plus grande fréquence d'échantillonnage

89
00:02:58,713 --> 00:03:00,278
ou une plus grande résolution

90
00:03:00,378 --> 00:03:02,323
permettent d'améliorer la qualité de l'enregistrement.

91
00:03:03,013 --> 00:03:06,080
Enfin, les formats vidéos sont obtenus en règle générale

92
00:03:06,180 --> 00:03:07,725
en combinant des représentations

93
00:03:07,825 --> 00:03:08,888
pour l'image et pour le son

94
00:03:08,988 --> 00:03:10,032
comme on peut s'y attendre.

95
00:03:10,792 --> 00:03:13,406
Notons pour terminer que la quantité de données nécessaires

96
00:03:13,506 --> 00:03:16,957
pour stocker un son, une image ou une vidéo en haute résolution

97
00:03:17,057 --> 00:03:18,687
peut vite devenir très importante.

98
00:03:18,787 --> 00:03:20,794
Cela pose de nombreuses difficultés

99
00:03:20,894 --> 00:03:21,835
en matière de stockage

100
00:03:21,935 --> 00:03:23,145
ou de transmission de ces contenus,

101
00:03:23,245 --> 00:03:24,928
pensez par exemple aux contenus en streaming.

102
00:03:25,254 --> 00:03:27,437
C'est pourquoi les formats utilisés en pratique

103
00:03:27,537 --> 00:03:29,351
mettent en œuvre des techniques de compression

104
00:03:29,451 --> 00:03:31,953
qui permettent de réduire fortement la taille des fichiers

105
00:03:32,053 --> 00:03:33,887
tout en conservant une qualité suffisante.

106
00:03:33,987 --> 00:03:36,267
En effet, la compression peut s'effectuer

107
00:03:36,367 --> 00:03:37,676
sans ou avec perte,

108
00:03:37,776 --> 00:03:39,965
selon que l'on puisse toujours retrouver, ou pas,

109
00:03:40,065 --> 00:03:41,020
les données d'origine.

110
00:03:41,310 --> 00:03:42,554
Par exemple, pour les images,

111
00:03:42,654 --> 00:03:45,095
le format JPEG utilise de la compression.

112
00:03:45,624 --> 00:03:49,061
Pour les fichiers sons, on trouve les formats MP3 ou FLAC.

113
00:03:49,281 --> 00:03:50,508
Enfin, pour la vidéo,

114
00:03:50,608 --> 00:03:52,944
on trouve par exemple le format MPEG.

115
00:03:53,883 --> 00:03:55,274
Tous ces formats précisent

116
00:03:55,374 --> 00:03:57,165
quelles sont les techniques de compression à utiliser.

117
00:03:57,265 --> 00:03:58,806
Ces techniques sont implémentées

118
00:03:58,906 --> 00:04:00,903
dans des programmes qu'on appelle des codecs,

119
00:04:01,003 --> 00:04:02,496
ce qui est l'abréviation de

120
00:04:02,596 --> 00:04:03,878
COder DECodeur.

