\documentclass[a4paper]{scrartcl}
\usepackage{mathptmx}
\usepackage[scaled=.90]{helvet}
\usepackage{courier}

\usepackage[french]{babel}
\usepackage[T1]{fontenc}
%\usepackage{a4wide}
\usepackage[pdftex]{graphicx}
\usepackage{url}
\usepackage{../../foncordi}
%\usepackage{hyperref}
%\usepackage{times}
\usepackage{rotating}
\usepackage{amssymb,amsmath}
%\newcommand{\xor}{\ensuremath{\operatorname{\mathrm{XOR}}}}
\newcommand{\desc}{\ensuremath{\textrm{desc}}}
\newcommand{\pres}{\ensuremath{\textrm{pr�sence}}}

\title{INFO-F-102 -- Fonctionnement des Ordinateurs\\ Le�ons 10 � 12 --
  La micro-architecture}
\author{Gilles Geeraerts}
\date{Ann�e acad�mique 2016--2017}

\begin{document}
\maketitle
Le niveaux qualifi� de <<~micro-architecture~>> est le niveau
interm�diaire entre les circuits �lectroniques digitaux et le langage
machine, qui est l'ensemble d'instructions que le processeur peut
ex�cuter. Les instructions du langage machine ne seront pas
directement traduites chacune en terme de circuits logiques (ce serait
trop co�teux, et cela entra�nerait une d�multiplication des
circuits). Chaque instruction du langage machine va �tre
\emph{interpr�t�} et traduite en une s�quence (courte) de
\emph{micro-instructions} tr�s simples. Nous allons maintenant �tudier
un exemple de machine capable d'ex�cuter un tel jeu de
micro-instructions. Il sera facile de se convaincre:
\begin{itemize}
\item d'une part que cette machine peut ais�ment �tre r�alis�e �
  l'aide des circuits digitaux �tudi�s pr�c�demment.
\item d'autre par que ces micro instructions sont suffisantes pour
  ex�cuter des instructions machine plus complexes.
\end{itemize}

L'exemple que nous allons �tudier est totalement fictif, mais
parfaitement r�aliste.

\section{Exemple de langage machine: l'IJVM}
Il s'agit d'un langage qui est ex�cut� sur une machine � pile. Cela
signifie que le processeur n'a pas de registre \emph{de travail}, mais
utilise un \textit{stack} en m�moire centrale, sur laquelle il faut
\emph{pusher} les donn�es � traiter.

La m�moire de la machine est organis�e en plusieurs zones dont:
\begin{itemize}
\item Une zone organis�e comme un \textit{stack} pour les op�randes.
\item Une zone pour les m�thodes (=code en langage machine).
\end{itemize}

% Pour acc�der � ces zones, l'IJVM utilise des registres (rem: ce ne
% sont pas des registres de travail !):
% \begin{itemize}
% \item CPP pour les constantes (d�but de la zone).
% \item LV pour le d�but de la zone des variables locales.
% \item SP pour la \emph{fin} de la zone de \textit{stack} d'op�randes.
% \item PC qui pointe vers la prochaine instruction � utiliser
% \end{itemize}
% Toutes les zones sont organis�es en mots de 32 bits, sauf la zone des
% m�thodes, qui est organis�e au mots d'un octet. Les adresses dans CPP
% LV et SP sont donc un num�ro de mot 32 bits, alors que PC est un
% num�ro de mot 8 bit.

Les instructions de la machine sont donn�es � la Fig 4--11. Nous
allons �tudier en particulier les instructions suivantes:
\begin{itemize}
\item \texttt{NOP}. Cette instruction ne fait rien. Il est parfois
  utile de disposer d'une telle instruction, comme nous le verrons par
  la suite.
\item \texttt{BIPUSH} $v$, o� $v$ est une valeur sur un
  octet. \textit{Push} cette valeur au sommet de la pile.
\item \texttt{POP}. Cette instruction sans param�tre \emph{pop} le
  sommet de la pile.
\item \texttt{IADD}, \texttt{ISUB}. Ces instructions sans param�tre
  r�alisent respectivement la somme et la diff�rence des deux valeurs
  au sommet de la pile, \emph{pop} ces deux valeurs, et \emph{push} le
  r�sultat � leur place.
\item \texttt{GOTO} $o$. Cette instruction r�alise un saut de $o$
  positions dans le code. Le d�calage $o$ est une valeur sur 2 octets.
\item \texttt{IFEQ} $o$. Cette instruction \textit{pop} la valeur au
  sommet de la pile et r�alise un saut de $o$ positions dans le code
  si et seulement si cette valeur vaut $0$. Le d�calage $o$ est une
  valeur sur 2 octets.
\end{itemize}

\paragraph{Remarque} Pour plus de clart� dans ce document, on n'�crira
pas explicitement le d�calage � effectuer lors d'un \texttt{GOTO} ou
d'un \texttt{IFEQ}, mais on utilisera des \emph{�tiquettes}. Cela
consiste � placer, au d�but d'une ligne de programme, un nom
d'�tiquette suivi de <<~\verb-:-~>>, par exemple:
\begin{verbatim}
etiqu:  IADD
\end{verbatim}
Ce nom doit �tre unique et identifie donc sans ambigu�t� cette ligne.

On pourra alors, si on veut faire un saut vers cette ligne, remplacer
le d�calage par le nom de l'�tiquette:
\begin{verbatim}
etiqu: IADD
       ...
       GOTO etiqu
\end{verbatim}

Il est facile d'�crire un petit programme qui collecte les positions
de toutes les �tiquettes et remplace leurs noms par les bons d�calages
dans les instructions \texttt{GOTO} et \texttt{IFEQ}.

\subsection{Exemples de programmes en IJVM}
Ce premier exemple \textit{push} trois valeurs sur le \textit{stack} et
r�alise leur somme.
\begin{verbatim}
BIPUSH 10
BIPSUH 20
BIPUSH 12
IADD
IADD
\end{verbatim}

Ce second exemple \textit{pop} les deux valeurs au sommet du
\textit{stack}, et \textit{push} la valeur $1$ si ces deux valeurs
sont �gales, $0$ sinon:
\begin{verbatim}
     SUB
     IFEQ equ
     BIPUSH 0
     GOTO fin
equ: BIPUSH 1
fin: NOP
\end{verbatim}

\subsection{Repr�sentation d'un programme IJVM en m�moire}
Concr�tement, les instructions IJVM seront stock�es en m�moire sous
forme binaire. Afin d'identifier les instructions, nous allons
associer un code num�rique unique, sur un octet, � chaque
instruction. Ce code est appel� \emph{code d'op�ration}, ou, selon
l'acronyme anglais, \emph{opcode}. La liste des \emph{opcodes} est
donn�e � la Fig. 4--11. Par exemple, l'\emph{opcode} de
\texttt{BIPUSH} est $10_{16}$. Si l'instruction a besoin d'une
op�rande, celle-ci sera plac�e apr�s l'\emph{opcode}.

\paragraph{Exemple} Notre premier programme d'exemple sera stock�
ainsi en m�moire (toutes les valeurs sont en hexad�cimal -- les
espaces servent � identifier les instructions):
\begin{verbatim}
10 0A   10 14   10 0C   60   60  
\end{verbatim}

Le seconde programme d'exemple est stock�e de la fa�on suivante:
\begin{verbatim}
64   99 00 08    10 01    A7 00 05   10 00   00
\end{verbatim}
% rem: bug dans l'assembleur, on a un push de ff au lieu de 00
Remarque: les d�calages du \verb-GOTO- et \verb-IFEQ- sont �
comprendre par rapport � l'adresse de l'\textit{opcode}, comme on le
voit sur cet exemple.

Nous allons maintenant �tudier la machine qui devra ex�cuter ces
instructions machine.


\section{Exemple de \textit{datapath}: le MIC 1}
Le c\oe ur de la machine qui va ex�cuter les micro-instructions est le
\emph{datapath} qui se compose (voir Fig. 4--1):
\begin{itemize}
\item D'un ensemble de registres: MAR, MDR, PC, MBR, SP, LV, CPP, TOS,
  OPC et H. Tous les registres ont 32 bits sauf le registre MBR qui
  fait 8 bits. L'utilit� de ces registres sera expliqu�e plus loin.
\item D'une ALU qui a plusieurs entr�es:
  \begin{itemize}
  \item Les valeurs A et B sur lesquelles effectuer les op�rations.
  \item Une sortie de la valeur calcul�e.
  \item Deux sorties additionnelles N et Z.
  \item 6 bits pour sp�cifier l'op�ration � effectuer.
  \end{itemize}
  Les op�rations dont l'ALU est capable sont d�taill�es � la
  Fig. 4--2.
\item D'un circuit de d�calage, qui re�oit deux bits en entr�e pour
  contr�ler le d�calage. Ce circuit est connect� � la sortie de
  l'ALU. Il est capable d'effectuer 3 op�rations diff�rentes:
  \begin{itemize}
  \item Ne pas modifier la valeur pass�e en entr�e, et la recopier
    telle quelle sur sa sortie.
  \item D�caler l'entr�e d'un octet vers la gauche en remplissant les
    bits <<~perdus~>> (ceux de poids faible) par des 0 (op�ration
    SLL8: \textit{Shift Left Logical 8 bits})
  \item D�caler l'entr�e d'un bit vers la droite en gardant le bit le
    plus significatif inchang�  (op�rations SRA1: \textit{Shift
      Arithmetic Right 1 bit})
  \end{itemize}
\item 4 bus:
  \begin{itemize}
  \item Le bus A qui connecte le registre H � l'entr�e A de l'ALU. Il
    permet de lire dans ce registre.
  \item Le bus B qui connecte tous les registres sauf H et MAR �
    l'entr�e B de l'ALU. Il permet de lire dans ces registres.
  \item Le bus C qui connecte la sortie du circuit de d�calage � tous
    les registres. Il permet d'�crire dans les registres
  \item Un petit bus qui connecte la sortie de l'ALU � l'entr�e du
    circuit de d�calage.
  \end{itemize}
\end{itemize}
Par ailleurs, certains registres sont connect�s � la m�moire centrale
de la machine \textit{via} un bus suppl�mentaire: ce sont les
registres MAR, MDR, PC et MBR.

\subsection{Cycle d'ex�cution}
\textbf{Un cycle d'ex�cution du \emph{data path} correspond � une
  micro-instruction.}

Un cycle dans ce \emph{data path} peut �tre d�compos� en trois
parties:
\begin{itemize}
\item Un registre qui peut �mettre sur le bus B est activ� et �met son
  contenu sur le bus.
\item L'ALU puis le circuit de d�calage ex�cutent les op�rations
  demand�es.
\item La valeur calcul�e par l'ALU et le circuit de d�calage est
  �mise sur le bus C et est inscrite dans certains registres qui ont
  �t� s�lectionn�s au d�but du cycle. Il est donc possible d'�crire
  dans plusieurs registres en un cycle, et m�me d'�crire dans le
  registre qui a servi � l'�mission sur le bus B.
\end{itemize}
Concr�tement, tout cela est r�gi par les pulsations de l'horloge. Un
cycle commence lors du flanc descendant d'une pulsation et se termine
avec l'�criture dans les registres <<~de sortie~>> lors du flanc
montant suivant. Voir Fig. 4--3 du livre.

\subsection{Communication avec la m�moire}
Par ailleurs, lors de chaque cycle, il est �galement possible de
commander une lecture ou une �criture dans la m�moire. Pour lire et
�crire des donn�es dans la m�moire, la machine dispose de deux
possibilit�s:
\begin{enumerate}
\item Soit elle utilise le registre MAR pour sp�cifier l'adresse d'un
  \emph{mot de 32 bits}, et utilise le registre MDR pour sp�cifier la
  donn�e � y �crire (op�ration \textit{wr}), ou pour recevoir la
  donn�e lue (op�ration \textit{rd}).
\item Soit elle utilise le registre PC pour sp�cifier l'adresse d'un
  \emph{mot de 8 bits}, dont le contenu est �crit dans MBR. Cette
  op�ration est appel�e \textit{fetch}. Il n'est pas possible d'�crire
  � travers ce m�canisme.
\end{enumerate}
\textit{Grosso modo} le port 32 bits MAR/MDR servira � manipuler des
donn�es, alors que le port 8 bits PC/MBR servira � lire le programme
machine (\textit{opcodes} + op�randes).

% \textbf{Attention !} Il faut bien diff�rencier les deux modes
% d'adressages utilis�s. Une m�me adresse dans PC et dans MAR ne
% correspond pas � un m�me emplacement m�moire !  Remarquons que la
% m�moire est \textit{in fine} une m�moire contenant des mots d'un
% octet. Il faut donc, � un certain moment, convertir les adresses de
% MAR en \emph{adresses physiques}, ce qui se fait ais�ment en
% multipliant les adresses par 4 (ou en d�calant de 2 bits vers la
% gauche).

\paragraph{D�lai m�moire} Malheureusement, les lectures et �critures
en m�moire \emph{ne sont pas imm�diates} (cette hypoth�se est
r�aliste: sur une vraie machine, un cycle dans le \textit{datapath}
est bien plus court que le temps de r�action de la m�moire).  Comme le
contenu de MAR ou PC n'est inscrit dans les registres qu'\emph{en fin
  de cycle}, on ne peut pas esp�rer voir l'effet d'une
lecture/�criture m�moire r�alis� \emph{au d�but du cycle suivant}, car
il faut laisser � la m�moire le temps de r�agir. L'effet n'aura lieu
\emph{qu'� la fin du cycle suivant}, et donc les donn�es ne seront
exploitables que \emph{deux cycles plus tard}.

Par exemple, supposons qu'on d�cide de faire une lecture via MAR/MDR
(et qu'on dispose d�j� de l'adresse � laquelle lire dans un autre
registre):
\begin{enumerate}
\item Cycle 1: durant ce cycle, le signal \textit{rd} est �mis �
  destination de la m�moire, et le cycle se d�roule ainsi:
  \begin{enumerate}
  \item On s�lectionne un registre qui contient l'adresse � lire
  \item On passe le contenu de ce registre � l'ALU et au circuit de
    d�calage via le bus B, qui ne le modifient pas.
  \item On pr�l�ve l'information sur le bus C et on la stocke dans
    MAR.
  \end{enumerate}
\item Cycle 2: deux choses se passent en parall�le:
  \begin{itemize}
  \item La m�moire effectue le lecture demand�e et
  \item Une autre op�ration est effectu�e dans le
    \textit{datapath}. Celle-ci ne peut donc pas utiliser la valeur
    lue au cycle pr�c�dent, car elle n'en dispose pas encore, mais
    elle peut modifier MAR, car l'adresse a d�j� �t� envoy�e � la
    m�moire.
  \end{itemize}
  \`A la fin du cycle, le contenu de la cellule m�moire MAR appara�t
  dans MDR, au moment o� l'on �crit le contenu du bus C dans les
  registres.
\item Cycle 3: au d�but de ce cycle, la donn�e est disponible dans
  MDR, on peut donc s'en servir durant ce cycle.
\end{enumerate}
Si on demande une lecture en m�moire au cycle $k$, on dispose donc de
l'information au cycle $k+2$ (\textit{idem} si on utilise PC/MBR)

% \paragraph{Conversion MBR vers 32 bits} Que se passe-t-il quand le
% registre MBR (qui ne fait qu'un octet) �met son contenu sur le bus B ?
% Comme le bus fait 32 bits, il faut bien compl�ter les 24 bits
% manquants. Pour ce faire, on a deux possibilit�s:
% \begin{enumerate}
% \item Soit on voit la valeur d'un octet comme un entier non-sign�, et
%   on compl�te les 24 bits manquants par des 0.
% \item Soit on voit la valeur d'un octet comme un entier sign� entre
%   $-128$ et $127$ (il s'agit d'une repr�sentation par exc�s � 127),
%   dans ce cas, il faut compl�ter les 24 bits restants:
%   \begin{itemize}
%   \item soit par des $0$ si le bit de poids fort de MBR est $0$.
%   \item soit par des $1$ si le bit de poids fort de MBR est $1$.
%   \end{itemize}
% \end{enumerate}
% Cette technique assure que l'interpr�tation (sign�e ou non) de MBR est
% conserv�e quand on le transforme en une valeur sur 32 bits. Quand on
% activera le registre MBR pour �mission sur le bus B, on pourra choisir
% chacun des deux possibilit�s (c'est pourquoi il y a deux fl�ches
% d'activation).

\subsection{Micro-instructions}
Une \emph{micro-instruction} correspond � l'ex�cution d'un cycle du
\textit{datapath}. \'Etant donn� la discussion qui pr�c�de, il est
clair que le comportement de la machine � chaque cycle sera r�gi par
les param�tres suivants:
\begin{itemize}
\item Quel registre sera s�lectionn� pour �crire sur le bus B ?
\item Quels registres recevront la valeur �mise sur le bus C ? 
\item Quelle op�ration l'ALU et le circuit de d�calage
  effectueront ils ? 
\item Y aura-t-il une communication avec la m�moire \textit{via} le
  port MAR/MDR ou \textit{via} le port PC/MBR ?
\item Quelle micro-instruction sera ex�cut�e au cycle suivant ?
\end{itemize}

En r�pondant � ces cinq questions, on sp�cifie compl�tement une
micro-instruction. En pratique, on utilisera la syntaxe suivante pour
les micro-instructions:
\begin{center}
  op�ration registres~;~m�moire~;~saut
\end{center}
o�:
\begin{itemize}
\item <<~op�ration registres~>> est une op�ration sur les registres
  qui peut �tre r�alis�e en un cycle. Par exemple: $\mathtt{MAR} =
  \mathtt{SP} = \mathtt{SP}-1$. Cette partie � pour effet de calculer
  $\mathtt{SP}-1$ (gr�ce � l'ALU) et de placer le r�sultat dans
  \texttt{SP} et dans \texttt{MAR}. C'est bien possible en un cycle.
\item <<~m�moire~>> est une op�ration sur la m�moire:
  \begin{itemize}
  \item \texttt{rd}: lance une lecture en m�moire de la cellule dont
    l'adresse est dans \texttt{MAR}. Cette valeur sera plac�e dans
    \texttt{MDR} au bout d'un cycle.
  \item \texttt{wr}: lance une �criture en m�moire, de la valeur
    contenue dans \texttt{MDR} � la cellule dont l'adresse est dans
    \texttt{MAR}. Cette valeur sera �crite au bout
    d'un cycle.
  \item \texttt{fetch}: lance une lecture en m�moire de la cellule
    dont l'adresse est dans \texttt{PC}. Cette valeur sera plac�e
    dans \texttt{MBR} au bout d'un cycle.
  \end{itemize}
\item <<~saut~>> indique quelle est la prochaine micro-instruction �
  r�aliser. Si rien n'est indiqu�, on passe � la suivante. Sinon, on
  peut utiliser \texttt{goto} ou \texttt{if() goto\ldots; else
    goto\ldots}
\end{itemize}

Concr�tement, les micro-instructions sont repr�sent�es de fa�on
binaire et un composant de la machine, appel�e \emph{s�quenceur} se
charge du flot d'ex�cution des micro-instructions. Nous expliquerons cela plus tard.


\section{R�alisation de l'IJVM � l'aide du MIC1}
Maintenant que nous connaissons le langage machine et le micro-langage,
�crivons le code qui permet de traduire l'un vers l'autre.

\subsection{Interpr�teur}
Nous souhaitons disposer d'un \emph{interpr�teur} en
micro-instructions. Ce programme va continuellement r�p�ter une boucle
qui va:
\begin{enumerate}
\item Lire la prochaine \emph{instruction machine}
\item La \emph{d�coder}
\item Ex�cuter une s�quence de \emph{micro-instructions} qui va
  \emph{r�aliser} l'effet de l'instruction machine.
\end{enumerate}

Pour ce faire, la machine doit pouvoir acc�der aux instructions en
m�moire et les analyser. Elle utilise deux registres du
\textit{datapath}:
\begin{itemize}
\item PC, le pointeur de programme, qui pointe vers la prochaine
  instruction � ex�cuter.
\item MBR, qui est utilis� pour obtenir un octet de donn�es en m�moire
  (et donc, en particulier, l'\textit{opcode} d'une instruction
  machine).
\end{itemize}

On supposera que, initialement (� l'allumage de la machine), le
registre PC est initialis� vers la premi�re instruction du programme,
et le registre MBR est charg� avec cette premi�re
instruction. Ensuite, l'interpr�teur en micro-code commence �
s'ex�cuter. La premi�re micro-instruction en est: <<~\texttt{PC=PC+1;
  fetch; goto(MBR)}~>>, ce qui a pour effet:
\begin{enumerate}
\item d'incr�menter PC pour pointer vers l'instruction machine
  suivante.
\item de lancer la lecture m�moire de l'instruction suivante (celle
  point�e par PC). Cette nouvelle instruction ne sera disponible qu'�
  la fin du cycle suivant.
\item de brancher le micro-code vers la s�quences de micro-instructions
  qui vont ex�cuter l'instruction machine en cours (qui est encore
  dans MBR).
\end{enumerate}
\`A la fin de l'ex�cution de l'instruction machine, l'interpr�teur
reviendra vers la premi�re ligne, pour passer � l'instruction machine
suivante (qui sera d�j� dans MBR �tant donn� le \textit{fetch}).

Passons maintenant en revue plusieurs instructions machines et
traduisons les en micro-instructions. Cette description nous permettra
d'introduire les diff�rents registres du \textit{datapath} et leur
utilit� respective.



\subsection{Manipulation du \textit{stack}}
Pour pouvoir acc�der au \textit{stack}, le Mic-1 doit disposer de
l'adresse de son sommet. Le registre SP (pour \textit{stack pointer})
sert � cela. Par ailleurs, on s'arrangera pour toujours garder dans le
registre TOS (\textit{top of stack}) la valeur qui est au sommet du
\textit{stack}, et ceci afin de permettre un acc�s plus rapide au
\textit{stack}. Ainsi, si on souhaite simplement consulter ce sommet,
il n'est pas n�cessaire de faire une lecture en m�moire. Si on
souhaite acc�der aux deux valeurs au sommet, une seule lecture m�moire
est n�cessaire, \textit{etc}.

\subsection{Op�rations \texttt{POP} et \texttt{BIPUSH}}
\paragraph{\texttt{POP}} Pour effectuer un \textit{pop}, il faut
d�cr�menter le pointeur de \textit{stack} et mettre � jour
TOS. Remarquons que l'ancien sommet est toujours pr�sent en m�moire,
mais ne fait plus partie du \textit{stack}.
\begin{itemize}
\item \texttt{MAR = SP = SP-1; rd}. Cette micro-instruction d�cr�mente le
  pointeur de \textit{stack}, place l'adresse du nouveau sommet dans
  MAR et lance une lecture.
\item Un cycle <<~vide~>> est n�cessaire, pour laisser le temps � la
  m�moire de r�pondre la valeur � placer dans TOS.
\item \texttt{TOS = MDR; goto Main1}. Cette micro-instruction inscrit
  dans TOS la valeur r�pondue par la m�moire (et donc plac�e dans
  MDR), puis revient au d�but de l'interpr�teur.
\end{itemize}

\paragraph{\texttt{BIPUSH}} Pour effectuer un \textit{push}, il faut
r�cup�rer en m�moire la valeur � placer sur le \textit{stack},
incr�menter le pointeur de \textit{stack}, et placer la valeur � cette
nouvelle adresse, ainsi que dans TOS.

Pour rappel, une instruction \texttt{BIPUSH} tient sur deux octets en
m�moire: le premier contient l'\textit{opcode} $10_{16}$ et le seconde
la valeur � placer sur le \textit{stack}. Cela signifie qu'au moment
o� l'on commence � ex�cuter les micro-instructions qui vont traduire le
\texttt{BIPUSH}, la valeur $10_{16}$ se trouve dans MBR, mais PC
pointe d�j� vers la case m�moire contenant l'op�rande, et la lecture
de cette op�rande est en cours. Elle ne sera disponible dans MBR qu'au
d�but du second cycle. Il faudra donc penser � incr�menter PC de fa�on
� ce qu'il pointe vers l'\textit{opcode} de instruction machine qui
suit le \texttt{BIPUSH}, et � charger cet \textit{opcode} dans MBR.
\begin{itemize}
\item \texttt{SP = MAR = SP+1}. Cette micro-instruction calcule la
  nouvelle adresse de sommet du \textit{stack}, et la place dans SP et
  dans MAR.
\item \texttt{PC = PC+1; fetch}. Lance la lecture de l'instruction
  machine suivante.
\item \texttt{MDR = TOS = MBR; wr; goto Main1}. La lecture lanc�e � la
  micro-instruction pr�c�dente n'est pas encore effectu�e, et MBR
  contient donc encore la valeur de l'op�rande au d�but du
  cycle. C'est donc la valeur de l'op�rande qui est copi�e dans TOS
  (ce sera la nouvelle valeur au sommet du \textit{stack}) et dans
  MDR. L'�criture en m�moire est lanc�e (l'adresse d'�criture avait
  �t� mise dans MAR lors de la premi�re micro-instruction). \`A la fin
  du cycle la lecture en m�moire lanc�e � la micro-instruction
  pr�c�dente est termin�e, et l'\textit{opcode} de la prochaine
  instruction est donc bien dans MBR au moment o� l'on revient au
  d�but de l'interpr�teur.
\end{itemize}

\subsection{Op�rations \texttt{IADD} et \texttt{ISUB}}
\paragraph{\texttt{IADD}} Comme, � tout moment, la valeur au sommet du
\textit{stack} est d�j� pr�sente dans TOS, on n'a donc besoin de ne
faire qu'un seul \textit{read} pour la p�nulti�me valeur du
\textit{stack}.

\begin{itemize}
\item \label{item1} \texttt{MAR = SP = SP-1; rd}. Cette instruction
  d�cr�mente SP et place la valeur calcul�e dans MAR. Ensuite, un
  \textit{read} est initi� (de la valeur � l'adresse SP-1, donc). Il
  s'agit donc de lire la p�nulti�me valeur du \textit{stack}, et de la
  placer dans MDR. Par ailleurs, le pointeur de \textit{stack} est maintenant
  sur la case o� l'on d�sire stocker le r�sultat (celle qui contenait
  -- et contient encore -- la seconde op�rande).
\item \texttt{H=TOS}. On place le sommet du \textit{stack} dans le
  registre H en vue de l'addition avec la valeur lue
  pr�c�demment. Remarquons que cette instruction aurait pu �tre plac�e
  plus t�t, mais elle agit comme un <<~tampon~>> pendant laquelle le
  \textit{read} ordonn� au point~\ref{item1} est ex�cut� par la
  m�moire.
\item \texttt{MDR=TOS=MDR+H; wr; goto Main1}. Cette instruction
  calcule la somme demand�e � l'aide de la valeur plac�e dans MDR,
  maintenant disponible. Cette somme est plac�e dans TOS, ce qui garde
  ce registre coh�rent, et �galement dans MDR. Ensuite, une �criture
  en m�moire est ordonn�e. Le r�sultat sera inscrit dans la case �
  l'adresse MAR, qui est toujours, depuis le point~\ref{item1},
  l'adresse de la case contenant la seconde op�rande. Le <<~goto
  Main1~>> permet de passer � l'instruction suivante.
\end{itemize}

\paragraph{\texttt{ISUB}} M�me principe, seule la commande � l'ALU
dans la derni�re micro-instruction change.

\subsection{Op�rations de saut: \texttt{GOTO} et \texttt{IFEQ}}
\paragraph{\texttt{GOTO}} Contrairement aux op�rations pr�c�dentes,
cette op�ration ne s'occupe pas du \textit{stack}. Quand on effectue
un <<~\texttt{GOTO} $o$~>>, on d�sire que PC prenne la valeur $a+o$ o�
$a$ est l'adresse m�moire de l'instruction <<~\texttt{GOTO}
$o$~>>. Remarquez qu'au moment o� on entre dans la partie de
l'interpr�teur qui s'occupe du \texttt{GOTO}, PC contient la valeur
$a+1$, �tant donn� que PC est syst�matiquement incr�ment� � la ligne
\textsf{Main1}. 

Par ailleurs, l'instruction \texttt{GOTO} $o$ tient en m�moire sur 3
octets (donc aux adresses $a$, $a+1$ et $a+2$), car le d�calage $o$
tient sur 2 octets. Or, la lecture dans la partie <<~instructions~>>
de la m�moire se fait au travers du registre MBR qui ne peut lire
qu'un octet � la fois. Il faudra donc lire les deux octets $o_1$ et
$o_2$ composant la valeur $o$ s�par�ment, et recomposer la valeur $o$,
en faisant un shift gauche de 8 bits de $o_1$ et en faisant un
\texttt{OR} bit � bit entre cette valeur et $o_2$:
\begin{center}
  \begin{tabular}{rc|c|c|}
    $o$&=&\phantom{aaaa}$o_1$\phantom{aaaa}&\phantom{aaaa}$o_2$\phantom{aaaa}\\ \cline{3-4}
     &\multicolumn{1}{c}{\ }&\multicolumn{1}{c}{8 bits}&\multicolumn{1}{c}{8 bits}
  \end{tabular}
\end{center}



\'Etant donn� l'op�ration effectu�e � la ligne \textsf{Main1}, on voit
que, lorsque on entre dans le micro-code qui ex�cute le \texttt{GOTO},
MBR contient encore l'\textit{opcode} du \texttt{GOTO}, mais la
lecture de l'octet $o_1$ est en cours. Cette valeur sera disponible
dans MBR au deuxi�me cycle.
\begin{itemize}
\item \texttt{OPC = PC-1}. Cette micro-instruction sauve dans OPC
  l'adresse de l'instruction \texttt{GOTO} que l'on s'appr�te �
  ex�cuter (la valeur $a$ dans la discussion ci-dessus).
\item \texttt{PC = PC+1; fetch}. Au moment d'ex�cuter cette
  micro-instruction, la valeur $o_1$ est dans MBR (en raison du
  \textit{fetch} de la ligne \textsf{Main1}). On incr�mente PC pour
  qu'il pointe vers la valeur $o_2$ et on lance une lecture.
\item \texttt{H = MBR $< <$ 8}. On d�cale la valeur $o_1$ (qui est
  encore dans MBR) de 8 bits vers la gauche et on place le r�sultat
  dans H.
\item \texttt{H = MBRU OR H}. On combine la valeur $o_1$ d�cal� de 8
  bits (qui est dans H) avec la valeur $o_2$ (qui est maintenant dans
  MBR) � l'aide d'un \texttt{OR}. On utilise MBRU eu lieu de MBR pour
  compl�ter correctement la valeur de MBR sur 32 bits (avec des
  $0$). Le d�calage du saut est maintenant dans H.
\item \texttt{PC = OPC + H; fetch}. On calcule la nouvelle valeur de PC
  � l'aide de l'adresse du \texttt{GOTO} (stock�e dans OPC) et du
  d�calage (calcul� dans H). On lance une nouvelle lecture, pour lire
  l'\textit{opcode} de l'instruction machine suivante.
\item \texttt{goto Main1}. On revient au d�but de l'interpr�teur.
\end{itemize}

\paragraph{\texttt{IFEQ}} Cette instruction combine les id�es vues
dans le \texttt{POP} (la valeur au sommet du \textit{stack} doit �tre
supprim�e de celui-ci) et dans le \texttt{GOTO}. La comparaison de la
valeur au sommet du \textit{stack} et de 0 s'effectue en faisant
transiter ce sommet par l'ALU. Quand une valeur est calcul�e par
l'ALU, le bit de sortie \texttt{Z} est mis � 1 ssi cette valeur vaut
$0$. Ont peut alors utiliser un branchement conditionnel dans le
micro-code, qui teste la valeur de \texttt{Z} pour sauter vers la
partie du micro-code qui va ex�cuter la bonne branche du \texttt{IF}.

\section{Contr�le: le s�quenceur}
Afin d'assurer la bonne ex�cution du micro-code, des circuits
suppl�mentaires sont n�cessaires. En effet, le \textit{datapath}
n'assure que l'ex�cution des deux premi�res parties (manipulation de
registres et communication avec la m�moire) d'une seule
micro-instruction. Le circuit qui assure le flot d'ex�cution du
micro-programme (et donc, en particulier, s'occupe des \texttt{goto} et
des branchements conditionnels \textit{dans le micro-code}) s'appelle
le \emph{s�quenceur}, car il d�termine la \emph{s�quence} des
instructions � ex�cuter.

\subsection{Format des micro-instructions}
Avant de parler du s�quenceur, il faut expliquer pr�cis�ment comment
les micro-instructions sont stock�es (sous forme binaire). Chaque
micro-instruction indique les informations suivantes:

\begin{itemize}
\item Quel registre est s�lectionn� pour �crire sur le bus B ? Comme
  il ne peut y avoir qu'un registre � la fois qui �crit sur le bus B,
  et qu'il y a 9 possibilit�s (8 candidats, dont MBR qui peut �mettre
  sur B de deux mani�re diff�rentes), on n'a besoin de que de 4 bits
  pour coder cette information ($2^3<9<2^4$).
\item Quels registres re�oivent la valeur �mise sur le bus C ? Comme
  il y a 9 candidats potentiels, et que toute combinaison de ces 9
  candidats est possible, on a besoin de 9 bits pour sp�cifier cela.
\item Quelle op�ration l'ALU et le circuit de d�calage effectuent
  ils~?  Il faut $6$ bits pour couvrir les instructions de l'ALU (voir
  Fig. 4.2) et $2$ bits pour les instructions du \emph{shifter}. Au
  total, cela fait donc $8$ bits.
\item Va-t-il y avoir une communication avec la m�moire via le port
  MAR/MDR ? Comme il y a trois r�ponses possibles (pas de
  communication, lecture ou �criture), il faut 2 bits pour sp�cifier
  cette information.
\item Va-t-il y avoir une communication avec la m�moire via le port
  PC/MBR ? Comme seule la lecture est possible, 1 bit est suffisant
  pour sp�cifier les deux options (pas de communication ou lecture).
\end{itemize}
D'apr�s ce bilan, on a donc besoin de 24 bits pour encoder le
comportement d'\emph{un seul cycle}. Mais ce n'est pas suffisant pour
encoder une \emph{instruction} ! En effet, une instruction doit
�galement pr�ciser comment passer � l'instruction suivante. Pour ce
faire, nous allons utiliser des bits suppl�mentaires:
\begin{itemize}
\item 9 bits pour indiquer l'adresse de l'instruction suivante �
  ex�cuter (en cas de saut par exemple).
\item 3 bits pour indiquer comment l'instruction suivante est choisie
  (instruction physiquement suivante, saut, saut
  conditionnel\ldots).
\end{itemize}
Le format binaire d'une micro-instruction est montr� � la Fig. 4.5

\subsection{S�quenceur} 
\`A chaque cycle, le s�quenceur, doit extraire de la micro-instruction
deux types d'informations:
\begin{enumerate}
\item Quels signaux de contr�le (des registres, de l'ALU et de la
  m�moire) doivent �tre activ�s.
\item Quelle est la micro-instruction suivante � ex�cuter.
\end{enumerate}



Concr�tement, cela est r�alise par la machine, appel�e MIC-1 et
d�taill�e � la Fig. 4--6 du livre. On y retrouve le \textit{data path}
et le module de contr�le. On distingue sur cette figure:
\begin{enumerate}
\item La \emph{m�moire de contr�le} ou \textit{control store}, qui
  contient le micro-programme (interpr�teur) que nous avons d�taill�
  ci-dessus. Remarquez qu'il s'agit d'une m�moire \emph{morte},
  interne au CPU (et non pas la m�moire principale -- le
  micro-programme n'est pas cens� �tre modifi� par l'utilisateur).
\item Les registres du \textit{control store}. Afin de pouvoir lire
  dans le \textit{control store}, nous avons besoin de registres: un
  registre pour indiquer l'\emph{adresse} � consulter, et un registre
  qui va recevoir l'\emph{instruction lue}. Ces registres sont appel�s
  MPC et MIR (pour \textit{MicroProgram Counter} et
  \textit{MicroInstruction Register}).
\item Le \emph{data path} d�j� �tudi�.
\item Un d�codeur.
\item Quelques circuits de contr�le.
\end{enumerate}
Chaque fois qu'une instruction � ex�cuter est stock�e dans MIR, une
partie des bits qu'elle contient peut �tre directement exploit�e pour
contr�ler l'ALU: ce sont les 8 bits de contr�le de l'ALU et du
\textit{shifter}. La partie <<~C~>> de l'instruction peut directement
�tre connect�e aux ports de contr�le <<~bus C~>> des registres. Par
contre, la partie <<~B~>> de la micro-instruction doit d'abord �tre
d�cod�e pour contr�ler les ports de contr�le <<~bus B~>> des
registres. Enfin, les 3 bits <<~M~>> de la micro-instruction servent �
contr�ler la m�moire (qui n'appara�t pas sur la figure).

L'ex�cution d'un cycle se passe ainsi (4 sous-cycles):
\begin{enumerate}
\item Au d�but du cycle (flanc descendant de l'horloge), le contenu
  du \textit{control store} � l'adresse MPC est charg� dans MIR.
\item Les informations provenant de la micro-instruction se propagent:
  l'ALU sait quelle op�ration ex�cuter, et les registres qui doivent
  �crire sur les bus B et lire sur le bus C sont r�gl�s.
\item L'ALU effectue son calcul, ainsi que le \textit{shifter}.
\item La sortie du \textit{shifter} se propage sur le bus C et atteint
  les registres de sortie.
\end{enumerate}

Pour d�terminer la micro-instruction suivante, l'adresse pr�sente dans
la micro-instruction courante est d'abord charg�e dans MPC. Ensuite, la
partie <<~JAM~>> de la micro-instruction est inspect�e et permet de
d�terminer s'il faut faire un saut (�ventuellement conditionnel).
% \begin{itemize}
% \item Si JAM vaut 000, rien de plus n'est fait.
% \item Sinon:
%   \begin{itemize}
%   \item Si JAMN vaut 1, on remplace le bit de poids fort de MPC par:
%     sa valeur OR la sortie N de l'ALU. Ce registre de sortie de l'ALU
%     sera � 1 si la valeur calcul�e est n�gative. Ce m�canisme permet
%     donc de faire des sauts conditonnels.
%   \item Si JAMZ vaut 1, on remplace le bit de poids fort de MPC par:
%     sa valeur OR la sortie Z de l'ALU. Ce registre de sortie de l'ALU
%     sera � 1 si la valeur calcul�e est nulle. Ce m�canisme permet donc
%     de faire des sauts conditonnels.
%   \item Si JMPC vaut 1 on calcul un OR \textit{bitwise} des 8 bits de
%     MBR avec les 8 bits de poids faible de MPC, et on place le
%     r�sultat dans les 8 bits de poids faible de MPC. On se sert de ce
%     m�canisme pour faire un saut, et on s'arrange alors pour avoir les
%     8 bits de poids faible de MBR � 0, de mani�re � copier le contenu
%     de MBR dans les 8 bits de poids faible. Seul le bit de poids fort
%     peut �tre � 0 ou � 1.
%   \end{itemize}
% \end{itemize}
\end{document}




