MOOC NSI - fondamentaux
chapitre 1
section 1
video 1_1_9
durée : 7'

Titre : Représentation des caractères

Dans les vidéos précédentes nous avons couvert plusieurs techniques pour représenter des nombres de toute nature.

Intéressons-nous maintenant à la représentation des textes. 

L'idée de base est simple: elle consiste à associer un nombre binaire unique à chaque symbole. 
On entend par symbole: des lettres ou de la ponctuation, ou même les espaces. 

Ensuite, on représente un texte par la séquence des nombres binaires qui représentent chacun de ses symboles.


Cette idée n'est pas neuve. Au dix-neuvième siècle, l'ingénieur français, Jean-Maurice Emile
Baudot invente le premier code pratique permettant de représenter tout
l'alphabet latin (ainsi que des symboles de ponctuation usuels) sur 5
bits. Vous voyez à l'écran le brevet qu'il a déposé.

Ce code était utilisé pour transmettre de l'information entre des sortes de grosses machines à écrire électromécaniques qu'on pouvait commander à distance et qu'on appelait des téléscripteurs. 

Ces systèmes ont été largement utilisés pour la transmission
d'information à travers les ligne téléphoniques, jusque dans les
années 1980.

Leur vitesse de transmission se mesurait en bauds, en référence à Émile Baudot.


La première solution informatique largement utilisée a été le code ASCII, qui est l'acronyme d'American Standard Code for Information Interchange. 

Il a été introduit en 1967, et servait à l'origine pour l'échange d'information bancaires.

Il associe un nombre sur 7 bits à 128 caractères différents. On le voit sur la table à l'écran.

Les 32 premiers caractères, en jaune, ne sont pas des lettres ou des signes de ponctuation, mais bien des codes destinés à contrôler la ligne de communication. Pour le reste, le tableau permet de retrouver le code.

Par exemple, le caractère "W" en majuscule a ses bits de poids faible égaux à 7 en hexadécimal, et ses bits de poids forts égaux à 5 en hexadécimal. 

cela donne donc un code de 57 en base 16, soit 
101 0111 
en binaire


On peut alors trouver la représentation binaire d'un texte comme MOOC NSI, par exemple. On remplace simplement chaque lettre par son code dans la table. Ici, on l'a fait en hexadécimal pour commencer, mais on peut ensuite traduire ces nombres en binaire.


On peut se demander pourquoi les codes ascii se limitent à 7 bits ?

Le huitième bit de l'octet était appelé bit de parité, et utilisé pour détecter d'éventuelles erreurs de transmission. 

Ce bit de poids fort est calculé en faisant le "XOR" de tous les autres bits. Le bit de parité vaut donc 1 quand il y a un nombre impair de bits à 1 dans les données à transmettre, et 0 autrement.

Dans cet exemple, on cherche à transmettre la lettre M, avec son bit de parité à 0. Une erreur de transmission a lieu et un bit est modifié. La lettre E est reçue par le destinataire.

Celui-ci peut refaire le calcul du bit de parité, et réaliser qu'il devrait être à 1. Il y a donc eu une erreur quelque part dans la transmission, et le destinataire peut donc demander de retransmettre la lettre.


Plus tard, le huitième bit du code ASCCI a été utilisé pour ajouter 128 caractères au code. Ces caractères supplémentaires permettent de s'en servir dans d'autres langues que l'anglais, en introduisant des caractères propres à une autre langue.

Par exemple, le code iso-latin-1 est le même que l'ASCII pour les 128 premiers caractères, mais offre des caractères accentués pour les codes dont le bit de poids fort est à 1.


Récememnt, un projet international de normalisation appelé Unicode a vu le jour. Il a pour but de regrouper tous les symboles utilisés dans n'importe quelle langue, et ce, dans un seul code. La version 13 d'Unicode, publiée en mars 2020 comprend 143 859 caractères.

Par exemple, le caractère A majuscule avec accent circonflexe possède le code 00C2 en hexadécimal, qu'on note U+00C2 pour indiquer qu'il s'agit du code Unicode.

En plus d'associer un entier unique, appelé point de code, à chaque caractère, la norme unicode précise également comment ces entiers doivent être encodés en binaire.

Le plus courant est appelé UTF-8, et les caractères y sont encodés sur 1, 2, 3 ou 4 octets.



La norme Unicode est devenue aujourd'hui bien adoptée, notamment par
les sites et navigateurs web ainsi que par le langage Python.




