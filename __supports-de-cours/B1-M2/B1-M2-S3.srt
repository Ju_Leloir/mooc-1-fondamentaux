1
00:00:03,953 --> 00:00:06,497
Passons au troisième et dernier type construit,

2
00:00:06,597 --> 00:00:07,354
le dictionnaire.

3
00:00:07,454 --> 00:00:09,636
Comme pour le tableau et le p-uplet,

4
00:00:09,736 --> 00:00:11,300
le dictionnaire permet de manipuler

5
00:00:11,400 --> 00:00:12,644
plusieurs valeurs

6
00:00:12,744 --> 00:00:15,095
mais ici, les valeurs ne sont pas forcément

7
00:00:15,195 --> 00:00:16,169
indexées par des entiers.

8
00:00:16,665 --> 00:00:18,303
Les valeurs sont indexées par des clés

9
00:00:18,403 --> 00:00:20,397
qui doivent être des éléments non mutables.

10
00:00:20,780 --> 00:00:22,596
Pour les valeurs, pas de contrainte.

11
00:00:23,066 --> 00:00:24,581
Mais tout de suite, je vous propose

12
00:00:24,681 --> 00:00:26,476
de passer dans un interprète interactif

13
00:00:26,576 --> 00:00:28,293
et de manipuler ensemble le dictionnaire.

14
00:00:28,729 --> 00:00:30,938
Commençons à manipuler les dictionnaires

15
00:00:31,038 --> 00:00:32,540
et voir comment on construit un dictionnaire.

16
00:00:32,640 --> 00:00:33,771
Plusieurs méthodes

17
00:00:33,871 --> 00:00:35,027
En extension,

18
00:00:35,127 --> 00:00:38,103
en listant donc chacune des paires clé/valeur

19
00:00:38,203 --> 00:00:40,161
et en les encadrant par des accolades.

20
00:00:40,375 --> 00:00:41,555
Chaque paire

21
00:00:41,915 --> 00:00:43,327
possède la syntaxe suivante :

22
00:00:43,427 --> 00:00:44,406
la clé d'abord

23
00:00:44,506 --> 00:00:45,745
suivie d'un deux-points

24
00:00:45,845 --> 00:00:47,027
et la valeur associée.

25
00:00:48,499 --> 00:00:50,529
En utilisant la fonction dict(),

26
00:00:51,132 --> 00:00:52,697
de différentes manières.

27
00:00:53,736 --> 00:00:55,513
Première utilisation possible

28
00:00:55,613 --> 00:00:58,546
ici en lui donnant une liste de couples,

29
00:00:58,646 --> 00:01:02,041
chaque couple correspond à une paire clé/valeur.

30
00:01:06,334 --> 00:01:09,692
Nous pouvons utiliser également la fonction dict()

31
00:01:09,792 --> 00:01:11,489
pour créer un nouveau dictionnaire

32
00:01:11,589 --> 00:01:13,415
à partir d'un dictionnaire existant.

33
00:01:14,146 --> 00:01:16,365
Et éventuellement en rajoutant

34
00:01:16,465 --> 00:01:18,513
des paires clé/valeur.

35
00:01:19,111 --> 00:01:24,525
Le système correspond au système de paramètre nommé,

36
00:01:24,625 --> 00:01:26,882
ici, le paramètre nommé "annee"

37
00:01:26,982 --> 00:01:28,786
va correspondre à une nouvelle clé

38
00:01:28,886 --> 00:01:31,074
à laquelle on associe la valeur 2011.

39
00:01:32,812 --> 00:01:35,086
Voilà notre ville2_completee

40
00:01:35,186 --> 00:01:40,636
qui est partie des paires du dictionnaire ville2

41
00:01:41,100 --> 00:01:44,487
en ayant une nouvelle paire ajoutée.

42
00:01:45,741 --> 00:01:48,614
Dernière utilisation ici de dict()

43
00:01:48,714 --> 00:01:51,111
associée à la fonction prédéfinie zip,

44
00:01:51,211 --> 00:01:54,535
qui permet de construire des couples

45
00:01:54,635 --> 00:01:56,110
"annee" associée à 2019,

46
00:01:56,210 --> 00:01:58,499
"population" associée à 1035000, et cætera,

47
00:01:58,975 --> 00:02:00,361
pour créer notre dictionnaire.

48
00:02:01,958 --> 00:02:03,399
La construction en compréhension

49
00:02:04,909 --> 00:02:08,956
le motif, le pattern, de notre construction

50
00:02:09,327 --> 00:02:12,965
consiste à donner la clé deux-points et la valeur.

51
00:02:13,065 --> 00:02:14,795
Ici, nous avons une valeur

52
00:02:14,895 --> 00:02:18,830
qui est la même pour toutes les paires,

53
00:02:18,930 --> 00:02:20,926
mais nous pourrions bien sûr avoir quelque chose de différent.

54
00:02:21,026 --> 00:02:23,872
Ici, nous créons un dictionnaire

55
00:02:23,972 --> 00:02:26,933
dont les clés sont des couples d'entiers,

56
00:02:27,033 --> 00:02:29,392
et les valeurs associées, la valeur booléenne False.

57
00:02:34,013 --> 00:02:36,457
Donc voilà les trois possibilités

58
00:02:36,557 --> 00:02:38,857
de construire un dictionnaire.

59
00:02:38,957 --> 00:02:40,433
Dans la prochaine vidéo, nous allons voir

60
00:02:40,533 --> 00:02:42,252
comment manipuler ces dictionnaires,

61
00:02:42,352 --> 00:02:43,586
comment parcourir les éléments.

62
00:02:44,877 --> 00:02:46,635
Commençons à manipuler un dictionnaire.

63
00:02:48,299 --> 00:02:49,603
Donnons-nous un dictionnaire.

64
00:02:49,703 --> 00:02:52,426
La première chose que nous souhaitons faire,

65
00:02:52,526 --> 00:02:54,344
c'est accéder à la valeur

66
00:02:54,985 --> 00:02:56,106
associée à une clé.

67
00:02:57,025 --> 00:02:58,917
Donc ici la clé "population"

68
00:02:59,542 --> 00:03:01,596
nous souhaitons accéder à la valeur associée.

69
00:03:01,696 --> 00:03:03,979
Il suffit d'utiliser l'opérateur crochet

70
00:03:04,079 --> 00:03:05,932
et de lui passer le nom de la clé.

71
00:03:06,475 --> 00:03:08,412
Attention, si la clé n'existe pas,

72
00:03:08,512 --> 00:03:12,687
il y a une exception qui est levée,

73
00:03:12,787 --> 00:03:14,285
ici, une KeyError donc

74
00:03:14,613 --> 00:03:15,909
sur la clé "densite"

75
00:03:16,009 --> 00:03:17,457
qui n'existe pas dans notre dictionnaire.

76
00:03:18,106 --> 00:03:19,251
La méthode get() permet

77
00:03:19,882 --> 00:03:22,720
d'obtenir la valeur associée à une clé

78
00:03:22,820 --> 00:03:25,611
et éventuellement de récupérer une valeur par défaut

79
00:03:25,711 --> 00:03:27,367
en cas d'inexistence de la clé.

80
00:03:29,906 --> 00:03:33,474
Ici, une première utilisation avec une clé inexistante,

81
00:03:33,574 --> 00:03:36,762
nous récupérons la valeur None que nous avions mise

82
00:03:36,862 --> 00:03:38,396
et avec une clé existante,

83
00:03:38,496 --> 00:03:40,861
nous récupérons bien la valeur associée à la clé.

84
00:03:41,742 --> 00:03:43,330
Pour modifier un dictionnaire,

85
00:03:43,430 --> 00:03:46,719
nous avons deux modifications possibles.

86
00:03:46,819 --> 00:03:52,307
la modification de la valeur associée à une clé existante,

87
00:03:52,407 --> 00:03:53,721
ici, la clé "annee",

88
00:03:53,821 --> 00:03:56,149
la valeur associée, nous souhaitons la modifier.

89
00:03:56,249 --> 00:03:58,942
Il suffit de faire une simple affectation

90
00:03:59,042 --> 00:04:03,034
comme pour la modification des éléments d'une liste par exemple.

91
00:04:03,927 --> 00:04:07,897
Nous pouvons également modifier notre dictionnaire

92
00:04:07,997 --> 00:04:10,303
en rajoutant une paire clé/valeur

93
00:04:10,403 --> 00:04:12,906
tout simplement avec une affectation

94
00:04:13,006 --> 00:04:16,215
et une clé qui n'existait pas encore

95
00:04:16,315 --> 00:04:18,302
et lui donner une valeur.

96
00:04:18,799 --> 00:04:20,868
Notre ville4 possède maintenant

97
00:04:20,968 --> 00:04:23,590
la clé "pays" et la valeur associée "suisse".

98
00:04:24,884 --> 00:04:26,496
Dans la prochaine vidéo, nous allons voir

99
00:04:26,596 --> 00:04:30,096
comment parcourir les éléments d'un dictionnaire,

100
00:04:30,196 --> 00:04:31,444
ses clés ou ses valeurs.

101
00:04:33,199 --> 00:04:35,087
Voyons ensemble comment parcourir un dictionnaire.

102
00:04:35,534 --> 00:04:38,902
Par une simple boucle for sur un dictionnaire,

103
00:04:39,002 --> 00:04:41,018
nous parcourons les clés du dictionnaire.

104
00:04:42,259 --> 00:04:43,835
On peut vouloir parcourir les valeurs,

105
00:04:43,935 --> 00:04:45,620
il faut alors utiliser la méthode values()

106
00:04:45,720 --> 00:04:48,950
qui nous offre un itérable sur les valeurs de notre dictionnaire.

107
00:04:50,430 --> 00:04:54,603
On peut également vouloir parcourir les couples clé/valeur

108
00:04:54,703 --> 00:04:57,586
en utilisant à ce moment-là la méthode items()

109
00:04:58,334 --> 00:05:00,124
et toujours la boucle for.

110
00:05:01,883 --> 00:05:03,984
Comment tester l'existence d'une clé ?

111
00:05:04,084 --> 00:05:05,804
Donnons-nous un dictionnaire

112
00:05:05,904 --> 00:05:07,700
et ici nous voulons tester

113
00:05:07,800 --> 00:05:09,984
l'existence de la clé "mangue"

114
00:05:10,084 --> 00:05:11,184
dans notre dictionnaire.

115
00:05:11,284 --> 00:05:13,683
Nous utilisons pour cela l'opérateur in

116
00:05:13,783 --> 00:05:17,020
qui nous fournit donc la réponse

117
00:05:17,120 --> 00:05:20,015
à l'existence d'une clé dans notre dictionnaire.

118
00:05:20,847 --> 00:05:23,725
Attention, sur les valeurs

119
00:05:23,825 --> 00:05:25,565
nous ne pouvons pas utiliser l'opérateur in

120
00:05:25,665 --> 00:05:29,182
pour savoir si une valeur appartient ou pas au dictionnaire.

121
00:05:30,730 --> 00:05:33,374
Voilà, cela conclut les vidéos sur les dictionnaires.

122
00:05:33,474 --> 00:05:35,928
Encore une fois, je vous conseille de beaucoup manipuler

123
00:05:36,028 --> 00:05:37,978
et de revenir au document texte

124
00:05:38,078 --> 00:05:39,205
qui est mis à votre disposition.

