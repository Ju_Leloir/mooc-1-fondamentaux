1
00:00:01,419 --> 00:00:03,240
Dans cette dernière vidéo, nous allons

2
00:00:03,340 --> 00:00:05,258
voir le dernier objet

3
00:00:05,458 --> 00:00:07,021
de notre application,

4
00:00:07,121 --> 00:00:09,155
de notre programme escape game,

5
00:00:09,255 --> 00:00:10,496
qui est la cellule.

6
00:00:11,313 --> 00:00:13,344
La cellule, c'est ce qui compose

7
00:00:14,309 --> 00:00:15,592
notre château.

8
00:00:18,526 --> 00:00:20,936
Le château est constitué, on le voit,

9
00:00:21,036 --> 00:00:22,152
d'un certain nombre de petits carrés,

10
00:00:22,252 --> 00:00:23,218
chacun de ces petits carrés

11
00:00:23,318 --> 00:00:25,174
est ce que j'appelle une cellule

12
00:00:25,274 --> 00:00:27,236
et nous avons donc des cellules

13
00:00:27,336 --> 00:00:28,480
de différents types.

14
00:00:28,580 --> 00:00:29,753
La cellule, elle possède

15
00:00:29,937 --> 00:00:31,533
des coordonnées en x et en y,

16
00:00:31,633 --> 00:00:33,050
une position donc dans le château,

17
00:00:34,398 --> 00:00:35,267
pour le château,

18
00:00:35,744 --> 00:00:39,143
nous avions défini un attritut map,

19
00:00:39,621 --> 00:00:41,207
ce sera une liste de listes,

20
00:00:41,307 --> 00:00:43,168
une matrice de cellules.

21
00:00:43,735 --> 00:00:45,840
Chaque cellule possède une coordonnée

22
00:00:46,598 --> 00:00:47,445
et un type.

23
00:00:47,545 --> 00:00:49,650
Ici, nous avons des cellules blanches

24
00:00:49,750 --> 00:00:51,187
de type vide on va dire,

25
00:00:51,287 --> 00:00:54,336
ce sont les couloirs dans lesquels le héros peut circuler,

26
00:00:54,436 --> 00:00:56,447
des cellules grises de type mur,

27
00:00:56,547 --> 00:00:58,578
des cellules orange de type porte,

28
00:00:58,678 --> 00:01:00,937
des cellules vertes de type objet, et cætera.

29
00:01:01,430 --> 00:01:04,585
Tous ces types se retrouvent en fait

30
00:01:04,685 --> 00:01:08,144
dans le fichier plan du château

31
00:01:08,809 --> 00:01:10,785
sous forme d'entiers, 0, 1, 2, 3, et cætera.

32
00:01:11,423 --> 00:01:14,248
Et au moment du setup,

33
00:01:14,348 --> 00:01:16,648
quand nous allons lire et traiter

34
00:01:16,748 --> 00:01:18,893
les données d'un fichier plan,

35
00:01:19,126 --> 00:01:20,969
c'est là que nous allons créer nos cellules

36
00:01:22,070 --> 00:01:23,566
de différents types.

37
00:01:23,666 --> 00:01:27,497
Les types sont ici regroupés en noms de constantes

38
00:01:27,597 --> 00:01:28,397
VIDE, c'est 0,

39
00:01:28,497 --> 00:01:29,266
MUR, c'est 1,

40
00:01:29,366 --> 00:01:30,676
SORTIE, c'est 2, et cætera.

41
00:01:33,080 --> 00:01:35,136
Voyons un petit peu à quoi ressemble

42
00:01:36,443 --> 00:01:38,657
notre objet cellule,

43
00:01:38,757 --> 00:01:39,543
et surtout,

44
00:01:40,043 --> 00:01:41,947
ce que nous pouvons faire comme interaction,

45
00:01:42,047 --> 00:01:43,424
ce que va pouvoir faire comme interaction

46
00:01:43,897 --> 00:01:44,720
la cellule.

47
00:01:45,775 --> 00:01:46,904
Nous l'appelons Cell.

48
00:01:48,243 --> 00:01:51,335
La cellule, nous l'avons vu, possède

49
00:01:53,828 --> 00:01:54,872
des coordonnées,

50
00:01:55,334 --> 00:01:57,301
une position donc dans le château,

51
00:01:57,401 --> 00:01:58,549
un type,

52
00:01:59,186 --> 00:02:00,482
et puis un château.

53
00:02:00,582 --> 00:02:02,324
La cellule est créée à l'intérieur d'un château.

54
00:02:07,298 --> 00:02:10,565
Voilà, le temps d'écrire nos différentes propriétés.

55
00:02:10,890 --> 00:02:11,797
Donc le type

56
00:02:11,897 --> 00:02:13,589
et puis le château.

57
00:02:14,713 --> 00:02:16,168
Le château est très important parce que

58
00:02:16,393 --> 00:02:17,931
effectivement, ce qu'il va se passer,

59
00:02:18,031 --> 00:02:18,609
c'est que la cellule,

60
00:02:18,709 --> 00:02:20,734
ça va être vraiment un objet type modèle.

61
00:02:20,834 --> 00:02:22,423
C'est-à-dire que la cellule

62
00:02:22,523 --> 00:02:26,078
va ne faire que des actions logiques,

63
00:02:26,178 --> 00:02:28,944
des changements de type, et cætera,

64
00:02:29,288 --> 00:02:32,271
mais la cellule ne va embarquer

65
00:02:32,371 --> 00:02:36,030
aucune méthode qui manipule une tortue.

66
00:02:36,130 --> 00:02:39,525
Et quand une cellule devra se mettre à jour

67
00:02:39,625 --> 00:02:40,657
d'un point de vue de l'affichage,

68
00:02:40,757 --> 00:02:42,258
c'est-à-dire quand son modèle aura changé,

69
00:02:42,358 --> 00:02:43,600
alors, son modèle, c'est quoi ?

70
00:02:43,700 --> 00:02:46,018
Effectivement, c'est que son type va changer,

71
00:02:46,118 --> 00:02:48,858
quand le héros

72
00:02:50,273 --> 00:02:52,256
par exemple, même sur une case vide,

73
00:02:52,356 --> 00:02:54,031
quand le héros passe sur une case vide,

74
00:02:54,630 --> 00:02:56,565
qu'il a visité une case vide,

75
00:02:56,665 --> 00:02:58,212
son type change et elle passe

76
00:02:58,312 --> 00:03:01,649
de VIDE à SEEN, déjà vue.

77
00:03:02,040 --> 00:03:04,049
Le type passe de 0 à 6,

78
00:03:04,167 --> 00:03:06,021
c'est juste un changement de modèle,

79
00:03:06,121 --> 00:03:08,803
un changement logique

80
00:03:10,130 --> 00:03:12,332
et une fois que ce changement logique

81
00:03:12,432 --> 00:03:13,488
est fait par la cellule,

82
00:03:13,588 --> 00:03:15,255
la cellule demande au château

83
00:03:15,355 --> 00:03:16,539
informe le château

84
00:03:16,639 --> 00:03:18,352
qu'il y a eu un changement du modèle

85
00:03:18,452 --> 00:03:21,884
et c'est le château qui répercute ce changement

86
00:03:21,984 --> 00:03:23,855
en mettant à jour son affichage.

87
00:03:23,955 --> 00:03:28,660
Et donc, c'est cette séparation bien franche

88
00:03:28,760 --> 00:03:30,719
qui est importante et qui permet d'avoir

89
00:03:30,819 --> 00:03:34,783
un code pas trop touffu

90
00:03:34,883 --> 00:03:37,636
et facile à suivre et à maintenir.

91
00:03:38,893 --> 00:03:40,458
Voilà pour notre cellule.

92
00:03:43,879 --> 00:03:47,302
Ces cellules, elles ont un type, on l'a dit,

93
00:03:48,449 --> 00:03:50,274
il y a des cellules qui se ressemblent plus ou moins,

94
00:03:50,374 --> 00:03:52,701
qui n'auront pas grand chose à faire,

95
00:03:52,801 --> 00:03:54,411
la cellule vide, la cellule mur,

96
00:03:54,511 --> 00:03:56,829
la cellule entrée, la cellule seen,

97
00:03:56,929 --> 00:03:57,632
la cellule sortie,

98
00:03:57,732 --> 00:03:59,588
donc toutes ces cellules-là se ressemblent,

99
00:03:59,688 --> 00:04:03,011
en gros, ce sont des cellules un peu neutres on va dire,

100
00:04:03,685 --> 00:04:06,119
et puis on a les cellules porte et objet

101
00:04:06,219 --> 00:04:08,804
qui elles, font un peu plus de choses

102
00:04:08,904 --> 00:04:12,039
et donc, c'est intéressant de créer

103
00:04:12,139 --> 00:04:13,864
des objets correspondant à ces cellules.

104
00:04:14,976 --> 00:04:18,283
Et donc on aura un objet Porte,

105
00:04:18,383 --> 00:04:21,303
qui dérive, d'un point de vue programmation objet,

106
00:04:21,403 --> 00:04:23,216
qui dérive de l'objet Cell,

107
00:04:23,798 --> 00:04:28,016
donc il va embarquer les mêmes attributs,

108
00:04:28,116 --> 00:04:30,122
et éventuellement d'autres.

109
00:04:35,049 --> 00:04:39,828
Et donc, d'un point de vue technique,

110
00:04:44,203 --> 00:04:46,911
le constructeur va définir

111
00:04:47,594 --> 00:04:49,213
deux attributs supplémentaires

112
00:04:49,313 --> 00:04:52,104
pour la porte,

113
00:04:52,204 --> 00:04:53,148
qui sont une question,

114
00:04:53,248 --> 00:04:54,194
qui va être une chaîne de caractères,

115
00:04:54,294 --> 00:04:55,717
qui au moment de la création n'existe pas,

116
00:04:55,817 --> 00:05:00,392
donc cette chaîne de caractères va être initialisée

117
00:05:00,492 --> 00:05:01,195
plus tard

118
00:05:01,295 --> 00:05:04,306
lorsqu'on va traiter le dictionnaire des portes.

119
00:05:04,406 --> 00:05:05,198
Dans le setup,

120
00:05:05,298 --> 00:05:06,685
quand on traitera le dictionnaire des portes,

121
00:05:06,785 --> 00:05:09,301
on va récupérer effectivement les deux chaînes de caractères

122
00:05:09,401 --> 00:05:10,836
qui correspondent

123
00:05:10,936 --> 00:05:13,824
à la question et à la solution.

124
00:05:16,456 --> 00:05:17,559
Au moment de la création,

125
00:05:17,659 --> 00:05:19,305
on a simplement deux attributs

126
00:05:19,405 --> 00:05:21,317
qui sont des chaînes de caractères

127
00:05:21,417 --> 00:05:25,055
mais qui pour l'instant n'ont pas encore de vraie valeur.

128
00:05:25,615 --> 00:05:28,667
Et puis, les arguments

129
00:05:28,767 --> 00:05:31,627
vont être ceux-là en fait,

130
00:05:31,727 --> 00:05:35,867
donc les coordonnées de la porte,

131
00:05:35,967 --> 00:05:39,807
le type qui sera en fait fixé,

132
00:05:39,907 --> 00:05:40,844
qui sera de type porte,

133
00:05:41,374 --> 00:05:42,713
et le château.

134
00:05:43,338 --> 00:05:44,880
Et donc on va demander,

135
00:05:45,365 --> 00:05:47,839
dans le constructeur de la porte,

136
00:05:47,939 --> 00:05:49,342
on va appeler

137
00:05:49,997 --> 00:05:54,802
le constructeur de la cellule

138
00:05:55,403 --> 00:05:58,700
en passant en premier paramètre la porte

139
00:05:58,800 --> 00:05:59,872
et en lui passant

140
00:06:03,509 --> 00:06:04,527
les arguments.

141
00:06:07,042 --> 00:06:09,705
De même pour l'objet,

142
00:06:09,805 --> 00:06:11,390
je ne détaille pas ici

143
00:06:11,490 --> 00:06:14,242
mais on aura une classe Objet

144
00:06:14,342 --> 00:06:15,756
au sens du jeu,

145
00:06:15,856 --> 00:06:17,377
Objet ou Indice si vous voulez,

146
00:06:18,901 --> 00:06:24,155
qui dérivera de l'objet Cell.

147
00:06:30,861 --> 00:06:32,414
Donc ces cellules,

148
00:06:33,298 --> 00:06:35,420
notamment on va voir

149
00:06:35,958 --> 00:06:38,438
les actions avec ces cellules,

150
00:06:38,538 --> 00:06:39,461
on va voir ça

151
00:06:39,561 --> 00:06:41,649
justement dans la méthode avancer.

152
00:06:41,749 --> 00:06:44,475
Quand le héros va vouloir avancer,

153
00:06:48,134 --> 00:06:50,163
donc il avance dans une direction,

154
00:06:54,435 --> 00:06:56,370
la direction, je vous rappelle, c'est un couple

155
00:07:00,639 --> 00:07:03,171
qui correspond aux deltas x et y

156
00:07:04,620 --> 00:07:06,703
sur les coordonnées du héros.

157
00:07:07,291 --> 00:07:08,939
Donc l'idée, ça va être de récupérer

158
00:07:09,039 --> 00:07:10,940
effectivement l'objet cellule,

159
00:07:11,952 --> 00:07:13,072
différents objets cellules,

160
00:07:13,172 --> 00:07:14,924
un premier objet cellule qui est l'objet

161
00:07:15,024 --> 00:07:17,254
que j'appelle la cellule d'origine,

162
00:07:17,354 --> 00:07:18,568
qui est la cellule

163
00:07:18,668 --> 00:07:19,750
où se trouve le héros.

164
00:07:19,850 --> 00:07:22,473
Le héros va demander au château

165
00:07:22,573 --> 00:07:24,058
quelle est la cellule sur laquelle il se trouve.

166
00:07:24,957 --> 00:07:27,912
Le château va devoir implémenter une méthode cell

167
00:07:28,518 --> 00:07:30,653
à qui on passe des coordonnées

168
00:07:30,753 --> 00:07:32,124
et qui va retourner

169
00:07:33,440 --> 00:07:34,608
l'objet cellule

170
00:07:34,708 --> 00:07:35,700
qui se trouve à cet endroit-là.

171
00:07:36,690 --> 00:07:37,890
Ça, ça va être important.

172
00:07:39,389 --> 00:07:41,942
Ensuite, on va commencer par

173
00:07:42,042 --> 00:07:43,598
un petit test qui

174
00:07:44,904 --> 00:07:47,282
est de savoir si la future destination

175
00:07:50,372 --> 00:07:52,111
est bien dans les limites du château.

176
00:07:52,211 --> 00:07:53,735
Donc si la future destination

177
00:07:54,414 --> 00:07:55,682
est dans les limites du château,

178
00:07:55,782 --> 00:07:57,823
donc si on ne tente pas de sortir.

179
00:07:57,923 --> 00:08:00,012
En fait, le seul cas possible,

180
00:08:00,112 --> 00:08:01,068
c'est quand on est dans l'entrée

181
00:08:01,168 --> 00:08:03,990
et qu'on essaie d'aller encore vers le nord ici.

182
00:08:04,090 --> 00:08:05,534
Sinon effectivement, le château normalement

183
00:08:05,634 --> 00:08:06,540
est entouré de murs

184
00:08:06,640 --> 00:08:10,765
et donc on va tomber sur un mur.

185
00:08:11,384 --> 00:08:14,766
Donc si la future destination est dans les limites,

186
00:08:15,901 --> 00:08:16,664
à ce moment-là,

187
00:08:16,764 --> 00:08:20,944
on récupère effectivement la destination.

188
00:08:21,044 --> 00:08:23,294
Quand je dis qu'on récupère la destination,

189
00:08:23,394 --> 00:08:24,927
encore une fois, il s'agit de l'objet

190
00:08:28,223 --> 00:08:31,599
qui se trouve aux coordonnées

191
00:08:31,699 --> 00:08:33,255
de la destination, c'est-à-dire

192
00:08:33,845 --> 00:08:37,150
en appliquant le delta de déplacement

193
00:08:37,250 --> 00:08:39,488
sur les coordonnées x et y

194
00:08:39,588 --> 00:08:41,771
où se trouve le héros.

195
00:08:44,989 --> 00:08:46,924
Et là, on va lancer également

196
00:08:47,024 --> 00:08:50,044
toute une série de tests,

197
00:08:50,144 --> 00:08:53,970
savoir si la destination par exemple est accessible.

198
00:08:54,070 --> 00:08:55,292
On va se poser la question 

199
00:08:55,392 --> 00:08:58,530
est-ce que la destination est accessible ?

200
00:09:00,138 --> 00:09:02,007
Cette question se traduit par un appel

201
00:09:02,107 --> 00:09:04,130
à une méthode, accessible,

202
00:09:04,658 --> 00:09:07,665
qui est une méthode de la cellule.

203
00:09:10,224 --> 00:09:12,139
Allons voir à quoi pourrait ressembler

204
00:09:12,239 --> 00:09:14,429
effectivement cette méthode.

205
00:09:22,664 --> 00:09:24,224
Pour une cellule lambda,

206
00:09:24,669 --> 00:09:26,070
je vous rappelle que les cellules lambdas

207
00:09:26,170 --> 00:09:28,214
sont les cellules vides,

208
00:09:28,314 --> 00:09:29,385
les cellules déjà vues,

209
00:09:30,039 --> 00:09:31,680
la cellule entrée

210
00:09:31,780 --> 00:09:33,034
et la cellule sortie

211
00:09:33,134 --> 00:09:34,154
et la cellule mur,

212
00:09:36,301 --> 00:09:39,788
donc toutes les cellules sauf les portes et les objets.

213
00:09:40,383 --> 00:09:42,640
Donc pour une cellule lambda,

214
00:09:42,740 --> 00:09:44,527
elle est accessible si quoi ?

215
00:09:44,627 --> 00:09:46,711
Si ce n'est pas un mur.

216
00:09:47,232 --> 00:09:48,416
Dès que c'est autre chose qu'un mur,

217
00:09:48,516 --> 00:09:49,237
c'est accessible.

218
00:09:49,707 --> 00:09:52,355
Donc ici, la méthode accessible

219
00:09:52,455 --> 00:09:54,579
renvoie simplement le fait que

220
00:09:56,769 --> 00:09:59,499
le type est différent du type mur.

221
00:10:00,064 --> 00:10:01,295
Voilà, tout simplement.

222
00:10:01,825 --> 00:10:06,271
Et par contre, les autres objets

223
00:10:06,371 --> 00:10:07,834
vont devoir implémenter

224
00:10:07,934 --> 00:10:10,522
leur propre méthode accessible.

225
00:10:10,622 --> 00:10:12,316
Par exemple, pour la porte,

226
00:10:16,160 --> 00:10:17,810
est-ce que la porte est accessible ?

227
00:10:17,910 --> 00:10:19,197
Elle est accessible si elle n'est pas verrouillée.

228
00:10:20,781 --> 00:10:22,563
On va retourner

229
00:10:23,085 --> 00:10:26,178
not self.verrouillee

230
00:10:28,377 --> 00:10:29,623
Tout simplement.

231
00:10:30,492 --> 00:10:33,700
Et en fait, ici, évidemment,

232
00:10:34,392 --> 00:10:36,883
une porte ne sera plus verrouillée

233
00:10:36,983 --> 00:10:37,830
si elle a changé de type,

234
00:10:37,930 --> 00:10:41,709
donc en fait si son type est devenu vide.

235
00:10:42,269 --> 00:10:46,311
Mais ce ne sont que des manipulations logiques

236
00:10:46,411 --> 00:10:47,873
sur notre modèle.

237
00:10:49,884 --> 00:10:52,728
Et ces manipulations logiques en fait

238
00:10:52,828 --> 00:10:54,242
ensuite vont se traduire

239
00:10:54,342 --> 00:10:55,652
effectivement par un changement

240
00:10:55,752 --> 00:10:58,424
sur l'affichage

241
00:10:58,524 --> 00:10:59,509
qui va être réalisé,

242
00:10:59,609 --> 00:11:00,676
donc cette mise à jour sur l'affichage

243
00:11:00,776 --> 00:11:02,227
va être réalisée par le château.

244
00:11:03,891 --> 00:11:05,013
Voilà.

245
00:11:07,363 --> 00:11:09,152
Je vous invite à terminer ce projet

246
00:11:09,252 --> 00:11:11,300
qu'on a bien dégrossi,

247
00:11:11,400 --> 00:11:13,786
on a bien vu les différents objets,

248
00:11:13,886 --> 00:11:14,784
les différents acteurs,

249
00:11:14,884 --> 00:11:16,344
les différentes interactions.

250
00:11:16,905 --> 00:11:20,002
Et donc vous n'avez plus qu'à finir

251
00:11:20,102 --> 00:11:21,083
l'ensemble des méthodes

252
00:11:21,183 --> 00:11:24,875
et pouvoir vous lancer dans des petits escape games.

