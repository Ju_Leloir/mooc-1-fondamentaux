1
00:00:03,207 --> 00:00:04,107
Bonjour !

2
00:00:04,594 --> 00:00:06,249
Dans le premier bloc de ce MOOC,

3
00:00:06,349 --> 00:00:07,519
nous avons parlé des données

4
00:00:07,619 --> 00:00:08,667
de façon très générale

5
00:00:08,767 --> 00:00:12,603
depuis leur encodage jusqu'à leur utilisation dans les bases.

6
00:00:13,366 --> 00:00:14,368
Dans ce deuxième bloc

7
00:00:14,468 --> 00:00:16,451
nous vous proposons d'étudier les outils

8
00:00:16,551 --> 00:00:18,248
qui permettent de manipuler ces données,

9
00:00:18,348 --> 00:00:20,192
à savoir les langages de programmation.

10
00:00:20,766 --> 00:00:24,103
Alors nous n'allons pas parler de chaque langage de programmation

11
00:00:24,203 --> 00:00:26,858
mais plutôt vous présenter les grandes familles

12
00:00:26,958 --> 00:00:30,696
constituées par les paradigmes de programmation.

13
00:00:31,992 --> 00:00:33,160
En trois modules,

14
00:00:33,260 --> 00:00:36,173
nous allons d'abord vous faire une petite introduction

15
00:00:36,273 --> 00:00:38,191
de la programmation orientée objet

16
00:00:38,291 --> 00:00:40,490
qui est l'un des paradigmes les plus utilisés actuellement

17
00:00:41,158 --> 00:00:43,479
sous l'angle du langage Python.

18
00:00:43,989 --> 00:00:46,920
Nous verrons donc une première définition

19
00:00:47,020 --> 00:00:49,325
de ce que c'est qu'un langage de programmation orienté objet.

20
00:00:49,425 --> 00:00:51,550
Nous présenterons en détail en vidéo

21
00:00:51,650 --> 00:00:54,176
un projet que vous pourrez continuer

22
00:00:54,276 --> 00:00:56,965
afin de manipuler concrètement la programmation objet.

23
00:00:57,433 --> 00:00:59,028
Dans un deuxième module,

24
00:00:59,128 --> 00:01:01,089
nous reviendrons sur différents paradigmes.

25
00:01:01,189 --> 00:01:02,639
Le paradigme impératif,

26
00:01:02,739 --> 00:01:05,274
le paradigme objet mais cette fois,

27
00:01:05,374 --> 00:01:06,838
d'un point de vue plus général,

28
00:01:07,038 --> 00:01:09,512
et enfin, le paradigme fonctionnel.

29
00:01:11,316 --> 00:01:15,009
Dans le troisième et dernier module de ce bloc,

30
00:01:15,109 --> 00:01:16,819
nous ferons une introduction

31
00:01:16,919 --> 00:01:18,356
à la complexité de programmes

32
00:01:18,456 --> 00:01:21,073
qui fera le lien avec le bloc suivant

33
00:01:21,173 --> 00:01:22,484
qui va nous parler d'algorithmique.

34
00:01:23,413 --> 00:01:24,641
Je vous remercie

35
00:01:24,741 --> 00:01:25,741
et bon apprentissage !

