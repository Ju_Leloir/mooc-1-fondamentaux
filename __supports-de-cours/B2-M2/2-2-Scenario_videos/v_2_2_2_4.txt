Scenario v_2_2_2_4.txt
Les langages impératifs

Nous résumons maintenant les caractéristiques des langages impératifs 


Un langage est dit impératif s'il utilise le paradigme de programmation impérative où les opérations sont des séquences d'instructions exécutées par l'ordinateur pour modifier l'état du programme.

La plupart des langages impératifs comportent cinq constructions de base :

- la déclaration de variable
- l'assignation appelée également affectation
- la séquence
- le test
- la boucle

auxquelles s'ajoute généralement des instructions d'entrées / sorties.

Si nous ajoutons la possibilité d'utiliser et de définir des fonctions et des modules, nous parlons de paradigme de programmation procédurale ou plus simplement de langages procéduraux. 

Dans ce cas, un programme complet comporte généralement (dans un ordre qui dépend du langage):

- Un contexte: l’identité du programme (son nom), des éléments informatifs (auteur, date d’écriture, machine cible...), l'objet du programme, etc ;

- Les importations de différents paquetages ou bibliothèques, standard ou ad hoc, ainsi que les autres fichiers sources à intégrer ;

- Les données et déclarations « globales » partagées par le programme et ses différents sous-programmes ;

- Les codes des sous-programmes éventuels.

- Le code "principal" du programme qui contient les déclarations, définitions et initialisations des éléments « locaux » et les instructions.

Si l’on compare différents langages, la plupart des langages algorithmiques, qu’on appelle aussi « ALGOL-like », se ressemblent fort par la forme et l’effet de leurs instructions, mais ils peuvent sembler très différents si l’on examine leurs parties contextuelles et déclaratives.

Dans la vidéo suivante nous détaillons certaines grandes caractéristiques qui permettent des classer les langages de programmation
