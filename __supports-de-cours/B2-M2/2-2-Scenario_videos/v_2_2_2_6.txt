


Scénario : v_2_2_2_6
Titre: Fonctions et modules



A peu près tous les langages impératifs et tous les langages orientés-objets contiennent sous une forme ou une autre la notion de fonction.

Nous utilisons le terme « fonction » pour désigner toutes les constructions des langages permettant la découpe du code en blocs de code appelables dans différents contextes.

Regardons cette notion d'un peu plus près.

D'abord, ce concept se décline sous différentes formes. Cela peut être :

• Une « sous-routine » , encore appelée simplement « routine », « sous-programme » ou « procédure » : qui est un bloc de code appelable avec retour à l’instruction qui suit l’appel  — échangeant ses informations via paramètres et variables globales ;


• Une « fonction »  stricto sensu : extension du cas précédent qui évalue, en outre, une valeur de retour, renvoyée dans le contexte de l’appel, nécessairement au sein d’une expression, une procédure étant parfois présentée comme « une fonction à valeur vide » ou « sans valeur » ;


- Une « méthode » : simple fonction ou procédure définie au sein d’une classe et s’appliquant donc implicitement à un objet de cette classe, parfois à l’objet-classe lui-même (pour les méthodes statiques) ;


- Un « opérateur » : cas particulier de fonction ou de méthode dont le nom n’est pas un identificateur mais un symbole (également appelé un opérateur) et dont la syntaxe d’appel s’apparente à celle d’une sous-expression algébrique ;


- Une « coroutine » : forme généralisée de routine qui, lors d’un appel, reprend l’exécution de son code à l’endroit du retour précédent — plutôt appelé « YIELD » que « RETURN », d’ailleurs  et doit par conséquent maintenir son état local  d’un appel à l’autre ;


- Un « thread » (ou fil d’exécution) : routine qui s’exécute en parallèle du bloc appelant qui, lui, poursuit donc son exécution sans attendre le retour de la routine, sauf aux points explicites de synchronisation (mécanismes de rendez-vous ou de sémaphore).


Nous utilisons ici le terme générique de « fonction » pour désigner ces différents cas.


Les notions d’en-tête, de corps, de paramètre formel, de paramètre effectif, d’arité de signature, de prototype, de type d’une fonction, de surcharge, de récursivité, de fonction variadique (qui admet un nombre variable et indéterminé de paramètres effectifs), de modes de transmission, valeur par défaut, ..., sont présents pour parler de fonctions.


Nous laissons le lecteur le soin de regarder la richesse qu'offre Python pour définir des fonctions variadiques ainsi que des paramètres positionnels ou nommés et des valeurs par défaut.


Notons les différents modes de transmission en fonction du langage utilisé.  On parle de transmission :

- par valeur (by copy-in en anglais)
- par référence
- par copies (call by copy-in copy-out ou by copy-restore)
- par partage
- par résultat (ou copy-out)
- par nom
- à l’usage (call by need)
- comme constante 

Je vous invite à regarder à quel mécanisme de passage de paramètre correspond chacun de ses modes de transmission

Il existe évidemment d’autres modes exotiques encore plus rares

