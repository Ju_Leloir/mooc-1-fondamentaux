1
00:00:00,251 --> 00:00:01,737
Un langage de programmation

2
00:00:01,837 --> 00:00:03,404
est donc une notation artificielle

3
00:00:03,504 --> 00:00:05,005
destinée à exprimer des algorithmes

4
00:00:05,105 --> 00:00:06,204
et produire des programmes.

5
00:00:06,516 --> 00:00:08,942
D'une manière similaire à une langue naturelle,

6
00:00:09,042 --> 00:00:11,515
un langage de programmation a un alphabet,

7
00:00:11,615 --> 00:00:12,703
un vocabulaire,

8
00:00:12,803 --> 00:00:14,543
des règles de grammaire, sa syntaxe,

9
00:00:14,643 --> 00:00:16,492
et une signification, sa sémantique.

10
00:00:16,968 --> 00:00:19,104
Il est mis en œuvre par un traducteur automatique

11
00:00:19,204 --> 00:00:21,526
qui est soit un compilateur pour les langages compilés,

12
00:00:21,626 --> 00:00:24,597
soit un interpréteur pour les langages interprétés.

13
00:00:24,913 --> 00:00:26,409
Parlons d'abord de compilateur.

14
00:00:27,000 --> 00:00:29,632
Un compilateur fonctionne par analyse/synthèse.

15
00:00:29,902 --> 00:00:32,486
Au lieu de remplacer chaque construction du langage source

16
00:00:32,586 --> 00:00:34,121
par une suite équivalente

17
00:00:34,221 --> 00:00:36,414
de constructions du langage cible,

18
00:00:36,514 --> 00:00:38,261
il commence par analyser le texte source

19
00:00:38,361 --> 00:00:41,326
pour en construire une représentation intermédiaire appelée image,

20
00:00:41,636 --> 00:00:43,181
qu'il traduit à son tour

21
00:00:43,281 --> 00:00:44,257
en langage cible.

22
00:00:44,357 --> 00:00:46,603
On sépare généralement un compilateur

23
00:00:46,703 --> 00:00:47,792
en au moins deux parties,

24
00:00:47,892 --> 00:00:49,785
une partie frontale, front-end en anglais,

25
00:00:50,300 --> 00:00:51,748
qui lit le texte source

26
00:00:51,848 --> 00:00:52,688
et produit l'image.

27
00:00:52,947 --> 00:00:55,418
Et une partie arrière, finale, back-end en anglais,

28
00:00:55,518 --> 00:00:57,431
qui parcourt cette représentation

29
00:00:57,531 --> 00:00:59,545
pour produire le code cible.

30
00:01:00,236 --> 00:01:01,692
Les phases de la compilation

31
00:01:01,792 --> 00:01:03,529
incluent, pour la partie analyse,

32
00:01:04,328 --> 00:01:06,271
l'analyse lexicale, scanning en anglais,

33
00:01:06,371 --> 00:01:09,189
qui découpe la séquence de caractères du code source

34
00:01:09,289 --> 00:01:11,821
en blocs atomiques, appelés jetons, tokens en anglais.

35
00:01:11,921 --> 00:01:13,144
Chaque jeton

36
00:01:13,244 --> 00:01:15,428
appartient à une unité atomique unique

37
00:01:15,528 --> 00:01:17,970
du langage appelée unité lexicale ou lexème,

38
00:01:18,070 --> 00:01:21,382
par exemple, un mot-clé, un identifiant ou un symbole.

39
00:01:21,759 --> 00:01:23,118
L'analyse syntaxique,

40
00:01:23,218 --> 00:01:24,685
également appelée parsing en anglais,

41
00:01:24,785 --> 00:01:26,831
implique l'analyse de la séquence de jetons

42
00:01:26,931 --> 00:01:29,515
pour identifier la structure syntaxique du programme.

43
00:01:29,615 --> 00:01:30,950
Cette phase s'appuie généralement

44
00:01:31,050 --> 00:01:33,308
sur la construction d'un arbre d'analyse

45
00:01:33,408 --> 00:01:35,881
appelé communément arbre syntaxique.

46
00:01:36,036 --> 00:01:38,611
On remplace la séquence linéaire de jetons

47
00:01:38,711 --> 00:01:40,081
par une structure en arbre

48
00:01:40,181 --> 00:01:41,908
construite selon la grammaire formelle

49
00:01:42,008 --> 00:01:43,692
qui définit la syntaxe du langage.

50
00:01:43,792 --> 00:01:47,186
Par exemple, une condition est toujours suivie d'un test logique

51
00:01:47,286 --> 00:01:49,409
et ensuite d'une instruction, et cætera.

52
00:01:49,761 --> 00:01:51,554
L'analyse sémantique, phase durant laquelle

53
00:01:51,654 --> 00:01:53,974
le compilateur ajoute des informations sémantiques

54
00:01:54,074 --> 00:01:56,777
donc sur le sens, à l'arbre syntaxique.

55
00:01:56,877 --> 00:01:59,798
Cette phase fait un certain nombre de contrôles

56
00:01:59,898 --> 00:02:02,853
comme vérifier le type des éléments utilisés dans le programme,

57
00:02:02,953 --> 00:02:04,071
leur visibilité,

58
00:02:04,171 --> 00:02:07,349
que toutes les variables locales sont initialisées,

59
00:02:07,449 --> 00:02:08,129
et cætera.

60
00:02:09,116 --> 00:02:10,651
Les trois phases d'analyse

61
00:02:10,751 --> 00:02:12,706
construisent et complètent la table des symboles

62
00:02:12,806 --> 00:02:14,488
qui centralise les informations attachées

63
00:02:14,588 --> 00:02:16,159
aux identificateurs du programme.

64
00:02:16,630 --> 00:02:18,924
Dans cette table, on retrouve des informations

65
00:02:19,024 --> 00:02:21,026
comme le type, l'emplacement mémoire,

66
00:02:21,126 --> 00:02:22,616
la portée, la visibilité,

67
00:02:22,716 --> 00:02:23,734
et cætera.

68
00:02:23,834 --> 00:02:25,435
C'est au cours de ces phases

69
00:02:25,535 --> 00:02:27,084
que le compilateur détecte également

70
00:02:27,184 --> 00:02:29,841
les erreurs éventuelles par rapport à la définition du langage

71
00:02:29,941 --> 00:02:31,149
et produit les messages d'erreur

72
00:02:31,249 --> 00:02:32,961
permettant à l'auteur du programme

73
00:02:33,061 --> 00:02:34,782
de poursuivre sa mise au point.

74
00:02:35,048 --> 00:02:37,618
Un pré-traitement peut également être effectué

75
00:02:37,718 --> 00:02:39,477
avant, ou de façon entremêlée,

76
00:02:39,577 --> 00:02:40,954
aux phases d'analyse.

77
00:02:41,244 --> 00:02:43,116
Un tel pré-traitement est nécessaire

78
00:02:43,216 --> 00:02:44,890
dans certains langages comme C

79
00:02:44,990 --> 00:02:47,363
où il prend en charge la substitution de macros,

80
00:02:47,463 --> 00:02:49,343
la compilation conditionnelle,

81
00:02:49,443 --> 00:02:50,273
et cætera.

82
00:02:50,518 --> 00:02:53,192
Vient ensuite la génération du code proprement dite

83
00:02:53,292 --> 00:02:54,462
qui inclut trois phases,

84
00:02:55,245 --> 00:02:57,012
la transformation du code source

85
00:02:57,112 --> 00:02:58,740
en code intermédiaire,

86
00:02:58,840 --> 00:03:01,012
l'optimisation du code intermédiaire,

87
00:03:02,031 --> 00:03:04,837
c'est-à-dire rendre le programme plus efficace

88
00:03:04,937 --> 00:03:07,220
par exemple, par la suppression du code mort

89
00:03:07,320 --> 00:03:08,995
qui ne pourra jamais être exécuté,

90
00:03:09,095 --> 00:03:12,413
la sortie de boucle d'opérations indépendantes de l'itération,

91
00:03:12,513 --> 00:03:13,125
et cætera.

92
00:03:13,572 --> 00:03:15,847
Vient enfin la génération du code final.

93
00:03:16,881 --> 00:03:18,706
Dans le cas des langages interprétés,

94
00:03:18,806 --> 00:03:21,700
l'interpréteur est le logiciel utilisé

95
00:03:21,800 --> 00:03:23,341
pour analyser et exécuter les codes.

96
00:03:23,568 --> 00:03:25,710
Le cycle d'interprétation

97
00:03:25,810 --> 00:03:27,190
consiste en une succession

98
00:03:27,290 --> 00:03:29,305
analyse de l'instruction suivante,

99
00:03:29,405 --> 00:03:31,693
exécution immédiate de celle-ci.

100
00:03:31,976 --> 00:03:34,078
L'interpréteur est donc à la fois

101
00:03:34,178 --> 00:03:37,270
éditeur, compilateur et runtime.

