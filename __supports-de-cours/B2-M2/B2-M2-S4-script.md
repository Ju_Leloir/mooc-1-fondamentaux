# MOOC NSI - fondamentaux. 
## Transcription de la vidéo

## 2.2.4 : Typage des langages de programmation



Les langages de programmation sont caractérisés par :

- le fait qu'ils soient interprétés ou compilés,
- le typage des données et la déclaration des variables
- le runtime et la possibilité d'effectuer de la réflexion sur le code
- et d'autres fonctionalité avancées, comme l'utilisation de ramasse-miètes (garbage collector en anglais), la manipulation d'exceptions, d'événements, de threads, etc.

Nous avons déjà parlés de langages compilés et interprétés.

Abordons brievement les notions de typage

Un type de données est :

- Un ensemble de valeurs que peut prendre la donnée ;
- Un ensemble fini d’opérations qui peuvent lui être appliquées.
- et aussi un codage des valeurs sous forme binaire.

Chaque langage offre un certain nombre de types élémentaires prédéfinis à partir desquels l’on peut définir de nouveaux types composés ou dérivés.

Selon les langages, divers types composés sont prédéfinis.  Notons que pointeurs et références sont considérés comme des types composés.

Pointeurs et références donnent des accès à un donnée. Les pointeurs manipulent explicitement les "adresses" des données.  Avec des références, l'accès aux données (ce que l'on appelle le dérérencement) est automatique. 

Les pointeurs, définis par exemple en C ou C++, ne sont pas définis en Python.

Les références sont définies ou utilisées en C++, Java et Python.

Plus précisément, toute variable Python est un nom, implémenté sous forme d'une référence, qui donne accès à un objet.


Les règles déterminant la manière dont les types sont attribués aux entités (variables, constantes, objets, fonctions...) constituent un système de typage.

Le langage réalise un typage statique ou dynamique :

Si la détermination du type et la vérification de la faisabilité de l’opération sont effectuées dès la compilation — on parle de typage statique

Si elle est faite lors de l’exécution : on parle de typage dynamique.

Par ailleurs, le langage réalise un typage fort ou faible

Le sens de cette distinction n’est pas universellement fixé.

La majorité des auteurs qualifient de « typage fort », un système suffisamment strict et riche pour procurer une vérification sûre de la cohérence du programme écrit et l’insertion non ambiguë de conversions efficaces, ce qu’on appelle « type-safety ».

Des langages comme Java, Pascal, Ada, LISP... et même COBOL sont fortement typés. Par contre, FORTRAN et C ne le sont pas.

Python pourrait, par certains aspects, être qualifié de fortement typé, mais la possibilité d’agir de manière imprévue sur le type d’un objet (une simple assignation à un nouvel attribut inexistant dans la définition de la classe, par exemple) rend impossible la détermination a priori du (bon) sens des opérations de la suite du programme. 

On parle aussi de typage explicite ou implicite.

Avec un typage explicite, c’est à l’utilisateur d’indiquer lui-même les types qu’il utilise, par exemple lors des déclarations de variables ou de fonctions.


Au contraire, avec un système de typage implicite, le développeur laisse au compilateur ou au runtime le soin de déterminer tout seul les types de données utilisées, par exemple par inférence.

Python a donc un typage dynamique, fort et implicite puisqu'il n'y a pas de déclaration explicite des varaibales par exemple



