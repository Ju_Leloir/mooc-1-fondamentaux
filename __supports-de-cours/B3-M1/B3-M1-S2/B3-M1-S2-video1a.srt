1
00:00:01,307 --> 00:00:03,228
Dans cette vidéo, nous allons vous présenter

2
00:00:03,328 --> 00:00:04,636
quelques idées concernant

3
00:00:04,736 --> 00:00:06,145
la complexité des algorithmes.

4
00:00:06,566 --> 00:00:08,162
On va partir d'un exemple,

5
00:00:08,262 --> 00:00:11,035
on va essayer de tripatouiller cet exemple

6
00:00:11,135 --> 00:00:12,725
pour essayer de voir ce qu'il a dans le ventre,

7
00:00:13,144 --> 00:00:14,407
essayer de compter

8
00:00:14,507 --> 00:00:15,912
le nombre d'opérations qu'il va faire

9
00:00:16,012 --> 00:00:17,373
pour réaliser

10
00:00:17,614 --> 00:00:19,029
quelque chose.

11
00:00:19,387 --> 00:00:20,667
À partir de là, on va

12
00:00:20,767 --> 00:00:23,059
donner les premières définitions de complexité,

13
00:00:23,329 --> 00:00:24,594
voir quels sont

14
00:00:24,694 --> 00:00:25,958
les différents liens,

15
00:00:26,058 --> 00:00:27,019
les ordres de grandeur ;

16
00:00:27,119 --> 00:00:28,155
on va parler du grand O,

17
00:00:28,255 --> 00:00:29,580
on va parler des choses comme ça.

18
00:00:30,275 --> 00:00:31,983
Et on va se poser une question

19
00:00:32,083 --> 00:00:33,227
importante en informatique

20
00:00:33,327 --> 00:00:33,902
qu'est qu'est-ce que c'est que

21
00:00:34,002 --> 00:00:35,452
la complexité d'un problème,

22
00:00:35,708 --> 00:00:37,152
et pas d'un algorithme,

23
00:00:37,252 --> 00:00:39,399
et de voir que sur l'exemple classique

24
00:00:39,499 --> 00:00:40,199
que l'on va prendre,

25
00:00:40,472 --> 00:00:41,795
on va avoir

26
00:00:42,882 --> 00:00:44,969
le calcul de la complexité du problème

27
00:00:45,069 --> 00:00:46,478
et donc savoir que finalement,

28
00:00:46,578 --> 00:00:47,667
on n'est pas si mauvais que ça.

29
00:00:48,520 --> 00:00:50,829
Ça, c'est l'idée de toute cette séquence,

30
00:00:50,929 --> 00:00:52,836
de cette partie de séquence,

31
00:00:55,637 --> 00:01:00,104
et on s'orientera, par la suite de la séquence,

32
00:01:00,204 --> 00:01:01,472
vers la preuve,

33
00:01:01,818 --> 00:01:03,212
mais ça, ce sera une autre histoire,

34
00:01:03,312 --> 00:01:04,345
sur d'autres algorithmes.

