1
00:00:02,255 --> 00:00:06,841
Au programme de ce petit cours,

2
00:00:07,194 --> 00:00:09,820
on va parler de preuve d'algorithme

3
00:00:09,920 --> 00:00:11,525
qui consiste à effectivement

4
00:00:11,625 --> 00:00:12,864
avoir l'assurance que

5
00:00:12,964 --> 00:00:15,464
l'algorithme que l'on est en train de développer

6
00:00:15,564 --> 00:00:17,535
et que l'on a imaginé

7
00:00:17,635 --> 00:00:19,341
fournit bien le bon résultat.

8
00:00:20,794 --> 00:00:22,206
L'objectif derrière,

9
00:00:22,306 --> 00:00:23,937
c'est d'arriver à éviter les erreurs.

10
00:00:24,338 --> 00:00:26,994
Et donc nous allons voir deux idées fondamentales

11
00:00:27,094 --> 00:00:28,626
dans la preuve d'algorithme,

12
00:00:28,726 --> 00:00:29,690
la notion d'invariant

13
00:00:29,925 --> 00:00:31,531
et la notion de terminaison.

14
00:00:31,827 --> 00:00:33,514
Alors évidemment, il y a d'autres choses aussi,

15
00:00:33,614 --> 00:00:34,553
il y a des principes,

16
00:00:34,653 --> 00:00:36,226
on va reprendre ça sur des exemples

17
00:00:36,621 --> 00:00:39,778
et on va essayer de mixer tout ça

18
00:00:39,878 --> 00:00:42,634
de façon à ce que ce soit accessible pour les élèves

19
00:00:42,734 --> 00:00:45,213
et qu'on puisse avoir la rigueur nécessaire

20
00:00:45,313 --> 00:00:47,813
pour faire de la preuve d'algorithme.

21
00:00:47,913 --> 00:00:49,321
Pourquoi est-ce que l'on souhaite faire

22
00:00:49,421 --> 00:00:51,010
de la preuve d'algorithme ?

23
00:00:51,444 --> 00:00:52,855
Tout simplement parce que

24
00:00:53,102 --> 00:00:55,408
nous allons manipuler des algorithmes

25
00:00:55,508 --> 00:00:56,828
qui vont se transformer en programmes

26
00:00:56,928 --> 00:00:58,464
qui vont s'exécuter sur des machines

27
00:00:58,761 --> 00:01:03,448
et que ces machines travaillant effectivement

28
00:01:03,548 --> 00:01:04,940
avec des données,

29
00:01:05,200 --> 00:01:06,644
il est possible que

30
00:01:06,744 --> 00:01:09,329
le résultat obtenu ne soit pas tout à fait celui escompté.

31
00:01:09,627 --> 00:01:11,578
Alors, la vraie difficulté,

32
00:01:11,678 --> 00:01:13,828
c'est l'augmentation considérable

33
00:01:13,928 --> 00:01:16,162
de tout ce qui est codé actuellement

34
00:01:16,667 --> 00:01:20,289
et donc, si vous prenez des exemples classiques,

35
00:01:20,610 --> 00:01:21,552
vous prenez, je ne sais pas,

36
00:01:22,229 --> 00:01:26,556
là, j'ai pris Google Chrome ou World of Warcraft,

37
00:01:26,822 --> 00:01:28,116
vous avez six millions de lignes,

38
00:01:30,002 --> 00:01:32,527
donc derrière, il faut que ce soit correct.

39
00:01:33,563 --> 00:01:34,996
Sinon, on ne va pas s'en sortir

40
00:01:35,096 --> 00:01:37,459
et on pourrait essayer de chercher les bugs

41
00:01:37,559 --> 00:01:38,975
mais chercher les bugs,

42
00:01:39,075 --> 00:01:41,207
c'est comme chercher une aiguille dans une botte de foin

43
00:01:41,307 --> 00:01:43,389
donc il vaut mieux, au démarrage,

44
00:01:43,642 --> 00:01:45,603
avoir une qualité associée à nos programmes,

45
00:01:45,703 --> 00:01:47,007
une qualité associée à nos algorithmes

46
00:01:47,107 --> 00:01:48,780
qui nous garantit qu'effectivement

47
00:01:48,880 --> 00:01:49,707
il n'y a pas trop d'erreurs.

48
00:01:50,279 --> 00:01:52,055
Alors, dans tous les systèmes,

49
00:01:52,155 --> 00:01:54,725
vous avez effectivement des erreurs qui peuvent se produire.

50
00:01:54,953 --> 00:01:57,587
Les erreurs qui sont des bugs de programmation,

51
00:01:57,760 --> 00:01:59,681
des dépassements de capacité

52
00:01:59,781 --> 00:02:01,096
sont plutôt faciles à détecter,

53
00:02:01,196 --> 00:02:03,362
mais les erreurs de méthode sont dramatiques.

54
00:02:03,944 --> 00:02:05,708
Si on se trompe dans l'algorithme

55
00:02:05,808 --> 00:02:06,372
en tant que tel,

56
00:02:06,713 --> 00:02:09,083
ça sera faux ad vitam aeternam

57
00:02:09,183 --> 00:02:10,581
même si vous avez bien codé.

58
00:02:11,121 --> 00:02:13,408
Donc il faut vraiment faire très, très attention

59
00:02:13,746 --> 00:02:16,182
à la partie preuve d'algorithme,

60
00:02:16,713 --> 00:02:18,076
que la méthode soit correcte

61
00:02:18,176 --> 00:02:20,186
et correcte de A à Z.

62
00:02:20,456 --> 00:02:22,192
Si on continue sur cette idée,

63
00:02:23,165 --> 00:02:25,442
prouver qu'un algorithme est correct,

64
00:02:26,310 --> 00:02:27,691
ça veut dire qu'on a réfléchi

65
00:02:27,791 --> 00:02:30,280
à ce que devait faire effectivement l'algorithme.

66
00:02:30,989 --> 00:02:32,880
Et ça veut dire qu'on a des critères

67
00:02:32,980 --> 00:02:35,283
pour établir ce que c'est qu'un algorithme correct.

68
00:02:36,478 --> 00:02:38,399
Si vous faites une recette de madeleines,

69
00:02:38,499 --> 00:02:39,615
elles peuvent être plus ou moins cuites,

70
00:02:39,715 --> 00:02:40,845
on dira quand même que c'est correct

71
00:02:40,945 --> 00:02:42,191
si vous pouvez l'envoyer à la vente.

72
00:02:42,763 --> 00:02:44,588
Donc la question qui est derrière,

73
00:02:44,688 --> 00:02:47,170
c'est comment on va considérer ça

74
00:02:47,470 --> 00:02:50,009
et comment on va, je dirais, décrire

75
00:02:50,293 --> 00:02:52,361
l'aspect correction d'un algorithme

76
00:02:52,643 --> 00:02:56,763
sur notre algorithme.

77
00:02:57,881 --> 00:02:59,767
Pour partir de là,

78
00:02:59,867 --> 00:03:01,309
on va partir d'une spécification

79
00:03:02,197 --> 00:03:04,203
qui va nous dire les données acceptables

80
00:03:04,303 --> 00:03:06,501
que vous mettez en entrée de cet algorithme,

81
00:03:07,407 --> 00:03:10,296
ces données acceptables vont fournir un résultat

82
00:03:10,396 --> 00:03:13,314
 et vous avez une description du résultat attendu

83
00:03:13,549 --> 00:03:16,600
et tout ça, c'est exprimé dans un certain langage.

84
00:03:16,832 --> 00:03:18,755
Ça peut être le langage naturel

85
00:03:22,464 --> 00:03:24,436
si vous avez juste une description,

86
00:03:24,981 --> 00:03:26,970
mais ça peut être aussi un langage formel

87
00:03:27,223 --> 00:03:29,200
si vous vous posez la question

88
00:03:29,938 --> 00:03:32,355
voilà, je vais décrire

89
00:03:33,337 --> 00:03:35,031
explicitement comment sont mes données,

90
00:03:35,131 --> 00:03:36,086
mes entrées, mes sorties,

91
00:03:36,186 --> 00:03:37,254
j'ai une propriété logique

92
00:03:37,840 --> 00:03:40,235
et je vais vérifier cette propriété logique.

93
00:03:40,620 --> 00:03:42,687
Alors la chose qui est difficile à comprendre,

94
00:03:42,787 --> 00:03:45,008
c'est qu'une preuve ne décrit pas

95
00:03:45,108 --> 00:03:46,345
comment fonctionne l'algorithme.

96
00:03:47,186 --> 00:03:48,722
Par contre, pour faire la preuve,

97
00:03:48,822 --> 00:03:49,993
vous avez besoin de savoir

98
00:03:50,093 --> 00:03:51,845
comment fonctionne l'algorithme.

99
00:03:52,803 --> 00:03:55,221
Alors, deux propriétés qui sont importantes,

100
00:03:55,321 --> 00:03:56,222
qui sont recherchées.

101
00:03:56,480 --> 00:03:59,033
La première, c'est l'exécution de l'algorithme

102
00:03:59,581 --> 00:04:01,334
fournit-elle en temps fini

103
00:04:02,080 --> 00:04:03,656
quelles que soient les données en entrée

104
00:04:03,756 --> 00:04:06,378
un résultat ?

105
00:04:06,888 --> 00:04:08,846
Ça, c'est la notion de terminaison.

106
00:04:10,036 --> 00:04:11,420
Cette notion est importante

107
00:04:11,520 --> 00:04:14,350
parce que, dans bon nombre de programmes

108
00:04:14,450 --> 00:04:15,603
que vous verrez avec vos élèves,

109
00:04:16,163 --> 00:04:18,852
vous avez des boucles infinies

110
00:04:19,453 --> 00:04:21,514
qui font que l'algorithme ne termine jamais

111
00:04:21,614 --> 00:04:22,745
donc vous n'avez pas le résultat.

112
00:04:23,724 --> 00:04:24,839
Et le deuxième point,

113
00:04:24,939 --> 00:04:26,578
c'est donc la correction partielle,

114
00:04:26,920 --> 00:04:28,930
c'est-à-dire lorsque l'algorithme s'arrête,

115
00:04:29,185 --> 00:04:30,604
le résultat calculé

116
00:04:30,704 --> 00:04:32,100
est-il la solution cherchée

117
00:04:32,200 --> 00:04:33,669
quelles que soient les données fournies.

118
00:04:34,336 --> 00:04:36,354
Ce sont donc les deux aspects

119
00:04:36,670 --> 00:04:37,757
très, très différents,

120
00:04:37,857 --> 00:04:40,250
un qui porte sur la dynamique du programme

121
00:04:40,526 --> 00:04:44,126
et l'autre qui porte sur l'état de sortie du programme.

122
00:04:45,015 --> 00:04:47,839
Et si on prend les deux ensemble,

123
00:04:47,939 --> 00:04:48,852
on obtient ce qu'on appelle

124
00:04:48,952 --> 00:04:50,648
la terminaison et correction partielle

125
00:04:50,748 --> 00:04:51,887
qui donnent la correction totale.

126
00:04:52,434 --> 00:04:55,335
Et là, quelles que soient les données en entrée

127
00:04:55,663 --> 00:04:56,988
à l'algorithme,

128
00:04:57,088 --> 00:04:59,051
si les données sont conformes

129
00:04:59,151 --> 00:05:00,342
à la spécification,

130
00:05:00,442 --> 00:05:02,703
alors la réponse de l'algorithme est correcte.

131
00:05:04,064 --> 00:05:06,363
Alors attention, il y a aussi

132
00:05:06,463 --> 00:05:08,892
des algorithmes qui sont plus compliqués à prouver

133
00:05:09,189 --> 00:05:10,742
et qui ne sont pas forcément

134
00:05:10,842 --> 00:05:12,049
avec une terminaison.

135
00:05:12,392 --> 00:05:15,513
On va dire que ces algorithmes sont partiellement corrects.

136
00:05:15,742 --> 00:05:17,247
Des exemples typiques sont

137
00:05:17,347 --> 00:05:18,904
les algorithmes systèmes qui sont faits

138
00:05:19,004 --> 00:05:21,282
pour être exécutés indéfiniment

139
00:05:21,661 --> 00:05:22,943
et ne se terminent jamais.

140
00:05:23,043 --> 00:05:23,851
Mais ce n'est pas grave,

141
00:05:24,103 --> 00:05:26,087
la question est

142
00:05:27,646 --> 00:05:30,663
je dirais, de montrer la correction partielle

143
00:05:30,991 --> 00:05:32,828
qui va assurer que l'algorithme

144
00:05:33,809 --> 00:05:35,192
va faire son travail.

145
00:05:35,860 --> 00:05:39,426
Alors, comment est-ce qu'on va écrire ça formellement ?

146
00:05:39,526 --> 00:05:41,930
Je fais ici du pseudo-formel

147
00:05:42,030 --> 00:05:44,888
c'est-à-dire que je vais essayer de poser les choses,

148
00:05:44,988 --> 00:05:47,250
de ne pas utiliser un langage de spécification,

149
00:05:47,350 --> 00:05:48,227
il en existe,

150
00:05:51,338 --> 00:05:52,594
et de juste dire exactement

151
00:05:52,694 --> 00:05:53,824
ce qui est attendu

152
00:05:54,105 --> 00:05:56,930
au niveau d'un CAPES.

153
00:05:57,244 --> 00:05:59,992
Alors, si j'ai un problème P

154
00:06:00,092 --> 00:06:01,714
qui est instancié par une donnée D

155
00:06:01,814 --> 00:06:03,991
et la réponse est fournie par un résultat R,

156
00:06:04,486 --> 00:06:06,523
en fait, l'algorithme,

157
00:06:06,623 --> 00:06:07,734
c'est ce qui va associer

158
00:06:07,834 --> 00:06:10,072
à la donnée D  le résultat R.

159
00:06:10,600 --> 00:06:12,986
Donc une spécification, c'est quoi ?

160
00:06:13,342 --> 00:06:16,817
C'est, étant donnée une propriété que va vérifier la donnée,

161
00:06:16,917 --> 00:06:18,339
ce qu'on appelle une précondition,

162
00:06:18,743 --> 00:06:20,678
je me donne une propriété

163
00:06:20,778 --> 00:06:22,630
qui va être l'association

164
00:06:22,958 --> 00:06:24,385
entre la donnée d'entrée

165
00:06:24,485 --> 00:06:25,512
et le résultat obtenu,

166
00:06:25,788 --> 00:06:27,881
et ça, c'est ce qu'on appelle la postcondition.

167
00:06:28,133 --> 00:06:30,004
Et donc si P(D) est vraie,

168
00:06:30,104 --> 00:06:32,783
alors Q(D, R) doit être vraie.

169
00:06:33,308 --> 00:06:38,417
Ça, c'est la notion de satisfaire une spécification.

170
00:06:39,177 --> 00:06:42,304
Le programme est dans ce cas-là dit correct

171
00:06:42,404 --> 00:06:44,399
par rapport à cette spécification.

172
00:06:44,499 --> 00:06:45,161
C'est-à-dire que

173
00:06:45,395 --> 00:06:47,203
on peut prouver des bouts de programme,

174
00:06:47,303 --> 00:06:48,263
des bouts d'algorithme,

175
00:06:48,556 --> 00:06:50,139
par rapport à des spécifications ;

176
00:06:50,391 --> 00:06:52,721
on se réfère en permanence à une spécification.

177
00:06:53,652 --> 00:06:55,182
Exemple très simple,

178
00:06:55,282 --> 00:06:58,050
qui est utilisé dès l'école élémentaire,

179
00:06:58,364 --> 00:07:00,959
je veux diviser a par b,

180
00:07:01,257 --> 00:07:03,024
donc j'ai deux entiers a et b,

181
00:07:03,337 --> 00:07:05,266
et j'ai envie d'avoir q le quotient

182
00:07:05,366 --> 00:07:07,693
et r le reste de la division euclidienne de a par b.

183
00:07:07,938 --> 00:07:10,255
C'est la division qu'on fait dans les petites classes.

184
00:07:10,900 --> 00:07:13,009
Alors, comme je ne suis pas très, très fort,

185
00:07:13,291 --> 00:07:16,230
je vais procéder par soustractions successives.

186
00:07:16,455 --> 00:07:18,025
C'est-à-dire que je vais prendre

187
00:07:18,302 --> 00:07:20,347
a, je vais lui soustraire b,

188
00:07:20,447 --> 00:07:22,890
et je vais lui soustraire b, puis je vais lui soustraire b,

189
00:07:23,339 --> 00:07:24,632
jusqu'à ce que j'arrive

190
00:07:24,929 --> 00:07:25,992
à mon résultat.

191
00:07:26,292 --> 00:07:30,821
Si j'écris ça de façon propre,

192
00:07:30,921 --> 00:07:32,896
par exemple, j'ai envie de diviser

193
00:07:33,620 --> 00:07:37,271
7 par 3,

194
00:07:38,578 --> 00:07:42,788
je fais 7 moins 3,

195
00:07:45,170 --> 00:07:48,572
il reste 4.

196
00:07:49,267 --> 00:07:52,899
Je fais 4 moins 3,

197
00:07:53,255 --> 00:07:54,937
il reste 1.

198
00:07:55,252 --> 00:07:58,153
1 est plus petit que 3

199
00:07:58,253 --> 00:08:00,114
donc je vais dire que je vais avoir

200
00:08:00,453 --> 00:08:01,964
soustrait deux fois

201
00:08:02,281 --> 00:08:03,182
ici.

202
00:08:06,399 --> 00:08:08,872
Et je vais avoir un reste

203
00:08:09,039 --> 00:08:11,067
qui est égal à 1.

204
00:08:12,744 --> 00:08:14,914
Donc l'idée, c'est de faire un programme

205
00:08:15,014 --> 00:08:16,329
qui soit le plus simple possible,

206
00:08:16,429 --> 00:08:17,919
un algorithme qui soit le plus simple possible,

207
00:08:18,019 --> 00:08:19,239
qui réalise cette division.

208
00:08:19,560 --> 00:08:21,349
Ça, ça permet de raisonner.

209
00:08:21,634 --> 00:08:23,143
J'aime bien cet exemple

210
00:08:23,243 --> 00:08:24,247
parce que cet exemple

211
00:08:25,194 --> 00:08:28,525
n'a pas de difficulté de nature mathématique,

212
00:08:28,625 --> 00:08:30,957
je fais juste des soustractions successives.

213
00:08:31,625 --> 00:08:34,554
Alors continuons dans cet esprit-là,

214
00:08:34,654 --> 00:08:35,982
qu'est-ce que je dois supposer ?

215
00:08:36,239 --> 00:08:38,275
C'est-à-dire quelles sont les propriétés

216
00:08:38,375 --> 00:08:40,475
que doivent vérifier les deux entiers a et b ?

217
00:08:41,041 --> 00:08:43,042
Alors tout de suite, se poser cette question,

218
00:08:43,387 --> 00:08:45,312
c'est une bonne introduction

219
00:08:45,412 --> 00:08:46,387
parce qu'on se dit  ah oui,

220
00:08:46,487 --> 00:08:47,543
il faut quand même que

221
00:08:47,840 --> 00:08:49,939
b soit non nul,

222
00:08:50,270 --> 00:08:52,033
strictement différent de 0,

223
00:08:52,342 --> 00:08:53,409
parce que sinon, je n'ai pas de sens,

224
00:08:53,509 --> 00:08:54,676
je ne peux pas diviser par 0.

225
00:08:54,971 --> 00:08:58,460
Et puis je vais me retrouver

226
00:08:58,560 --> 00:09:00,118
avec une boucle

227
00:09:00,218 --> 00:09:01,890
qui va être infinie,

228
00:09:01,990 --> 00:09:03,408
j'ai r qui est toujours plus grand que 0,

229
00:09:04,528 --> 00:09:06,293
ça va tourner indéfiniment,

230
00:09:06,393 --> 00:09:07,529
donc là, j'ai un problème.

231
00:09:07,629 --> 00:09:08,508
Donc il faut vraiment,

232
00:09:08,804 --> 00:09:10,826
pour que mon problème ait du sens,

233
00:09:10,926 --> 00:09:12,204
que b soit différent de 0.

234
00:09:12,542 --> 00:09:16,018
Alors je vais prendre b strictement plus grand que 0

235
00:09:16,697 --> 00:09:18,274
mais est-ce que je peux prendre b négatif ?

236
00:09:20,013 --> 00:09:22,057
Là aussi, c'est une question qui peut se poser.

237
00:09:23,274 --> 00:09:25,537
Est-ce que je prends toujours un positif ?

238
00:09:25,831 --> 00:09:27,924
Et donc une des premières façons de faire,

239
00:09:28,024 --> 00:09:29,909
c'est de se dire on va simplifier les choses,

240
00:09:30,268 --> 00:09:31,884
je vais prendre une spécification,

241
00:09:31,984 --> 00:09:33,845
je vais faire la division euclidienne

242
00:09:34,156 --> 00:09:36,396
de nombres entiers positifs,

243
00:09:36,621 --> 00:09:39,589
et ça va m'éviter d'avoir un problème quelconque.

244
00:09:40,119 --> 00:09:42,066
Alors après, quel est le résultat attendu ?

245
00:09:42,166 --> 00:09:43,887
Le résultat attendu, c'est

246
00:09:44,179 --> 00:09:46,081
d'avoir a qui s'exprime comme

247
00:09:46,181 --> 00:09:48,080
le quotient fois b plus le reste,

248
00:09:48,375 --> 00:09:51,124
mais en plus, dans la division euclidienne,

249
00:09:51,224 --> 00:09:53,296
le reste doit être compris entre 0

250
00:09:53,440 --> 00:09:55,278
et strictement plus petit que b.

251
00:09:55,730 --> 00:09:58,294
C'est ça qui fait la vraie division.

252
00:09:59,158 --> 00:10:01,594
a et b doivent être inchangés.

253
00:10:03,621 --> 00:10:05,353
Évidemment, ça aussi,

254
00:10:05,453 --> 00:10:08,721
c'est quelque chose auquel je dois faire attention.

255
00:10:08,998 --> 00:10:11,391
Si je manipule des données comme a et b,

256
00:10:11,491 --> 00:10:13,670
je dois faire attention

257
00:10:13,770 --> 00:10:15,399
à fournir le résultat.

258
00:10:16,292 --> 00:10:18,577
Alors, comment est-ce qu'on va démontrer

259
00:10:18,677 --> 00:10:23,257
ce type d'algorithme ?

260
00:10:23,512 --> 00:10:25,446
En fait, l'idée de la démonstration,

261
00:10:25,546 --> 00:10:27,590
c'est de faire ça de proche en proche,

262
00:10:27,851 --> 00:10:29,707
établir que la postcondition est vraie

263
00:10:29,807 --> 00:10:31,440
chaque fois que la précondition est vraie.

264
00:10:31,774 --> 00:10:34,249
C'est l'idée d'implication

265
00:10:34,349 --> 00:10:37,199
à l'intérieur de notre algorithme.

266
00:10:38,115 --> 00:10:42,885
Si j'ai juste des affectations qui sont les unes derrière les autres,

267
00:10:43,161 --> 00:10:44,959
pas de problème, ça se passe bien,

268
00:10:45,059 --> 00:10:46,223
je vais pouvoir vérifier

269
00:10:46,323 --> 00:10:48,104
si a vérifie ça au début,

270
00:10:48,204 --> 00:10:49,858
alors a vérifie ça après, et cætera.

271
00:10:51,367 --> 00:10:53,077
Si j'ai une séquence, ça marche,

272
00:10:53,177 --> 00:10:55,218
si j'ai des conditions, c'est un peu plus compliqué

273
00:10:55,318 --> 00:10:56,870
mais en gros, c'est plutôt facile.

274
00:10:57,239 --> 00:10:58,543
Qu'est-ce qu'il se passe

275
00:10:58,643 --> 00:11:00,049
lorsque j'ai une itération ?

276
00:11:00,341 --> 00:11:02,131
Lorsque j'ai une itération en fait,

277
00:11:02,231 --> 00:11:03,911
j'ai quelque chose qui re-rentre

278
00:11:04,011 --> 00:11:05,060
à l'intérieur du bloc.

279
00:11:05,315 --> 00:11:06,055
Et dans ce cas-là,

280
00:11:06,155 --> 00:11:09,098
mes valeurs de variables vont changer.

281
00:11:09,350 --> 00:11:10,884
Donc c'est difficile de dire

282
00:11:10,984 --> 00:11:12,282
quelle est la valeur de la variable,

283
00:11:12,382 --> 00:11:14,586
il faut tenir compte de l'itération

284
00:11:14,686 --> 00:11:16,759
et du pas de l'itération dans laquelle vous êtes.

285
00:11:17,964 --> 00:11:20,397
Alors on va appeler invariant

286
00:11:21,179 --> 00:11:24,809
une propriété sur les valeurs des variables

287
00:11:25,543 --> 00:11:29,070
qui, si elle est vraie en début de boucle,

288
00:11:29,312 --> 00:11:31,526
elle est vérifiée en fin de boucle

289
00:11:31,626 --> 00:11:33,869
c'est-à-dire au début de l'itération suivante.

290
00:11:34,770 --> 00:11:37,748
C'est ça qui va être la notion importante,

291
00:11:38,134 --> 00:11:40,298
c'est-à-dire comment une propriété

292
00:11:40,533 --> 00:11:44,109
se propage à l'intérieur d'une boucle.

293
00:11:44,407 --> 00:11:46,340
Ce n'est vraiment pas compliqué.

294
00:11:46,693 --> 00:11:48,800
C'est juste bien comprendre

295
00:11:48,900 --> 00:11:50,210
comment fonctionne la boucle

296
00:11:50,539 --> 00:11:52,663
et comment une propriété se propage

297
00:11:52,763 --> 00:11:53,929
à l'intérieur de la boucle.

298
00:11:56,614 --> 00:11:58,426
Alors la méthodologie derrière,

299
00:11:58,526 --> 00:11:59,705
c'est choisir

300
00:12:00,261 --> 00:12:02,258
un invariant judicieux,

301
00:12:02,358 --> 00:12:04,178
c'est-à-dire un invariant qui vous permette

302
00:12:04,560 --> 00:12:06,800
de se caler sur la précondition,

303
00:12:06,900 --> 00:12:08,532
de se caler sur la postcondition

304
00:12:08,632 --> 00:12:10,505
et à la fin d'avoir la preuve de votre programme.

305
00:12:11,479 --> 00:12:14,315
Une fois que vous avez cet invariant,

306
00:12:14,593 --> 00:12:16,289
il vous faut démontrer que c'est vrai

307
00:12:16,616 --> 00:12:18,084
avant d'entrer dans la boucle,

308
00:12:18,669 --> 00:12:20,504
démontrer que s'il est vérifié

309
00:12:20,604 --> 00:12:22,371
au début d'une itération,

310
00:12:22,582 --> 00:12:23,477
quelle qu'elle soit,

311
00:12:23,577 --> 00:12:25,595
alors il est vrai en fin d'itération,

312
00:12:25,695 --> 00:12:28,611
c'est-à-dire en début d'itération suivante,

313
00:12:32,658 --> 00:12:35,319
et puis, lorsque je vais sortir de ma boucle,

314
00:12:35,419 --> 00:12:36,866
la condition va être fausse,

315
00:12:37,153 --> 00:12:38,272
d'entrée dans ma boucle,

316
00:12:38,571 --> 00:12:40,287
donc là, je dois vérifier que

317
00:12:40,387 --> 00:12:42,728
ça va bien me donner la postcondition attendue

318
00:12:43,007 --> 00:12:44,623
c'est-à-dire prouver que ça va bien marcher.

319
00:12:45,239 --> 00:12:47,742
Alors, comment est-ce qu'on choisit

320
00:12:47,842 --> 00:12:49,114
un bon invariant ?

321
00:12:49,214 --> 00:12:50,673
Pour choisir un bon invariant,

322
00:12:50,773 --> 00:12:51,685
ce n'est pas facile

323
00:12:51,941 --> 00:12:55,294
et il n'y a pas de méthode systématique.

324
00:12:55,394 --> 00:12:57,459
Souvent, on va choisir l'invariant

325
00:12:57,694 --> 00:13:00,204
en fonction de ce qu'on a envie de faire comme code,

326
00:13:00,304 --> 00:13:02,045
en fonction de notre algorithme,

327
00:13:02,145 --> 00:13:03,231
en fonction des variables,

328
00:13:03,331 --> 00:13:05,055
en fonction de ce qu'on a compris de l'algorithme.

329
00:13:06,229 --> 00:13:09,152
Une fois que l'on a effectivement un premier invariant,

330
00:13:09,459 --> 00:13:11,456
on va utiliser les préconditions

331
00:13:11,737 --> 00:13:13,120
pour vérifier que

332
00:13:13,220 --> 00:13:16,703
l'invariant est vérifié en début d'itération

333
00:13:17,341 --> 00:13:18,797
et puis on va propager,

334
00:13:18,897 --> 00:13:20,120
dans le corps de la boucle,

335
00:13:21,245 --> 00:13:23,132
la propriété,

336
00:13:23,452 --> 00:13:25,680
et on va regarder quelles sont les valeurs des variables

337
00:13:25,780 --> 00:13:27,083
à la fin de la boucle.

338
00:13:27,781 --> 00:13:30,110
Alors, pour des questions de notation,

339
00:13:30,311 --> 00:13:32,085
vous voyez qu'une même variable x

340
00:13:32,185 --> 00:13:33,461
en début et en fin de boucle

341
00:13:33,561 --> 00:13:35,100
peut prendre des valeurs différentes.

342
00:13:36,434 --> 00:13:38,153
Donc, par tradition,

343
00:13:38,432 --> 00:13:40,374
on utilise le prime pour dire

344
00:13:40,474 --> 00:13:41,882
la valeur de la variable

345
00:13:41,982 --> 00:13:43,584
en fin d'itération.

346
00:13:45,807 --> 00:13:49,399
Et enfin, pour trouver la postcondition,

347
00:13:49,648 --> 00:13:51,997
on dit que l'on ne rentre plus dans la boucle while

348
00:13:52,232 --> 00:13:52,864
ou repeat,

349
00:13:53,527 --> 00:13:56,130
donc la condition n'est pas vérifiée

350
00:13:56,414 --> 00:13:58,484
et le fait que ce ne soit pas vérifié

351
00:13:58,816 --> 00:14:01,259
ajouté à la propriété d'invariant

352
00:14:01,524 --> 00:14:03,180
va vous donner la sortie,

353
00:14:03,490 --> 00:14:06,996
la postcondition de votre bloc

354
00:14:07,096 --> 00:14:08,647
et donc, ça va vous donner

355
00:14:08,894 --> 00:14:10,289
la correction du programme.

356
00:14:11,039 --> 00:14:13,231
Alors reprenons notre division par soustractions.

357
00:14:14,816 --> 00:14:15,758
Voilà l'exemple.

358
00:14:16,015 --> 00:14:18,510
J'ai a et b des entiers positifs,

359
00:14:18,610 --> 00:14:20,289
je vais prendre des préconditions

360
00:14:20,543 --> 00:14:22,697
et ma postcondition va être

361
00:14:22,797 --> 00:14:24,220
a = bq + r

362
00:14:24,479 --> 00:14:27,858
et 0 est plus petit que r est plus petit que b,

363
00:14:27,958 --> 00:14:28,857
a et b inchangés.

364
00:14:29,674 --> 00:14:31,111
Mes préconditions, ça va être

365
00:14:31,211 --> 00:14:33,834
a positif et b strictement positif

366
00:14:34,085 --> 00:14:36,669
et donc je vais essayer d'itérer à l'intérieur.

367
00:14:37,622 --> 00:14:40,122
Alors quel invariant puis-je considérer ?

368
00:14:40,562 --> 00:14:42,342
En fait, je vais voir,

369
00:14:42,661 --> 00:14:44,863
j'ai la valeur de la variable a,

370
00:14:44,963 --> 00:14:46,108
la valeur de la variable b,

371
00:14:46,208 --> 00:14:48,174
j'ai deux autres variables, q et r.

372
00:14:48,274 --> 00:14:50,327
Et je vais regarder les valeurs de ces variables.

373
00:14:50,810 --> 00:14:54,091
Donc si je reprends le schéma qui est ici,

374
00:14:57,865 --> 00:15:00,147
là, ça veut dire que la valeur

375
00:15:01,015 --> 00:15:05,933
de la variable a,

376
00:15:06,280 --> 00:15:09,237
donc ça, c'est cette partie-là,

377
00:15:10,658 --> 00:15:14,442
est égale à la valeur de la variable b

378
00:15:14,747 --> 00:15:17,138
fois la valeur de la variable q

379
00:15:17,438 --> 00:15:19,938
plus la valeur de la variable r.

380
00:15:20,864 --> 00:15:23,250
Donc, il faut bien comprendre que en rouge,

381
00:15:23,543 --> 00:15:26,034
ce sont les valeurs des variables,

382
00:15:26,134 --> 00:15:28,458
ce ne sont pas les noms des variables.

383
00:15:28,760 --> 00:15:30,899
Dans la partie gauche,

384
00:15:31,366 --> 00:15:34,712
ici, r, q, a, b

385
00:15:35,031 --> 00:15:38,391
sont des noms de variables.

386
00:15:39,130 --> 00:15:41,508
Donc il y a une vraie différence entre les deux

387
00:15:41,759 --> 00:15:44,439
et souvent, c'est ça qui est compliqué à comprendre

388
00:15:44,539 --> 00:15:46,240
pour les élèves.

389
00:15:46,984 --> 00:15:49,044
Et puis, je vais me poser la question

390
00:15:49,144 --> 00:15:50,782
maintenant en fin d'itération,

391
00:15:51,559 --> 00:15:53,528
quelles sont les valeurs des variables ?

392
00:15:54,221 --> 00:15:55,586
Alors a et b

393
00:15:55,686 --> 00:15:58,219
ne sont pas modifiées par le corps de l'itération.

394
00:15:58,535 --> 00:16:01,254
Par contre, r et q sont modifiées

395
00:16:01,640 --> 00:16:03,565
à la fin de l'itération.

396
00:16:03,858 --> 00:16:05,295
Donc ce que je dois calculer,

397
00:16:05,587 --> 00:16:08,281
c'est b fois la nouvelle valeur de q

398
00:16:08,548 --> 00:16:10,654
plus la nouvelle valeur de r,

399
00:16:10,964 --> 00:16:15,738
c'est cette partie ici,

400
00:16:16,231 --> 00:16:18,557
et je dois calculer ceci

401
00:16:18,657 --> 00:16:20,878
et montrer que ça correspond bien à ce qu'il me faut.

402
00:16:21,316 --> 00:16:23,015
Alors je remplace.

403
00:16:23,375 --> 00:16:28,204
La valeur de q', c'est la valeur de q plus 1

404
00:16:28,304 --> 00:16:31,230
à cause de ceci

405
00:16:31,611 --> 00:16:32,978
qui donne ça,

406
00:16:33,425 --> 00:16:37,158
r donne r - b,

407
00:16:37,258 --> 00:16:39,726
c'est donné par cette instruction-là,

408
00:16:39,995 --> 00:16:42,850
et donc in fine après un peu d'algèbre,

409
00:16:43,178 --> 00:16:48,031
j'obtiens que j'ai b fois q plus b moins b plus r,

410
00:16:48,131 --> 00:16:50,916
j'obtiens finalement b fois q plus r

411
00:16:51,016 --> 00:16:53,027
c'est-à-dire les valeurs initiales que j'avais

412
00:16:53,127 --> 00:16:55,658
en entrée de boucle de a, b, q, et cætera

413
00:16:55,928 --> 00:16:57,313
et cette quantité-là,

414
00:16:57,621 --> 00:17:00,026
si mon invariant est vérifié

415
00:17:00,126 --> 00:17:01,330
en début de boucle,

416
00:17:01,430 --> 00:17:02,972
il est encore vérifié

417
00:17:03,072 --> 00:17:04,951
en fin d'itération,

418
00:17:05,332 --> 00:17:06,554
donc en fin de bloc.

419
00:17:09,968 --> 00:17:12,435
À la fin de ce bloc-là,

420
00:17:14,227 --> 00:17:15,688
ici,

421
00:17:15,788 --> 00:17:19,046
j'ai la valeur de la variable b

422
00:17:19,146 --> 00:17:20,744
fois la valeur de la variable q

423
00:17:20,844 --> 00:17:22,342
plus la valeur de la variable r

424
00:17:22,442 --> 00:17:23,243
est égal à a.

425
00:17:23,611 --> 00:17:25,217
C'est un peu long à dire.

426
00:17:25,580 --> 00:17:27,786
Et donc, in fine,

427
00:17:27,886 --> 00:17:31,155
j'obtiens a égale b fois q plus r.

428
00:17:31,493 --> 00:17:33,391
À quel moment je vais avoir ça ?

429
00:17:33,491 --> 00:17:35,437
Quand je vais sortir de mon itération

430
00:17:35,769 --> 00:17:37,089
et à ce moment-là,

431
00:17:37,388 --> 00:17:39,309
qu'est-ce qui est vérifié ?

432
00:17:40,302 --> 00:17:43,572
Ici, j'ai r qui est supérieur ou égal à b.

433
00:17:43,672 --> 00:17:45,724
Quand je sors de mon itération,

434
00:17:46,066 --> 00:17:47,019
qu'est-ce qu'il se passe ?

435
00:17:47,119 --> 00:17:50,349
J'ai r qui est strictement plus petit que b.

436
00:17:51,613 --> 00:17:54,662
Donc je montre bien que r est plus petit que b

437
00:17:54,762 --> 00:17:56,475
et a et b sont inchangés.

438
00:17:57,450 --> 00:18:00,610
Il manque un petit truc.

439
00:18:01,041 --> 00:18:02,435
Il manque un petit truc qui est

440
00:18:03,465 --> 00:18:05,425
je dois aussi montrer que

441
00:18:05,725 --> 00:18:08,383
0 est toujours inférieur ou égal à r

442
00:18:08,483 --> 00:18:10,192
pour que ma propriété soit vérifiée.

443
00:18:14,855 --> 00:18:16,778
Et donc, qu'est-ce que je vais devoir faire ?

444
00:18:16,878 --> 00:18:22,811
Je vais enrichir mon invariant

445
00:18:23,108 --> 00:18:24,750
et je vais dire ici que

446
00:18:24,850 --> 00:18:30,239
r est toujours supérieur ou égal à 0.

447
00:18:30,717 --> 00:18:33,654
Alors r est supérieur ou égal à 0.

448
00:18:33,958 --> 00:18:38,758
Est-ce que r' est supérieur ou égal à 0 ?

449
00:18:40,361 --> 00:18:42,699
Oui, parce que j'ai r qui est,

450
00:18:43,214 --> 00:18:44,603
quand je suis dans ma boucle,

451
00:18:44,703 --> 00:18:46,486
j'ai r qui est plus grand que b,

452
00:18:46,784 --> 00:18:49,684
r prend la valeur r moins b

453
00:18:49,784 --> 00:18:52,427
donc r' égale r  moins b

454
00:18:52,753 --> 00:18:54,339
et donc, en entrée de boucle,

455
00:18:54,439 --> 00:18:55,646
j'ai r plus grand que b

456
00:18:55,746 --> 00:18:57,300
et r' égale r moins b,

457
00:18:57,579 --> 00:19:00,790
donc en sortie du bloc de l'itération,

458
00:19:00,890 --> 00:19:02,616
j'ai r' qui est plus grand que 0.

459
00:19:02,917 --> 00:19:05,133
Et donc la deuxième partie de mon inégalité

460
00:19:05,233 --> 00:19:06,239
est également vérifiée.

461
00:19:06,582 --> 00:19:09,057
Vous voyez que là, j'ai raisonné en deux étapes.

462
00:19:09,365 --> 00:19:10,688
J'ai raisonné en une étape

463
00:19:12,414 --> 00:19:13,933
pour regarder mon invariant général,

464
00:19:14,033 --> 00:19:14,922
et puis je me suis rendu compte

465
00:19:15,022 --> 00:19:16,307
que je devais aussi vérifier

466
00:19:16,668 --> 00:19:18,711
que r était plus grand que 0.

467
00:19:20,925 --> 00:19:21,971
Alors, évidemment,

468
00:19:22,071 --> 00:19:23,273
c'est ce qu'il reste à démontrer,

469
00:19:23,373 --> 00:19:25,695
a et b sont inchangés

470
00:19:26,404 --> 00:19:29,573
et donc j'ai, pour partie,

471
00:19:30,408 --> 00:19:31,726
ici démontré mon algorithme.

