1
00:00:01,504 --> 00:00:02,794
Quel est le problème initial ?

2
00:00:02,894 --> 00:00:05,140
En fait, le premier problème qui est important,

3
00:00:05,240 --> 00:00:06,659
qui est caché derrière

4
00:00:06,759 --> 00:00:09,151
et sur lequel effectivement on n'insiste pas trop

5
00:00:09,251 --> 00:00:11,710
et qui est un des problèmes fondamentaux de l'informatique,

6
00:00:11,885 --> 00:00:14,534
c'est comment je fais pour retrouver une information

7
00:00:14,634 --> 00:00:18,001
dans toute l'information que j'ai à ma disposition.

8
00:00:18,367 --> 00:00:21,244
Donc le problème, tel que je le spécifie ici,

9
00:00:21,344 --> 00:00:23,222
c'est que j'ai un ensemble d'éléments

10
00:00:24,102 --> 00:00:25,660
les éléments ont des clés,

11
00:00:26,049 --> 00:00:27,399
c'est ce qu'on appelle des clés,

12
00:00:27,499 --> 00:00:28,670
c'est-à-dire des identifiants,

13
00:00:28,770 --> 00:00:30,612
c'est-à-dire des propriétés,

14
00:00:30,712 --> 00:00:33,518
c'est-à-dire des nombres, des choses comme ça,

15
00:00:34,157 --> 00:00:36,945
et parmi tous ces éléments,

16
00:00:37,045 --> 00:00:39,120
j'ai envie de retrouver un élément

17
00:00:39,623 --> 00:00:41,892
ou des éléments dont la clé vaut k.

18
00:00:42,238 --> 00:00:44,711
Et c'est ça qui va être intéressant,

19
00:00:44,811 --> 00:00:45,584
c'est-à-dire que

20
00:00:45,863 --> 00:00:47,524
je veux par exemple retrouver

21
00:00:47,624 --> 00:00:50,383
tous les étudiants qui sont inscrits

22
00:00:50,483 --> 00:00:53,197
dans telle promotion,

23
00:00:53,297 --> 00:00:55,497
ou qui sont inscrits en informatique à Grenoble.

24
00:00:57,033 --> 00:00:58,340
Quand je vais regarder

25
00:00:59,421 --> 00:01:03,251
ces exemples,

26
00:01:03,351 --> 00:01:04,803
ce sont des choses très simples,

27
00:01:05,224 --> 00:01:06,728
ça peut être un numéro d'étudiant,

28
00:01:06,828 --> 00:01:08,548
ça peut être une date de naissance,

29
00:01:08,648 --> 00:01:10,517
ça peut être une note, ça peut être ce qu'on veut,

30
00:01:10,850 --> 00:01:14,534
En fait, rechercher un étudiant

31
00:01:14,634 --> 00:01:15,968
ayant cette propriété-là,

32
00:01:16,068 --> 00:01:17,113
ça pose un problème

33
00:01:17,402 --> 00:01:19,147
qui est le problème de savoir

34
00:01:20,208 --> 00:01:21,987
le trouver rapidement.

35
00:01:22,435 --> 00:01:25,435
Trouver un dossier, ce n'est pas évident.

36
00:01:25,710 --> 00:01:29,950
On a un certain nombre de méthodes pour faire

37
00:01:30,050 --> 00:01:32,241
et pour cela, on va essayer d'organiser les données

38
00:01:32,543 --> 00:01:34,263
de façon à être plus efficace.

39
00:01:34,946 --> 00:01:36,405
Alors si je n'ai aucune structure,

40
00:01:36,505 --> 00:01:38,288
juste un mécanisme d'accès à mes données,

41
00:01:39,664 --> 00:01:41,061
par exemple, vous avez juste un tableau,

42
00:01:41,161 --> 00:01:43,652
vous avez mis vos éléments dans un tableau ou une liste,

43
00:01:44,289 --> 00:01:46,366
vous aurez à parcourir toute la structure

44
00:01:46,466 --> 00:01:47,584
pour savoir exactement

45
00:01:48,736 --> 00:01:51,221
quels sont les éléments qui vont avoir une clé donnée.

46
00:01:54,044 --> 00:01:55,603
Si je vais un peu plus loin,

47
00:01:55,960 --> 00:01:59,220
je peux me dire : tiens, si je triais mes données au préalable,

48
00:01:59,320 --> 00:02:01,535
c'est-à-dire si je les organisais déjà un petit peu,

49
00:02:01,822 --> 00:02:03,628
peut-être que ça irait beaucoup plus vite.

50
00:02:04,606 --> 00:02:07,042
Si par exemple vous avez un tableau trié,

51
00:02:07,364 --> 00:02:09,322
ce qu'on va appeler la recherche dichotomique

52
00:02:09,422 --> 00:02:11,368
va vous permettre de retrouver

53
00:02:11,645 --> 00:02:14,106
rapidement un élément qui est dans le tableau.

54
00:02:14,638 --> 00:02:16,918
Vous triez tous les éléments par la clé

55
00:02:17,223 --> 00:02:18,533
et vous allez retrouver

56
00:02:18,633 --> 00:02:21,740
assez facilement votre élément.

57
00:02:23,410 --> 00:02:25,563
Si on va un peu plus loin,

58
00:02:25,999 --> 00:02:28,475
on pourrait même imaginer des mécanismes

59
00:02:28,575 --> 00:02:31,250
dans lesquels vous avez un accès direct à l'élément

60
00:02:31,529 --> 00:02:32,585
à partir de la clé,

61
00:02:32,950 --> 00:02:35,172
c'est ce que vous allez retrouver dans les dictionnaires,

62
00:02:35,396 --> 00:02:39,769
où la structure sous-jacente est ce qu'on appelle les fonctions de hachage,

63
00:02:40,014 --> 00:02:41,686
qui vous permettent à partir de la clé

64
00:02:41,786 --> 00:02:42,847
de retrouver l'élément.

65
00:02:43,125 --> 00:02:44,452
Malheureusement, on ne peut pas

66
00:02:44,552 --> 00:02:46,203
mettre des fonctions de hachage sur tout,

67
00:02:46,565 --> 00:02:47,894
ce n'est pas forcément évident,

68
00:02:47,994 --> 00:02:49,378
ça ne s'adapte pas forcément,

69
00:02:49,647 --> 00:02:52,582
et donc une des techniques les plus simples,

70
00:02:52,682 --> 00:02:54,617
c'est d'organiser les données

71
00:02:54,717 --> 00:02:57,207
sous forme de tableau trié,

72
00:02:57,495 --> 00:02:58,544
ou de liste triée.

73
00:02:58,818 --> 00:03:00,379
Plus tard, dans les chapitres suivants,

74
00:03:00,479 --> 00:03:01,610
nous verrons sous forme d'arbre,

75
00:03:01,904 --> 00:03:03,541
de stocker dans des arbres,

76
00:03:03,872 --> 00:03:05,792
et donc de résoudre le problème

77
00:03:06,967 --> 00:03:07,989
à partir de là.

78
00:03:08,089 --> 00:03:10,242
Alors on peut avoir d'autres critères

79
00:03:10,342 --> 00:03:11,519
qui peuvent jouer

80
00:03:11,619 --> 00:03:13,283
pour l'organisation des données.

81
00:03:13,606 --> 00:03:16,652
En particulier, est-ce qu'on a envie

82
00:03:16,752 --> 00:03:18,744
d'avoir une structure statique ou dynamique,

83
00:03:18,844 --> 00:03:21,375
c'est-à-dire à laquelle je peux rajouter des éléments,

84
00:03:21,475 --> 00:03:23,613
à laquelle je peux retirer des éléments ?

85
00:03:23,713 --> 00:03:24,799
On peut avoir des données

86
00:03:25,204 --> 00:03:27,874
qui sont des combinaisons de plusieurs opérateurs.

87
00:03:27,974 --> 00:03:30,678
Par exemple, vous pouvez vouloir aussi

88
00:03:30,778 --> 00:03:33,371
avoir une structure de données dans laquelle

89
00:03:33,471 --> 00:03:35,664
vous avez envie d'énumérer tous les éléments.

90
00:03:35,957 --> 00:03:38,252
Vous avez envie d'énumérer tous les éléments

91
00:03:38,352 --> 00:03:40,173
dont la clé est comprise entre ça et ça.

92
00:03:40,477 --> 00:03:41,917
Vous avez envie de sélectionner

93
00:03:42,017 --> 00:03:43,508
tous les éléments dont la clé, c'est ça

94
00:03:43,608 --> 00:03:45,370
et vérifiant telle autre propriété.

95
00:03:46,498 --> 00:03:48,187
Tout ça, ce sont des techniques

96
00:03:48,287 --> 00:03:51,253
qui vous permettent de regarder.

97
00:03:51,353 --> 00:03:52,489
Vous pouvez également

98
00:03:53,309 --> 00:03:54,466
vous dire que

99
00:03:54,566 --> 00:03:56,245
quand vous avez une structure de données,

100
00:03:57,715 --> 00:03:59,562
vous avez effectivement vos éléments

101
00:03:59,829 --> 00:04:01,362
et ce qui va être intéressant,

102
00:04:01,462 --> 00:04:04,693
c'est de répéter les opérations de recherche.

103
00:04:04,991 --> 00:04:06,297
Une fois que j'ai ma liste d'étudiants,

104
00:04:06,397 --> 00:04:08,043
je suis bien content parce que je suis capable

105
00:04:08,143 --> 00:04:09,697
de répéter mes recherches.

106
00:04:14,370 --> 00:04:15,763
Alors pourquoi le tri ?

107
00:04:17,441 --> 00:04:18,858
La réponse la plus élémentaire,

108
00:04:18,958 --> 00:04:20,010
ça serait de dire

109
00:04:20,294 --> 00:04:21,925
c'est rangé, donc ça va bien,

110
00:04:22,025 --> 00:04:25,107
c'est joli et c'est facile.

111
00:04:25,808 --> 00:04:27,925
En fait, il faut aller quand même un petit peu plus loin.

112
00:04:29,033 --> 00:04:31,729
Quand on a des données qui sont structurées,

113
00:04:31,829 --> 00:04:32,804
comme des données triées,

114
00:04:33,079 --> 00:04:35,069
on a des algorithmes de recherche

115
00:04:35,169 --> 00:04:36,167
qui vont être efficaces.

116
00:04:36,575 --> 00:04:38,193
La recherche par dichotomie,

117
00:04:38,293 --> 00:04:39,101
c'est un exemple.

118
00:04:39,418 --> 00:04:40,428
J'en donne un autre,

119
00:04:40,528 --> 00:04:42,833
vous avez les mots qui sont triés dans le dictionnaire

120
00:04:42,933 --> 00:04:44,295
et quand vous allez chercher un mot,

121
00:04:44,395 --> 00:04:45,961
vous n'allez pas faire une recherche dichotomique,

122
00:04:46,061 --> 00:04:47,923
vous n'allez pas ouvrir le dictionnaire au milieu

123
00:04:48,023 --> 00:04:49,205
et dire c'est à droite ou c'est à gauche.

124
00:04:49,472 --> 00:04:53,318
Vous allez dire, c'est une lettre, je ne sais pas, T

125
00:04:53,418 --> 00:04:55,134
donc c'est plutôt à la fin du dictionnaire.

126
00:04:55,418 --> 00:04:58,007
Donc je vais essayer d'ouvrir mon dictionnaire proche de la fin.

127
00:04:58,331 --> 00:05:01,286
Puis là, vous tombez sur la lettre S,

128
00:05:01,386 --> 00:05:03,259
vous dites ce n'est pas très loin de la lettre S,

129
00:05:03,359 --> 00:05:05,319
donc plutôt que de couper en deux de S à Z,

130
00:05:05,670 --> 00:05:07,181
vous allez vous mettre quelque part

131
00:05:07,281 --> 00:05:11,183
qui est plus près de la lettre T estimée.

132
00:05:11,283 --> 00:05:13,606
Ça, c'est ce qu'on appelle les recherches par interpolation

133
00:05:13,706 --> 00:05:15,289
qui peuvent être extrêmement efficaces.

134
00:05:17,153 --> 00:05:18,951
Vous pouvez avoir aussi

135
00:05:20,005 --> 00:05:22,519
des aspects qui sont des fonctions de base

136
00:05:23,562 --> 00:05:25,568
de tri, donc ça, ça vous permet

137
00:05:25,668 --> 00:05:27,113
de faire très rapidement

138
00:05:27,213 --> 00:05:29,075
des opérations de manipulation de données.

139
00:05:29,765 --> 00:05:32,005
Pourquoi est-ce que dans la littérature

140
00:05:32,105 --> 00:05:33,740
on a autant d'algorithmes de tri ?

141
00:05:35,013 --> 00:05:37,739
Ça, c'est quelque chose qui peut surprendre

142
00:05:37,839 --> 00:05:39,325
le néophyte en informatique,

143
00:05:39,621 --> 00:05:40,893
c'est que des algorithmes de tri,

144
00:05:40,993 --> 00:05:42,142
vous les avez dans tous les cours,

145
00:05:42,242 --> 00:05:44,481
dans tous les chapitres, dans tous les volumes,

146
00:05:44,581 --> 00:05:46,934
vous avez un petit algorithme de tri qui se balade

147
00:05:47,227 --> 00:05:48,609
et qui effectivement,

148
00:05:50,798 --> 00:05:53,155
vous donne un tri

149
00:05:53,255 --> 00:05:54,935
et puis un autre tri, puis un autre tri,

150
00:05:55,035 --> 00:05:56,169
vous en avez 36 000.

151
00:05:58,122 --> 00:06:00,993
Et donc, la question qui se pose, c'est

152
00:06:02,307 --> 00:06:03,702
que ça dépend éventuellement

153
00:06:03,802 --> 00:06:04,933
de la caractéristique des données,

154
00:06:06,000 --> 00:06:07,532
selon le type de données que vous avez,

155
00:06:07,632 --> 00:06:08,661
peut-être que vous avez des tris

156
00:06:08,761 --> 00:06:10,026
qui sont plus adaptés que d'autres.

157
00:06:10,696 --> 00:06:13,423
Si vous avez des données qui sont triées dans une liste,

158
00:06:13,523 --> 00:06:15,833
peut-être qu'un tri sera plus adapté qu'un autre.

159
00:06:16,486 --> 00:06:18,609
Vous avez différents supports physiques

160
00:06:18,709 --> 00:06:19,576
qui peuvent jouer

161
00:06:19,676 --> 00:06:21,253
quand par exemple, vous avez des données

162
00:06:21,353 --> 00:06:22,115
qui sont tellement grandes

163
00:06:22,215 --> 00:06:23,213
qu'elles ne tiennent pas en mémoire

164
00:06:23,313 --> 00:06:25,105
et que vous êtes obligés de faire des accès disque

165
00:06:25,515 --> 00:06:26,880
pour aller récupérer les données,

166
00:06:26,980 --> 00:06:27,818
il n'est pas évident

167
00:06:27,918 --> 00:06:30,955
que ce soit la bonne façon de faire.

168
00:06:31,595 --> 00:06:34,700
Et puis vous avez différentes approches

169
00:06:34,800 --> 00:06:36,423
par rapport au tri,

170
00:06:36,772 --> 00:06:38,073
des approches statiques,

171
00:06:38,173 --> 00:06:39,403
des approches dynamiques,

172
00:06:40,384 --> 00:06:41,740
c'est-à-dire dans lesquelles

173
00:06:41,840 --> 00:06:43,190
vous allez pouvoir vous adapter,

174
00:06:44,346 --> 00:06:46,217
des recherches globales, locales

175
00:06:46,317 --> 00:06:48,218
en fonction de la position en mémoire, et cætera.

176
00:06:49,248 --> 00:06:50,136
Donc il y a peut-être

177
00:06:50,236 --> 00:06:52,315
plein de solutions qui peuvent être différentes,

178
00:06:52,415 --> 00:06:53,823
tout simplement pour s'adapter

179
00:06:54,108 --> 00:06:55,586
au contexte dans lequel vous allez

180
00:06:56,407 --> 00:06:58,729
effectuer votre recherche.

181
00:07:01,041 --> 00:07:03,213
Si maintenant, je regarde

182
00:07:03,313 --> 00:07:05,341
ces bouquins d'algorithmique,

183
00:07:06,411 --> 00:07:08,030
en fait, ce qui va être important,

184
00:07:08,130 --> 00:07:11,082
c'est que les algorithmes de tri,

185
00:07:11,182 --> 00:07:12,364
on va les utiliser

186
00:07:12,464 --> 00:07:14,064
comme des paradigmes algorithmiques,

187
00:07:14,164 --> 00:07:15,862
c'est-à-dire comme des schémas

188
00:07:16,141 --> 00:07:18,105
qui permettent de mettre en avant

189
00:07:18,205 --> 00:07:20,360
une propriété qui nous intéresse.

190
00:07:21,791 --> 00:07:22,909
L'objectif est donc

191
00:07:23,009 --> 00:07:24,766
plutôt pédagogique à ce niveau-là.

192
00:07:25,146 --> 00:07:26,264
Il ne faut pas se tromper,

193
00:07:26,364 --> 00:07:29,378
ce n'est pas le fait

194
00:07:29,478 --> 00:07:30,883
qu'on va trier des éléments

195
00:07:30,983 --> 00:07:31,641
qui est important,

196
00:07:31,942 --> 00:07:33,864
c'est le fait que le problème du tri

197
00:07:33,964 --> 00:07:36,042
va nous permettre de bien comprendre

198
00:07:36,142 --> 00:07:37,056
certains schémas.

199
00:07:37,774 --> 00:07:39,824
Et ça, c'est je dirais la chose

200
00:07:39,924 --> 00:07:42,444
qui est un peu ignorée de nos jours.

201
00:07:42,883 --> 00:07:45,544
Alors, pourquoi est-ce que ça va être utile ?

202
00:07:45,644 --> 00:07:47,320
Tout simplement parce que vous allez

203
00:07:47,420 --> 00:07:50,413
d'une part, avoir un problème simple,

204
00:07:50,513 --> 00:07:52,001
tout le monde a été confronté

205
00:07:52,352 --> 00:07:54,352
à trier des objets,

206
00:07:54,659 --> 00:07:55,900
depuis la maternelle,

207
00:07:56,000 --> 00:07:57,660
en maternelle, on apprend à trier des objets,

208
00:07:57,975 --> 00:08:00,799
et donc le problème est super simple à énoncer.

209
00:08:01,764 --> 00:08:05,913
Ensuite, les schémas d'algorithmes

210
00:08:06,013 --> 00:08:07,189
sont faciles à analyser,

211
00:08:07,289 --> 00:08:08,740
parce qu'on maîtrise bien le problème.

212
00:08:12,107 --> 00:08:14,506
Quand je regarde d'un peu plus près,

213
00:08:14,606 --> 00:08:17,099
le problème du tri a été largement étudié

214
00:08:17,199 --> 00:08:19,275
donc on a des informations théoriques

215
00:08:19,375 --> 00:08:21,390
sur le nombre de permutations,

216
00:08:21,490 --> 00:08:23,587
sur les propriétés des permutations, et cætera.

217
00:08:23,687 --> 00:08:26,592
Il y a eu une analyse du problème du tri

218
00:08:26,692 --> 00:08:28,087
depuis très, très longtemps.

219
00:08:29,200 --> 00:08:33,625
Ensuite, ça permet de faire ce que j'appelle

220
00:08:34,235 --> 00:08:36,771
de l'analyse d'algorithmes, preuve et complexité,

221
00:08:36,871 --> 00:08:38,306
de façon assez élémentaire

222
00:08:38,720 --> 00:08:41,198
pour mettre le focus sur juste des propriétés

223
00:08:41,298 --> 00:08:42,285
qui nous intéressent.

224
00:08:42,667 --> 00:08:43,739
Quand vous aurez à prouver

225
00:08:43,839 --> 00:08:45,179
des algorithmes plus complexes,

226
00:08:45,504 --> 00:08:47,671
vous aurez l'architecture qui sera donnée

227
00:08:47,771 --> 00:08:50,466
sans doute dans une preuve d'algorithme de tri.

228
00:08:50,702 --> 00:08:52,444
La conclusion de tout ça,

229
00:08:52,544 --> 00:08:53,931
c'est que l'algorithme de tri,

230
00:08:54,282 --> 00:08:56,616
ça vous permet d'illustrer

231
00:08:56,716 --> 00:08:58,540
un grand nombre de principes algorithmiques,

232
00:08:58,640 --> 00:08:59,855
des schémas,

233
00:08:59,955 --> 00:09:01,409
et donc c'est, je dirais,

234
00:09:01,509 --> 00:09:03,551
dans votre boîte à outils d'informaticien,

235
00:09:03,858 --> 00:09:05,469
un outil de base pour dire

236
00:09:05,569 --> 00:09:08,272
on va résoudre par exemple le problème du crêpier,

237
00:09:08,608 --> 00:09:10,148
je reconnais

238
00:09:10,248 --> 00:09:11,864
un algorithme de recherche du maximum

239
00:09:11,964 --> 00:09:12,755
et je reconnais

240
00:09:13,041 --> 00:09:14,920
un algorithme de tri à l'intérieur.

241
00:09:15,193 --> 00:09:17,601
Donc ça va vous servir

242
00:09:17,701 --> 00:09:18,663
comme boîte à outils

243
00:09:19,483 --> 00:09:20,559
de pensée

244
00:09:20,659 --> 00:09:22,447
au niveau de l'algorithmique.

