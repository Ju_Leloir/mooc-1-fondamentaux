1
00:00:01,226 --> 00:00:03,279
Alors, prenons le premier tri,

2
00:00:03,379 --> 00:00:04,484
le tri par sélection.

3
00:00:05,146 --> 00:00:07,077
Le principe du tri par sélection,

4
00:00:07,177 --> 00:00:09,950
c'est de sélectionner successivement le maximum

5
00:00:10,050 --> 00:00:11,134
parmi les éléments

6
00:00:11,234 --> 00:00:13,247
jusqu'à ne plus avoir d'éléments à trier.

7
00:00:13,730 --> 00:00:15,112
C'est-à-dire que

8
00:00:15,212 --> 00:00:17,838
pour avoir un ensemble trié,

9
00:00:18,203 --> 00:00:19,580
je vais partir,

10
00:00:19,680 --> 00:00:21,076
je vais sélectionner le plus grand,

11
00:00:21,176 --> 00:00:22,340
et puis je vais recommencer

12
00:00:22,817 --> 00:00:23,632
avec la suite.

13
00:00:23,732 --> 00:00:26,220
Alors, ce qui est intéressant,

14
00:00:26,320 --> 00:00:27,305
c'est que dans cette formulation,

15
00:00:27,405 --> 00:00:28,747
on a une formulation récursive.

16
00:00:29,565 --> 00:00:32,605
Alors ici, j'ai pris la construction de mon ensemble,

17
00:00:32,910 --> 00:00:34,460
ça sera des listes.

18
00:00:35,809 --> 00:00:37,162
On aurait pu prendre autre chose,

19
00:00:37,262 --> 00:00:38,416
ce n'est pas un souci,

20
00:00:38,516 --> 00:00:39,558
on peut le faire dans un tableau,

21
00:00:40,208 --> 00:00:41,396
la question n'est pas là.

22
00:00:42,871 --> 00:00:44,933
L'algorithme est le suivant.

23
00:00:45,033 --> 00:00:46,338
J'ai X un ensemble d'éléments.

24
00:00:46,733 --> 00:00:48,176
Le résultat, ce sera une liste

25
00:00:48,276 --> 00:00:49,888
ou tableau de tous les éléments de X

26
00:00:49,988 --> 00:00:51,070
qui seront ordonnés.

27
00:00:51,478 --> 00:00:52,650
Si X est vide,

28
00:00:52,750 --> 00:00:54,324
et bien je retourne un tableau vide.

29
00:00:54,797 --> 00:00:56,973
Et si X n'est pas vide,

30
00:00:57,211 --> 00:01:00,043
alors cette opération-là, ici,

31
00:01:00,143 --> 00:01:02,715
est possible, je peux sélectionner le maximum

32
00:01:03,239 --> 00:01:04,554
Qu'est-ce que je vais faire ?

33
00:01:04,912 --> 00:01:06,652
Je vais trier

34
00:01:07,669 --> 00:01:09,227
ce qu'il reste

35
00:01:09,815 --> 00:01:10,509
ici

36
00:01:11,161 --> 00:01:12,084
et je vais rajouter,

37
00:01:12,184 --> 00:01:13,382
alors si je veux être à la Python,

38
00:01:13,482 --> 00:01:14,565
je vais devoir faire

39
00:01:14,665 --> 00:01:15,703
un petit quelque chose comme ça,

40
00:01:16,066 --> 00:01:17,677
et je vais rajouter à la fin

41
00:01:17,777 --> 00:01:19,414
l'élément x que j'ai sélectionné.

42
00:01:19,869 --> 00:01:21,103
Et donc vous voyez que vous avez

43
00:01:21,203 --> 00:01:23,429
dans cette forme récursive

44
00:01:23,842 --> 00:01:26,133
X -  {x},

45
00:01:26,233 --> 00:01:28,153
vous enlevez l'élément et vous continuez.

46
00:01:28,924 --> 00:01:32,250
Ça, c'est vraiment un des tris les plus simples.

47
00:01:32,675 --> 00:01:34,688
Alors, moi, je l'aime bien parce que

48
00:01:34,788 --> 00:01:38,555
ça rappelle un tri qui est communément utilisé

49
00:01:38,655 --> 00:01:39,922
qu'on appelle le tri spaghetti

50
00:01:40,022 --> 00:01:43,064
et je vais essayer de vous faire une démonstration.

51
00:01:43,164 --> 00:01:45,671
Si je réfléchis à cet algorithme-là,

52
00:01:45,771 --> 00:01:46,952
j'ai une façon de l'implémenter

53
00:01:47,052 --> 00:01:47,926
qui est assez sympathique

54
00:01:48,026 --> 00:01:50,064
qui est ce qu'on appelle le tri par spaghetti.

55
00:01:50,400 --> 00:01:52,372
Le tri par spaghetti consiste à

56
00:01:52,472 --> 00:01:54,607
prendre un certain nombre de spaghettis

57
00:01:54,707 --> 00:01:55,857
qui sont de toutes les couleurs,

58
00:01:55,957 --> 00:01:57,489
de toutes les valeurs

59
00:01:57,589 --> 00:01:58,588
correspondant aux clés,

60
00:01:58,949 --> 00:02:00,431
d'approcher la main par dessus.

61
00:02:03,169 --> 00:02:04,249
Je touche le plus grand

62
00:02:04,656 --> 00:02:06,690
et je peux extraire le plus grand.

63
00:02:07,237 --> 00:02:09,422
Alors on va le mettre en œuvre

64
00:02:09,522 --> 00:02:11,021
de façon un peu différente.

65
00:02:11,509 --> 00:02:13,272
Ici, je me place,

66
00:02:14,649 --> 00:02:16,542
j'ai mes différents chalumeaux,

67
00:02:16,642 --> 00:02:18,551
alors évidemment, c'est du recyclage de chalumeaux,

68
00:02:19,048 --> 00:02:22,316
que je tasse comme mes spaghettis

69
00:02:22,416 --> 00:02:23,659
sur la gauche

70
00:02:24,365 --> 00:02:26,016
et puis je vais prendre un instrument

71
00:02:26,116 --> 00:02:26,859
qui est une règle.

72
00:02:26,959 --> 00:02:30,005
La règle va descendre,

73
00:02:30,105 --> 00:02:33,190
et me dit que le plus grand, c'est celui-ci.

74
00:02:33,626 --> 00:02:35,464
Automatiquement, je le sélectionne

75
00:02:35,564 --> 00:02:36,602
et je le mets en bas.

76
00:02:36,702 --> 00:02:38,986
Le plus grand, c'est celui-ci,

77
00:02:39,086 --> 00:02:40,423
je le sélectionne

78
00:02:40,523 --> 00:02:41,594
et je le mets en bas.

79
00:02:41,694 --> 00:02:44,189
Le plus grand, c'est celui-ci,

80
00:02:44,888 --> 00:02:46,066
je le sélectionne

81
00:02:46,890 --> 00:02:47,755
et je le mets en bas.

82
00:02:48,469 --> 00:02:51,031
Le plus grand, c'est le rose.

83
00:02:51,669 --> 00:02:52,542
Je le sélectionne

84
00:02:53,452 --> 00:02:54,391
je le mets en bas.

85
00:02:55,160 --> 00:02:57,177
Alors ça demande une certaine dextérité.

86
00:02:57,277 --> 00:02:58,651
Je prends le vert.

87
00:03:00,633 --> 00:03:02,926
Ici, le vert clair.

88
00:03:05,154 --> 00:03:07,331
Et puis le bleu.

89
00:03:07,431 --> 00:03:10,729
Ils sont presque tous dans le bon ordre.

90
00:03:12,732 --> 00:03:14,734
Puis le vert,

91
00:03:16,364 --> 00:03:17,572
le rose,

92
00:03:19,582 --> 00:03:21,286
le jaune clair,

93
00:03:21,767 --> 00:03:26,276
et là, il me reste un bleu clair.

94
00:03:28,250 --> 00:03:30,581
Et le dernier, qui est le rouge.

95
00:03:30,941 --> 00:03:32,589
Et ainsi, j'ai trié tous mes éléments.

96
00:03:32,873 --> 00:03:34,972
Alors on sent bien la récursivité qui est derrière

97
00:03:35,072 --> 00:03:35,834
c'est-à-dire que

98
00:03:36,110 --> 00:03:40,823
on va avoir dans cet algorithme

99
00:03:41,413 --> 00:03:43,207
le traitement du tas restant

100
00:03:43,871 --> 00:03:46,366
une fois qu'on a isolé un des éléments.

101
00:03:46,705 --> 00:03:49,067
Et donc, effectivement, dans la descente récursive,

102
00:03:49,167 --> 00:03:50,691
vous avez, à la fin,

103
00:03:50,791 --> 00:03:52,367
les éléments qui vont reconstruire

104
00:03:52,467 --> 00:03:53,564
tout le tableau au fur et à mesure

105
00:03:53,664 --> 00:03:56,375
que vous aurez traité les appels internes,

106
00:03:56,557 --> 00:03:57,787
les appels récursifs.

107
00:03:58,218 --> 00:04:00,510
Voilà, ça, ça vous donne un exemple,

108
00:04:00,868 --> 00:04:02,861
une illustration je dirais,

109
00:04:02,961 --> 00:04:05,199
de l'algorithme de tri par sélection.

110
00:04:05,516 --> 00:04:07,234
Maintenant, on va aller un peu plus loin,

111
00:04:07,334 --> 00:04:08,918
on va regarder comment

112
00:04:09,358 --> 00:04:12,439
on peut exprimer cet algorithme

113
00:04:12,539 --> 00:04:14,161
et comment on va l'analyser.

114
00:04:14,261 --> 00:04:15,756
On a ici donc

115
00:04:15,856 --> 00:04:17,288
la vision récursive

116
00:04:17,740 --> 00:04:19,753
et nous pouvons passer maintenant

117
00:04:19,853 --> 00:04:21,037
à une écriture

118
00:04:21,457 --> 00:04:24,587
qui est une écriture itérative

119
00:04:24,687 --> 00:04:27,116
et plus proche déjà de l'implémentation.

120
00:04:28,187 --> 00:04:30,045
On va supposer qu'on a mis

121
00:04:30,145 --> 00:04:31,580
nos éléments dans un tableau,

122
00:04:31,926 --> 00:04:33,650
et on va regarder comment

123
00:04:33,946 --> 00:04:36,210
est-ce que je peux traiter les éléments dans ce tableau.

124
00:04:36,676 --> 00:04:38,131
Alors ici, vous vous souvenez,

125
00:04:38,231 --> 00:04:39,797
on avait l'extraction du maximum.

126
00:04:40,279 --> 00:04:43,304
On va avoir quelque part à extraire

127
00:04:43,404 --> 00:04:44,629
un maximum,

128
00:04:45,016 --> 00:04:47,437
et donc ce maximum,

129
00:04:47,537 --> 00:04:48,694
une fois que je l'aurai trouvé,

130
00:04:48,794 --> 00:04:50,563
je vais le placer à sa place

131
00:04:50,663 --> 00:04:51,729
à l'intérieur du tableau.

132
00:04:52,093 --> 00:04:55,136
Ici, vous avez l'indice

133
00:04:56,337 --> 00:04:59,129
de la position dans laquelle vous allez être.

134
00:04:59,229 --> 00:05:02,144
C'est-à-dire que vous avez déjà

135
00:05:04,103 --> 00:05:06,009
trié les éléments de i + 1 à n

136
00:05:08,661 --> 00:05:10,864
Ça, c'est ce qui va être le premier point.

137
00:05:11,199 --> 00:05:12,712
Et puis, vous avez derrière

138
00:05:13,013 --> 00:05:13,906
ici

139
00:05:16,578 --> 00:05:18,371
la recherche du maximum

140
00:05:20,221 --> 00:05:24,776
dans, ici, le sous-tableau.

141
00:05:26,346 --> 00:05:28,226
Ici, il faudrait que je fasse un dessin,

142
00:05:28,659 --> 00:05:29,668
on fera le dessin après

143
00:05:29,768 --> 00:05:30,946
quand on regardera les invariants.

144
00:05:32,874 --> 00:05:34,757
L'idée, c'est d'illustrer cet algorithme-là

145
00:05:35,760 --> 00:05:37,646
par un dessin et regarder comment on fait.

146
00:05:38,066 --> 00:05:41,107
Ici, ce que je fais

147
00:05:41,207 --> 00:05:42,335
dans la dernière partie,

148
00:05:42,435 --> 00:05:44,702
j'échange tout simplement l'indice du maximum

149
00:05:44,802 --> 00:05:47,884
et la position à laquelle ce maximum va devoir se trouver.

150
00:05:49,779 --> 00:05:52,245
Alors, si je regarde un exemple.

151
00:05:52,585 --> 00:05:54,469
Ici, pour faire un exemple,

152
00:05:54,569 --> 00:05:55,724
en fait, je vais avoir besoin

153
00:05:56,852 --> 00:05:59,034
de reprendre mon algorithme et de l'illustrer

154
00:05:59,147 --> 00:06:04,078
avec un premier exemple

155
00:06:04,178 --> 00:06:05,454
pour montrer comment ça fonctionne.

156
00:06:05,781 --> 00:06:06,795
Pour faire ça,

157
00:06:07,739 --> 00:06:09,703
je vais indiquer des endroits

158
00:06:09,803 --> 00:06:11,817
où je souhaite observer mon algorithme.

159
00:06:12,126 --> 00:06:13,645
Ici, j'ai deux endroits,

160
00:06:13,937 --> 00:06:14,956
un qui est ici

161
00:06:15,323 --> 00:06:16,377
et un qui est là,

162
00:06:17,790 --> 00:06:20,421
qui me permettent de dire des choses

163
00:06:20,828 --> 00:06:23,996
sur l'état de mes variables.

164
00:06:24,096 --> 00:06:27,746
Je vous rappelle que quand on va essayer d'analyser un algorithme,

165
00:06:28,087 --> 00:06:29,200
ce qui va m'intéresser,

166
00:06:29,300 --> 00:06:32,287
c'est l'état des variables.

167
00:06:34,015 --> 00:06:37,604
Alors maintenant, je vais prendre un exemple.

168
00:06:38,296 --> 00:06:40,059
Voilà, je prends un exemple simple,

169
00:06:40,159 --> 00:06:40,822
machine,

170
00:06:41,096 --> 00:06:42,285
qui, comme vous le savez,

171
00:06:42,385 --> 00:06:43,974
a toutes ses lettres qui sont différentes.

172
00:06:44,364 --> 00:06:45,908
Et puis je vais itérer

173
00:06:46,008 --> 00:06:46,931
petit à petit.

174
00:06:47,031 --> 00:06:49,532
machine, c'est ce que j'ai comme tableau T

175
00:06:49,632 --> 00:06:50,631
au tout début

176
00:06:51,046 --> 00:06:53,268
puis je vais regarder ce que ça va donner.

177
00:06:53,368 --> 00:06:55,351
n est égal à 7

178
00:06:55,451 --> 00:06:57,007
puisque j'ai 7 éléments dans mon tableau.

179
00:06:57,844 --> 00:07:02,997
Mon indice i, c'est 7 - 1, ça fait 6.

180
00:07:03,097 --> 00:07:05,621
Je vais rechercher l'élément maximum

181
00:07:06,006 --> 00:07:08,266
dans tout cet ensemble-là,

182
00:07:08,776 --> 00:07:11,645
de 0 jusqu'à 6,

183
00:07:11,982 --> 00:07:13,902
et, à partir de là,

184
00:07:14,660 --> 00:07:18,819
je vais échanger,

185
00:07:18,919 --> 00:07:20,501
c'est-à-dire que je vais échanger

186
00:07:20,601 --> 00:07:21,558
l'élément qui est là

187
00:07:21,916 --> 00:07:24,211
qui va venir ici,

188
00:07:24,311 --> 00:07:25,265
et l'élément qui est là

189
00:07:25,365 --> 00:07:27,170
qui va venir ici.

190
00:07:27,270 --> 00:07:28,416
Je fais juste un échange.

191
00:07:29,555 --> 00:07:31,022
Si je continue d'un cran,

192
00:07:33,411 --> 00:07:35,179
qu'est-ce qu'il s'est passé ?

193
00:07:35,279 --> 00:07:36,912
J'ai placé l'élément le plus grand

194
00:07:37,012 --> 00:07:37,859
en fin de tableau.

195
00:07:37,959 --> 00:07:41,808
Donc je recommence à faire exactement le même algorithme

196
00:07:41,908 --> 00:07:43,505
mais avec un tableau plus petit.

197
00:07:44,025 --> 00:07:46,593
Donc ici, j'ai i qui est égal à 5.

198
00:07:46,693 --> 00:07:50,019
Je vais aller chercher le maximum

199
00:07:50,399 --> 00:07:51,726
sur tout l'ensemble.

200
00:07:51,826 --> 00:07:55,250
Je trouve que le maximum est ici.

201
00:07:55,820 --> 00:07:57,529
Et donc je vais échanger

202
00:07:57,629 --> 00:07:58,599
le maximum,

203
00:07:59,575 --> 00:08:02,549
qui va là,

204
00:08:03,077 --> 00:08:05,082
et celui-là qui va là,

205
00:08:05,182 --> 00:08:06,103
et j'ai fait un échange.

206
00:08:07,085 --> 00:08:09,528
Et ainsi de suite, on continue,

207
00:08:09,883 --> 00:08:11,686
et on va avoir donc

208
00:08:11,891 --> 00:08:13,216
un ensemble ici

209
00:08:13,629 --> 00:08:15,066
d'éléments qui sont triés

210
00:08:16,204 --> 00:08:17,683
qui va augmenter

211
00:08:18,632 --> 00:08:19,544
petit à petit.

212
00:08:21,709 --> 00:08:22,971
Je continue.

213
00:08:34,917 --> 00:08:40,266
Et j'arrive effectivement

214
00:08:40,366 --> 00:08:42,066
à mon ensemble qui est trié.

215
00:08:42,941 --> 00:08:45,223
Ici, je m'arrête en 1.

216
00:08:45,323 --> 00:08:47,742
Ici, il faut bien voir qu'on s'arrête en 1

217
00:08:47,842 --> 00:08:49,477
parce que le dernier élément,

218
00:08:49,577 --> 00:08:51,244
comme il est plus petit que tous les autres,

219
00:08:51,344 --> 00:08:53,105
il est obligatoirement à sa place.

220
00:08:53,768 --> 00:08:57,521
Et donc ainsi, j'arrive à voir

221
00:08:57,621 --> 00:09:00,717
mon algorithme qui réussit à faire le tri.

222
00:09:01,787 --> 00:09:03,820
Maintenant, on a fait un exemple,

223
00:09:03,920 --> 00:09:04,772
on continue

224
00:09:05,093 --> 00:09:06,164
et on peut faire la preuve.

225
00:09:06,588 --> 00:09:09,287
Ici, vous aurez la preuve aussi sur les transparents

226
00:09:09,387 --> 00:09:10,930
donc je vais aller un peu plus vite.

227
00:09:11,446 --> 00:09:13,818
L'idée, c'est d'exprimer un invariant

228
00:09:14,378 --> 00:09:15,585
c'est-à-dire de dire

229
00:09:15,685 --> 00:09:16,854
dans mon itération

230
00:09:16,954 --> 00:09:17,989
qu'est-ce qu'il va se passer,

231
00:09:18,586 --> 00:09:20,319
et donc je vais prendre un invariant

232
00:09:20,419 --> 00:09:23,369
qui va être relativement simple.

233
00:09:24,297 --> 00:09:26,009
Petite remarque.

234
00:09:26,364 --> 00:09:29,507
Ici, vous voyez, vous avez deux boucles qui sont imbriquées.

235
00:09:29,928 --> 00:09:31,629
Donc on va avoir à prouver

236
00:09:33,300 --> 00:09:34,955
ce que fait cette boucle-là

237
00:09:35,055 --> 00:09:36,663
et après à prouver

238
00:09:36,763 --> 00:09:38,139
ce que fait cette boucle-là.

239
00:09:38,656 --> 00:09:40,326
Si je regarde l'itération interne,

240
00:09:41,960 --> 00:09:43,580
j'ai un invariant

241
00:09:43,680 --> 00:09:45,351
qui est l'indice max, ind_max,

242
00:09:45,451 --> 00:09:47,223
indice de la valeur maximale

243
00:09:47,323 --> 00:09:50,091
du sous-tableau de 0 à j - 1.

244
00:09:51,050 --> 00:09:52,718
C'est vrai ici.

245
00:09:53,133 --> 00:09:56,073
C'est vrai en étoile,

246
00:09:56,173 --> 00:09:57,047
c'est vrai ici

247
00:09:57,349 --> 00:09:59,228
donc si je regarde

248
00:09:59,328 --> 00:10:00,137
d'un peu plus loin,

249
00:10:00,237 --> 00:10:01,630
la pré-condition est vraie

250
00:10:01,730 --> 00:10:03,130
c'est-à-dire que l'invariant est vérifié

251
00:10:03,560 --> 00:10:04,656
à l'entrée de la boucle.

252
00:10:04,756 --> 00:10:06,114
Alors attention, i - 1,

253
00:10:08,835 --> 00:10:09,887
ça fait 0,

254
00:10:09,987 --> 00:10:14,134
donc le tableau avec un seul élément,

255
00:10:14,234 --> 00:10:15,365
effectivement il est trié,

256
00:10:15,884 --> 00:10:18,267
il a la valeur maximale

257
00:10:18,367 --> 00:10:21,270
et si c'est vérifié à la fin

258
00:10:22,710 --> 00:10:26,312
de cette boucle,

259
00:10:26,412 --> 00:10:29,129
et bien, quand vous allez commencer l'itération suivante,

260
00:10:29,229 --> 00:10:30,579
ça sera encore vrai.

261
00:10:30,679 --> 00:10:32,670
Donc votre invariant est bien vérifié.

262
00:10:33,071 --> 00:10:34,841
À la fin de la boucle,

263
00:10:35,148 --> 00:10:38,094
vous aurez j qui sera égal à i

264
00:10:38,194 --> 00:10:41,422
et donc vous avez l'indice du maximum

265
00:10:41,780 --> 00:10:44,789
des éléments qui sont entre

266
00:10:44,889 --> 00:10:46,850
0 et i.

267
00:10:47,520 --> 00:10:49,358
Ça marche bien.

268
00:10:49,998 --> 00:10:52,818
Au passage, une petite remarque,

269
00:10:52,918 --> 00:10:54,234
il faut ajouter à l'invariant

270
00:10:54,334 --> 00:10:56,021
que vous n'avez rien modifié d'autre

271
00:10:56,121 --> 00:10:57,954
et que vous avez juste de la consultation d'éléments

272
00:10:58,054 --> 00:10:58,924
mais pas de manipulation,

273
00:10:59,312 --> 00:11:02,011
vous ne touchez à rien, vous avez juste une valeur d'indice.

274
00:11:02,318 --> 00:11:04,603
Et puis après, il vous faut encore démontrer

275
00:11:04,703 --> 00:11:07,632
que la boucle se termine,

276
00:11:08,055 --> 00:11:10,052
c'est-à-dire que vous n'avez pas une boucle infinie.

277
00:11:10,152 --> 00:11:11,212
Ici, vous avez une boucle for

278
00:11:11,312 --> 00:11:12,948
donc c'est relativement trivial.

279
00:11:13,381 --> 00:11:16,569
i - j est une valeur entière

280
00:11:16,669 --> 00:11:17,747
strictement décroissante

281
00:11:18,036 --> 00:11:19,108
minorée par 0

282
00:11:19,208 --> 00:11:20,760
et donc la terminaison est assurée.

283
00:11:22,212 --> 00:11:23,707
J'ai la correction partielle,

284
00:11:23,807 --> 00:11:24,964
j'ai la terminaison,

285
00:11:25,064 --> 00:11:27,215
donc cette boucle, cette itération interne

286
00:11:27,315 --> 00:11:27,872
est correcte.

287
00:11:29,062 --> 00:11:31,232
Et on va faire pareil avec l'itération externe,

288
00:11:31,332 --> 00:11:33,023
c'est-à-dire l'itération qui

289
00:11:33,123 --> 00:11:35,373
correspond à ce for-là.

290
00:11:36,725 --> 00:11:39,311
Là, il va falloir changer un tout petit peu les choses,

291
00:11:39,911 --> 00:11:41,010
et il va falloir dire

292
00:11:41,110 --> 00:11:43,792
qu'est-ce que c'est que l'invariant que je vais avoir.

293
00:11:44,706 --> 00:11:46,857
En fait, là, c'est toujours très sympathique

294
00:11:46,957 --> 00:11:47,796
de faire un dessin.

295
00:11:48,220 --> 00:11:49,698
Qu'est-ce que vous avez ?

296
00:11:53,138 --> 00:11:55,526
Lorsque vous en êtes à l'indice i,

297
00:11:56,707 --> 00:11:58,307
donc ici, vous avez i,

298
00:12:01,999 --> 00:12:05,249
vous avez donc trié

299
00:12:05,349 --> 00:12:06,520
tous les éléments

300
00:12:06,965 --> 00:12:08,670
qui sont après i.

301
00:12:09,578 --> 00:12:12,212
Donc en fait, vous avez tous les éléments qui sont là,

302
00:12:12,312 --> 00:12:13,827
vous imaginez mes chalumeaux,

303
00:12:17,637 --> 00:12:19,296
et après i,

304
00:12:19,396 --> 00:12:21,021
vous n'avez que des chalumeaux

305
00:12:21,121 --> 00:12:22,180
qui sont plus grands

306
00:12:22,735 --> 00:12:27,675
donc ici, vous avez quelque chose qui est croissant

307
00:12:27,972 --> 00:12:29,358
et ce point-là,

308
00:12:29,686 --> 00:12:31,383
il est toujours plus grand

309
00:12:35,058 --> 00:12:36,382
que tous les autres qui sont avant.

310
00:12:36,900 --> 00:12:42,106
Donc c'est ça que vous dit juste l'invariant I1.

311
00:12:42,516 --> 00:12:44,568
En fait, l'invariant I1, c'est juste l'expression

312
00:12:44,668 --> 00:12:46,584
de l'algorithme tel qu'on l'a énoncé tout à l'heure.

313
00:12:46,786 --> 00:12:48,747
Il n'y a rien de plus dans l'invariant.

314
00:12:50,500 --> 00:12:52,411
Alors, la pré-condition,

315
00:12:52,511 --> 00:12:54,626
évidemment, c'est toujours vérifié en étoile.

316
00:12:54,726 --> 00:12:57,161
Vous pouvez le faire à la main, il n'y a pas de souci.

317
00:12:57,498 --> 00:13:02,081
Et puis si cette condition est vérifiée ici,

318
00:13:02,422 --> 00:13:03,750
c'est-à-dire en début de boucle,

319
00:13:03,850 --> 00:13:04,990
elle va être vérifiée

320
00:13:05,090 --> 00:13:06,835
en sortie de boucle ici

321
00:13:07,589 --> 00:13:09,292
parce que vous allez faire l'échange,

322
00:13:09,669 --> 00:13:11,473
c'est l'échange là qui va vous donner

323
00:13:11,856 --> 00:13:18,421
le fait que le tableau sera trié de i + 1 jusqu'à n,

324
00:13:18,521 --> 00:13:20,819
et donc la post-condition implique

325
00:13:21,819 --> 00:13:23,018
une fois que vous aurez fini,

326
00:13:23,118 --> 00:13:24,353
puisque vous arrêtez à 1,

327
00:13:24,453 --> 00:13:26,002
qu'il est trié

328
00:13:26,419 --> 00:13:28,518
de 1 jusqu'à n - 1

329
00:13:29,543 --> 00:13:31,935
et que le premier est plus petit que tous les autres.

330
00:13:32,035 --> 00:13:33,574
Donc le tableau est trié.

331
00:13:34,755 --> 00:13:36,733
Donc si la boucle interne est correcte,

332
00:13:36,833 --> 00:13:39,434
on a l'indice max qui nous est donné,

333
00:13:39,790 --> 00:13:41,668
on va avoir que

334
00:13:41,854 --> 00:13:43,783
l'itération se termine.

335
00:13:44,232 --> 00:13:46,811
J'ai la correction partielle,

336
00:13:47,097 --> 00:13:50,959
j'ai le variant qui me dit que l'itération termine,

337
00:13:51,302 --> 00:13:53,742
donc mon algorithme est prouvé.

338
00:13:54,435 --> 00:13:56,732
Alors, on peut après, une fois qu'on a fait

339
00:13:57,097 --> 00:13:58,798
cet aspect preuve,

340
00:13:58,898 --> 00:14:01,147
regarder l'aspect complexité.

341
00:14:01,639 --> 00:14:02,619
Là encore,

342
00:14:02,891 --> 00:14:04,229
il faut bien repréciser

343
00:14:04,329 --> 00:14:05,752
tous les éléments qu'on va regarder

344
00:14:06,110 --> 00:14:07,191
Donc on va regarder

345
00:14:07,291 --> 00:14:09,726
quel opérateur va être utilisé

346
00:14:12,194 --> 00:14:14,774
pour évaluer la complexité.

347
00:14:15,152 --> 00:14:17,508
Ici, l'opérateur que l'on va utiliser,

348
00:14:17,608 --> 00:14:19,815
c'est l'opérateur de comparaison

349
00:14:20,151 --> 00:14:21,340
qui est celui-ci.

350
00:14:21,696 --> 00:14:24,336
Pourquoi on prend l'opérateur de comparaison

351
00:14:24,436 --> 00:14:25,649
et pas celui d'échange ?

352
00:14:26,318 --> 00:14:28,605
On pourrait prendre l'opérateur d'échange

353
00:14:28,705 --> 00:14:31,812
mais je dirais qu'au niveau où l'on se situe là,

354
00:14:32,093 --> 00:14:34,272
on va plutôt regarder l'opérateur de comparaison

355
00:14:34,372 --> 00:14:37,155
qui nous semble plus intéressant.

356
00:14:37,541 --> 00:14:39,996
Alors en fait, il faudrait prendre le max entre les deux

357
00:14:40,096 --> 00:14:42,665
parce que certains tris vont avoir plus d'échanges que d'autres,

358
00:14:43,257 --> 00:14:44,484
et moins de comparaisons.

359
00:14:44,803 --> 00:14:47,362
Et évidemment, il nous faut une taille des données.

360
00:14:47,595 --> 00:14:49,124
Ici, ce que je suggère,

361
00:14:49,224 --> 00:14:50,468
c'est de prendre n

362
00:14:50,834 --> 00:14:51,854
la taille du tableau

363
00:14:51,954 --> 00:14:53,282
qui correspond à la taille des données

364
00:14:53,382 --> 00:14:54,373
que l'on va utiliser.

365
00:14:54,919 --> 00:14:57,666
Là encore, il va se passer deux petites choses.

366
00:14:58,976 --> 00:15:00,478
On va regarder d'abord

367
00:15:00,718 --> 00:15:03,738
ce qu'il se passe dans l'itération interne.

368
00:15:04,176 --> 00:15:05,313
Voilà.

369
00:15:05,527 --> 00:15:08,331
L'itération interne, c'est-à-dire

370
00:15:08,698 --> 00:15:11,503
cette partie-là.

371
00:15:12,515 --> 00:15:14,009
Qu'est-ce que je vais faire ?

372
00:15:14,109 --> 00:15:16,310
Je vais avoir une boucle de longueur i

373
00:15:16,590 --> 00:15:20,159
et à chaque fois que j'ai un passage dans la boucle,

374
00:15:20,655 --> 00:15:24,988
j'effectue une comparaison

375
00:15:25,375 --> 00:15:26,388
qui est ici.

376
00:15:26,911 --> 00:15:29,569
Il suffit de regarder,

377
00:15:29,669 --> 00:15:31,204
j'ai de j égal à 1 jusqu'à i,

378
00:15:31,304 --> 00:15:34,290
donc je vais avoir exactement i comparaisons.

379
00:15:34,952 --> 00:15:36,035
Ce qui est normal,

380
00:15:36,135 --> 00:15:37,801
pour trouver le maximum dans un tableau,

381
00:15:37,901 --> 00:15:38,955
je regarde tous les éléments.

382
00:15:40,417 --> 00:15:44,179
Là, on a la complexité qui est associée à l'itération interne,

383
00:15:44,570 --> 00:15:46,316
et pour avoir l'itération externe,

384
00:15:46,416 --> 00:15:48,669
c'est la somme.

385
00:15:48,769 --> 00:15:51,797
Les valeurs de i vont de n - 1 jusqu'à 1,

386
00:15:52,159 --> 00:15:53,355
et donc je vais avoir

387
00:15:53,743 --> 00:15:56,490
une somme de i égal n - 1 jusqu'à 1

388
00:15:56,728 --> 00:15:58,064
et je vais regarder le coût

389
00:15:58,164 --> 00:15:59,819
à chaque fois de l'itération interne.

390
00:16:00,181 --> 00:16:01,717
Donc ça, c'est le coût de l'itération,

391
00:16:01,817 --> 00:16:03,650
ça, c'est l'itération interne numéro 1

392
00:16:03,889 --> 00:16:04,882
quand i vaut n - 1,

393
00:16:04,982 --> 00:16:06,754
ça, c'est quand i vaut n - 2, et cætera.

394
00:16:07,189 --> 00:16:10,984
Et donc je me retrouve avec 1 + 2 + 3 jusqu'à n - 1.

395
00:16:11,242 --> 00:16:13,573
Ça, c'est une formule que vous devez connaître,

396
00:16:13,673 --> 00:16:16,160
ça nous donne n(n - 1)/2.

397
00:16:16,565 --> 00:16:20,397
Une chose extrêmement importante à noter ici,

398
00:16:20,497 --> 00:16:22,964
c'est que le coût ne dépend

399
00:16:23,555 --> 00:16:25,926
que de la taille du tableau,

400
00:16:26,495 --> 00:16:28,245
c'est-à-dire que quels que soient les éléments

401
00:16:28,345 --> 00:16:30,223
que vous aurez à l'intérieur de votre tableau,

402
00:16:30,556 --> 00:16:33,519
le coût de l'algorithme sera exactement le même.

