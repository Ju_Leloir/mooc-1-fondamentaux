1
00:00:01,125 --> 00:00:03,701
Dans cette deuxième partie de la séquence,

2
00:00:03,801 --> 00:00:06,431
nous allons présenter les arbres

3
00:00:06,747 --> 00:00:08,453
qui sont un des outils fondamentaux

4
00:00:08,553 --> 00:00:10,039
utilisés en informatique.

5
00:00:10,308 --> 00:00:12,326
Alors c'est un outil de pensée d'abord,

6
00:00:12,426 --> 00:00:15,537
et puis après, c'est mis en œuvre effectivement sur machine

7
00:00:15,813 --> 00:00:18,380
avec des structures de données adaptées.

8
00:00:18,723 --> 00:00:20,259
Nous parlerons principalement

9
00:00:20,359 --> 00:00:21,989
de la forme de pensée dans ce cours-là

10
00:00:22,277 --> 00:00:23,768
et de la façon de l'utiliser

11
00:00:24,044 --> 00:00:26,443
puisque dans la partie programmation

12
00:00:26,543 --> 00:00:30,273
et dans la partie structures de données

13
00:00:30,373 --> 00:00:33,250
vous verrez différentes façons d'implémenter des arbres.

14
00:00:33,569 --> 00:00:35,537
Je pourrai en dire quelques mots au fur et à mesure.

15
00:00:35,909 --> 00:00:37,712
On va avoir plusieurs parties

16
00:00:39,627 --> 00:00:40,638
dans ce cours,

17
00:00:40,738 --> 00:00:43,073
on va d'abord commencer par définir proprement

18
00:00:43,173 --> 00:00:45,293
et regarder les objets que nous allons manipuler.

19
00:00:45,603 --> 00:00:48,965
Ensuite, nous allons regarder comment on peut les parcourir.

20
00:00:49,301 --> 00:00:51,672
Et enfin, nous essaierons de donner quelques extensions

21
00:00:51,772 --> 00:00:53,008
mais juste pour information

22
00:00:53,108 --> 00:00:55,060
puisque ces extensions seront sans doute reprises

23
00:00:55,407 --> 00:00:56,578
dans les cours suivants.

