#!/usr/bin/env python3
import threading

class myThread (threading.Thread):

    def __init__(self, phrase):
        threading.Thread.__init__(self)
        self.phrase = phrase
        
    def run(self):
        print("Starting " + self.name)
        for i in range(0,10):
            print(i, self.phrase)
        print("Exiting " + self.name)

def main():
    print("Starting the main thread!!!")
    
    # Create new threads
    thread1 = myThread("Hello")
    thread2 = myThread("Salut")
    thread3 = myThread("Ciao")
    thread4 = myThread("Marhaba")


    # Start new Threads
    thread1.start()
    thread2.start()
    thread3.start()
    thread4.start()

    print("Exiting the main thread!!!")

main()
