#!/usr/bin/env python3

import os

def main():
    print('[père] mon pid est ', os.getpid())
    newpid = os.fork()
    if newpid == 0:
        print('[fils] mon pid est ', os.getpid())
        print('[fils] mon père a comme pid ', os.getppid())
    else:
        print('[père] mon fils a comme pid ', newpid)
        os.waitpid(newpid,0)
    os._exit(0)

main()
