#!/usr/bin/env python3
import threading

class myThread (threading.Thread):

    def __init__(self, phrase, toWait):
        threading.Thread.__init__(self)
        self.phrase = phrase
        self.toWait = toWait
        
    def run(self):
        print("Starting " + self.name)
        for i in range(0,10):
            print(i, self.phrase)
        if self.toWait!=None:
            self.toWait.join()
        print("Exiting " + self.name)

def main():
    print("Starting the main thread!!!")
    
    # Create new threads
    thread1 = myThread("Hello",None)
    thread2 = myThread("Salut", thread1)
    thread3 = myThread("Ciao", thread2)
    thread4 = myThread("Marhaba", thread3)
    
    # Start new Threads
    thread1.start()
    thread2.start()
    thread3.start()
    thread4.start()

    thread4.join()
    print("Exiting the main thread!!!")

main()
