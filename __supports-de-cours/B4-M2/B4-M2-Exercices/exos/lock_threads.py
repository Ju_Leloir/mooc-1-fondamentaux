#!/usr/bin/env python3
import threading

ITERATIONS = 1000

# PlusThread -------------------------------------
class PlusThread (threading.Thread):
    def __init__(self, pair):
        threading.Thread.__init__(self)
        self.pair = pair
        
    def run(self):
        global lx
        global verrou
        for i in range(0,ITERATIONS):
            for j in range(0,list_size):
                verrou.acquire()
                lx[j] += 1.0
                verrou.release()

# MinusThread ------------------------------------
class MinusThread (threading.Thread):
    def __init__(self, pair):
        threading.Thread.__init__(self)
        self.pair = pair
        
    def run(self):
        global lx
        global verrou
        for i in range(0,ITERATIONS):
            for j in range(0,list_size):
                verrou.acquire()
                lx[j] -= 1.0
                verrou.release()



     
# main function ------------------------------------
def main():
    global lx
    NB_THREADS = 10
    
    # Create new threads
    threads = []
    for t in range(0,NB_THREADS//2):
        thread = PlusThread(t)
        threads.append(thread)

    for t in range(NB_THREADS//2,NB_THREADS):    
        thread = MinusThread(t)
        threads.append(thread)

    # Start new threads
    for t in threads:
        t.start()

    # Wait for threads
    for t in threads:
        t.join()
    
    print("Exiting the main thread with lx = \n", lx)

# MAIN -----------------------------------------
print("Starting the main thread!!!")

#shared list
list_size=100
lx=[]
for i in range(0,list_size):
    lx.append(0.0)

#shared lock
verrou = threading.Lock()
    
main()
