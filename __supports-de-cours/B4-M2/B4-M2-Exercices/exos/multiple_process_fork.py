#!/usr/bin/env python3

import os
import time
    

def main():
    print('[parent process] my pid is', os.getpid())
    for i in range(0,3):
        newpid = os.fork()
        print('[process', os.getpid(), '] running')
        print('[process', os.getpid(), ']', i)
        if newpid == 0:
            print('[process', os.getpid(), '] my parent pid', os.getppid())
    time.sleep(10)
    os._exit(0)

main()
