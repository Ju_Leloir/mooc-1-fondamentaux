#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Nov  3 00:41:45 2021

@author: ajuton
"""

import socket
fin=-1

adresse_socket_serveur = ("127.0.0.1", 8082)  #adresse du serveur : localhost, port 8081

#création du socket client
socket_client = socket.socket(socket.AF_INET,socket.SOCK_STREAM)

#tentative de connexion au serveur
socket_client.connect(adresse_socket_serveur)
print("Le client est connecté au serveur")
donnees_recues_du_serveur = socket_client.recv(4096)
print(donnees_recues_du_serveur.decode("utf-8"))

#envoi des propositions jusqu'à la victoire
while(fin<0) :
  proposition=int(input("votre proposition ? "))
  socket_client.send((str(proposition)).encode("utf-8"))
  donnees_recues_du_serveur = socket_client.recv(4096)
  print(donnees_recues_du_serveur.decode("utf-8"))
  fin = (donnees_recues_du_serveur.decode("utf-8")).find('trouvé') #le client a gagné
  
#fermeture du socket
socket_client.close()