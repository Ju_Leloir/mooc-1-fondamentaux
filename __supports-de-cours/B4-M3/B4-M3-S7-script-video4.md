# MOOC NSI - fondamentaux. 
## Transcription de la vidéo

## 4.3.7.4 : VPN

**[00:00:01]**

Nous parlons dans cette vidéo du VPN ou réseau privé virtuel, qui est notamment un élément essentiel de sécurité des réseaux d'entreprise dans la mise en œuvre du télétravail. 

On appelle VPN pour Virtual Private Network, un réseau privé virtuel chiffré qui permet d'établir un lien sécurisé entre une machine et un serveur donnant accès à un réseau distant ou entre deux réseaux reliés par deux routeurs connectés via un lien sécurisé. 
Les VPN permettent de traverser des réseaux non sûrs comme Internet, sans risque de compromettre les données échangées. 

Pour une meilleure compréhension, nous allons revenir sur chacun des termes dans VPN. 
Alors d'abord, il y a le thème réseau ou network. On parle de réseau, car il y a interconnexion entre un ensemble de systèmes informatiques dispersés avec des fonctions de routage ou de commutation. 
Ensuite, on a privé ou private. Le VPN permet de transporter des flots de messages d'une organisation unique de façon indépendante de ceux d'autres usagers. Les usagers doivent également communiquer en utilisant des adresses, une topologie et un routage privé. 
Enfin, on a virtuel qui signifie que le réseau physique ne correspond pas forcément au réseau visé. En effet, le VPN est réalisé sur des connexions temporaires par routage de paquets sur des ressources d'un ou de plusieurs fournisseurs d'accès. 

Les VPN sont caractérisés par :
Une sécurité des communications. En effet, pour établir un réseau privé sur Internet, le protocole utilisé doit remplir deux fonctionnalités importantes. Tout d'abord, l'authentification de l'identité des machines reliées par le VPN. Le serveur VPN doit pouvoir être sûr de parler aux vrais clients VPN et vice versa.



**[00:02:01]**

Seules les personnes autorisées doivent pouvoir se connecter au réseau virtuel et on doit aussi pouvoir conserver les logs de connexion. Deuxièmement, on a le chiffrement des communications entre ces machines pour assurer confidentialité et intégrité.
 En ce qui concerne la qualité de service, les VPN simples créés par un utilisateur sont du type best-effort. Ils ne garantissent pas une bonne qualité de service et peuvent être victimes de surcharge du réseau public qu’ils utilisent. 
Les VPNs permettent de réaliser des économies de coûts grâce à une efficacité du partage de voies physiques à haut débit dans un réseau partagé tel qu'Internet. En effet, la solution alternative longuement pratiquée par les entreprises avant le VPN était de construire leur propre infrastructure complètement privée en louant des liaisons spécialisées ou des circuits, ce qui entraînait d'énormes coûts liés à la location et à l'administration.

Suivant les besoins, on référence trois types de VPN:
Le VPN d'accès qui est utilisé pour permettre à des télétravailleurs d'accéder au réseau de leur entreprise. L'utilisateur se sert d'une connexion Internet afin d'établir une liaison sécurisée. 
L'intranet VPN, qui est utilisé pour relier deux ou plusieurs intranets d'une même entreprise entre eux. Ce type de réseau est particulièrement utilisé au sein d'une entreprise possédant plusieurs sites distants.
Et l'extranet VPN qu'une entreprise peut utiliser pour ouvrir un accès distant à un prestataire extérieur. Par exemple, la société chargée de la maintenance du bâtiment aura accès au système de supervision du réseau électrique. Dans ce cas, il est nécessaire d'avoir une authentification forte des utilisateurs ainsi qu'une trace des différents accès.


**[00:03:52]**

De plus, seule une partie des ressources sera partagée, ce qui nécessite une gestion rigoureuse des espaces d'échanges. 

Le réseau VPN repose sur un protocole appelé protocole de tunneling. Ce protocole permet de faire circuler les paquets de l'entreprise de façon chiffrée d'un bout à l'autre du tunnel. Ainsi, les utilisateurs ont l'impression de se connecter directement sur le réseau de leur entreprise. Le principe du schilling consiste à construire un chemin virtuel après avoir identifié l'émetteur et le destinataire. Par la suite, la source chiffre les données et les achemine en empruntant ce chemin virtuel. Le tunneling est l'ensemble des processus d'encapsulation, de transmission et de d'encapsulation. 

Une illustration de tunnel est présentée sur la figure à droite de l'écran, sur la figure. On peut voir un PC sur un réseau privé au domicile, derrière une box ou à l'aéroport, par exemple, qui a accès à distance via un VPN au réseau privé de son entreprise. On suppose qu'il envoie un paquet à un serveur de son entreprise d'adresse IP privée 192.168.1.1 2. Le paquet sera chiffré et encapsulé par le protocole de tunneling dans un autre paquet, avec l'adresse publique de la box internet ou du routeur de l'aéroport comme adresse IP source et l'adresse privée du routeur d'entreprise comme adresse de destination. Une fois dans le réseau local de l'entreprise, le paquet sera déchiffré et pourra être routé vers le serveur d'entreprise qui est son destinataire. Cela équivaut à l'architecture virtuelle qu'on peut voir en bas où les trois équipements sont comme dans un même réseau local.

**[00:05:39]**

Seulement, il faut noter que les requêtes ne peuvent aller que du PC distant, qui est le client VPN vers le réseau local, et pas le contraire. En effet, le PC est inaccessible depuis Internet, car il est sur un réseau privé derrière le firewall de la box ou du routeur de l'aéroport. Les hôtes du réseau d'entreprise peuvent bien sûr envoyer des paquets en réponse aux requêtes du PC distant. 

Trois protocoles sont associés pour réaliser un tunnel. 
On a le protocole transporté ou Passenger Protocol. Il s'agit du protocole utilisateur que l'on souhaite acheminer. Il contient donc le paquet IP original à transmettre. 
Le protocole d'encapsulation ou tunneling Protocol. Il est ajouté pour encapsuler les données usagers en les sécurisant par chiffrement. Quelques exemples sont GRE, IPSec, PPTP ou L2TP. 
Enfin, on a le protocole porteur ou carrier protocol. Il s'agit du protocole employé par le réseau pour transporter les données ; IP la plupart du temps. 

En résumé, nous avons vu la notion de réseau privé virtuel ou VPN, qui permet d'établir en fonction des cas un lien sécurisé entre deux réseaux distants (intranet VPN ou extranet VPN) ou un lien sécurisé entre un utilisateur et un réseau distant. 
Le protocole de tunneling est le protocole utilisé qui permet d'établir un VPN. Il réalise ainsi l'authentification des réseaux ou utilisateurs, la confidentialité et l'intégrité des messages transmis. Il fait tout ceci par les processus d'encapsulation, de transmission et de d'encapsulation que nous avons évoqués.
