
# Représentation en binaire du texte - Texte 

Maintenant que nous avons étudié des techniques pour représenter et
manipuler des *nombres* en binaire, intéressons-nous à d’autres types
d’informations. Nous commencerons par le *texte*, c’est-à-dire des
séquences de caractères. L’idée générale de la représentation (en
binaire) des caractères consiste à faire correspondre à chaque caractère
un et un seul nombre naturel, et à représenter le caractère par la
représentation binaire de ce naturel. Comme il n’existe pas de manière
universelle et naturelle d’associer un nombre à chaque caractère, il
faut fixer une table, qui fait correspondre chaque caractère à un
nombre.

<figure>
<img src="../images/Chap2/Emile_Baudot.jpg" style="width:40.0%" alt="Émile Baudot" /><figcaption aria-hidden="true">Émile <span class="smallcaps">Baudot</span></figcaption>
</figure>

#### Codes historiques: Émile <span class="smallcaps">Baudot</span> et les téléscripteurs

Le besoin de représenter et transmettre de l’information de manière
mécanique est une préoccupation qui a reçu une première réponse lors de
l’introduction du télégraphe, où le fameux code Morse était utilisé pour
représenter les lettres par une série de traits et de points. Au
dix-neuvième siècle, un français, Jean-Maurice Émile <span
class="smallcaps">Baudot</span>[^1] invente le premier code pratique
permettant de représenter tout l’alphabet latin (ainsi que des symboles
de ponctuation usuels) sur $5$ bits. Ce code est présenté à la figure
ci-dessous.

La machine de <span class="smallcaps">Baudot</span> comprenait un
clavier de $5$ touches, avec lequel on entrait les caractères à
transmettre, selon le code que nous avons présenté. Le récepteur
disposait d’une machine imprimant automatiquement les caractères
transmis sur un ruban de papier. La cadence à laquelle les caractères
pouvaient être transmis (par exemple, $10$ caractères par minute) était
mesurée en *bauds*, une unité que l’on connait encore aujourd’hui, bien
qu’elle soit tombée en désuétude. Le système de <span
class="smallcaps">Baudot</span> sera plus tard amélioré pour utiliser du
ruban perforé pour stocker les informations à transmettre. Cela donnera
lieu aux *téléscripteurs* (ou *teletypes* en anglais, abbrévié TTY),
sortes de machines à écrire qui ont la capacité d’envoyer et de recevoir
du texte en utilisant le code de <span class="smallcaps">Baudot</span>,
sur des lignes téléphoniques. Ces systèmes ont été largement utilisés
pour la transmission d’information, notamment par la presse, jusque dans
les années 1980.

<figure>
<img src="../images/Chap2/Code-Baudot.png" style="width:100.0%" alt="Le code Baudot, un système historique de représentation binaire des caractères (on peut associer le + à 1 et le - à 0), breveté en 1882 en France. On voit ici un extrait du brevet américain (1888)." /><figcaption aria-hidden="true">Le code Baudot, un système historique de représentation binaire des caractères (on peut associer le <span class="math inline">+</span> à <span class="math inline">1</span> et le <span class="math inline">−</span> à <span class="math inline">0</span>), breveté en 1882 en France. On voit ici un extrait du brevet américain (1888).</figcaption>
</figure>

#### Le code ASCII

L’évolution la plus marquante des développements historiques que nous
venons de présenter est le code ASCII (*American Standard Code for
Information Interchange*, présenté en 1967), qui est encore en usage
aujourd’hui. Il comprend $128$ caractères encodés sur $7$ bits, et est
présenté à la figure suivante.

<figure>
<img src="../images/Chap2/ascii.png" style="width:100.0%" alt="Le code ASCII. Chaque caractère est représenté par une valeur hexadécimale sur 2 chiffres: le chiffre de poids fort est donné par la ligne, le chiffre de poids faible par la colonne." /><figcaption aria-hidden="true">Le code ASCII. Chaque caractère est représenté par une valeur hexadécimale sur 2 chiffres: le chiffre de poids fort est donné par la ligne, le chiffre de poids faible par la colonne.</figcaption>
</figure>

**Exemple :**

Avec le code ASCII la chaîne de caractères `MOOC NSI` est représentée
par (en hexadécimal):

<div class="center" markdown="1">

`4D 4F 4F 43 20 4E 53 49`,

</div>

ou, en binaire, par:

<div class="center" markdown="1">

$100\ 1101\ \ 100\ 1111\ \ 100\ 1111\ \ 100\ 0011\ \ 010\ 0000\ \ 100\ 1110\ \ 101\ 0011\ \ 100\ 1001$

</div>

$\blacksquare$

Ce code, bien adapté à la langue anglaise, ne permet pas de représenter
les caractères accentués. C’est pourquoi plusieurs extensions ont vu le
jour, sur $8$ bits, où les 128 caractères supplémentaires permettaient
d’encoder les caractères propres à une langue choisie. Par exemple sur
l’IBM PC, ces extensions étaient appelées *code pages*. En voici deux
exemples:

-   le *code page* 437: le jeu de caractère ASCII standard auxquels on a
    ajouté les caractères accentués latin;

-   le *code page* 737: le code ASCII standard plus les caractères
    grecs, voir figure suivante.

L’utilisation de ces *code pages* supposait que l’utilisateur
connaissait le code qui avait été utilisé pour représenter le texte
source, et qu’il disposait des moyens logiciels et matériels pour
afficher les caractères correspondants. En pratique, cela se révélait
souvent fastidieux, et donnait lieu à des surprises si on se trompait de
*code page*…

<figure>
<img src="../images/Chap1/CP737.png" style="width:100.0%" alt="Le Code Page 737: une extension du code ASCII pour obtenir les caractères grecs. (“Code Page 737 — Wikipedia, the Free Encyclopedia” n.d.)" /><figcaption aria-hidden="true">Le <span><em>Code Page</em></span> 737: une extension du code ASCII pour obtenir les caractères grecs. <span class="citation" data-cites="wiki:cfp737">(<span>“Code Page 737 — Wikipedia<span>,</span> the Free Encyclopedia”</span> n.d.)</span></figcaption>
</figure>

#### Unicode

Plus récemment, le projet *Unicode*[^2] a vu le jour, et s’est donné
pour objectif de créer une norme reprenant la plupart des systèmes
d’écriture utilisés dans le monde, et de leur attribuer un encodage. La
version en cours d’Unicode est la version 11 et elle comprend $137\ 439$
caractères. La norme Unicode associe plusieurs encodages à chaque
caractère. L’encodage le plus courant est appelé UTF-8 : il s’agit d’un
encodage à longueur variable car chaque caractère est encodé sur 1, 2, 3
ou 4 octets. Tous les caractères encodés sur 1 octet sont compatibles
avec le standard ASCII. Les encodages sur 2, 3 et 4 octets sont utilisés
pour représenter d’autres caractères, aussi exotiques soient-ils… Par
exemple, la figure suivante présente l’encodage Unicode de l’alphabet
Tagbanwa, en usage aux Philippines (“Tagbanwa (Unicode Block) —
Wikipedia, the Free Encyclopedia” n.d.; “Tagbanwa” 1991).

La norme Unicode est devenue aujourd’hui bien adoptée, notamment par les
sites et navigateurs web.

<figure>
<img src="../images/Chap1/tagbanwa.png" style="width:100.0%" alt="Extrait du standard Unicode, alphabet Tagbanwa (“Tagbanwa (Unicode Block) — Wikipedia, the Free Encyclopedia” n.d.)." /><figcaption aria-hidden="true">Extrait du standard Unicode, alphabet Tagbanwa <span class="citation" data-cites="wiki:tagbanwa">(<span>“Tagbanwa (Unicode Block) — Wikipedia<span>,</span> the Free Encyclopedia”</span> n.d.)</span>.</figcaption>
</figure>

<div id="refs" class="references csl-bib-body hanging-indent"
markdown="1">

<div id="ref-wiki:cfp737" class="csl-entry" markdown="1">

“Code Page 737 — Wikipedia, the Free Encyclopedia.” n.d. Accessed
September 1, 2017.
<https://en.wikipedia.org/w/index.php?title=Code_page_737&oldid=782268129>.

</div>

<div id="ref-tagbanwa" class="csl-entry" markdown="1">

“Tagbanwa.” 1991. The Unicode Consortium.
<http://www.unicode.org/charts/PDF/U1760.pdf>.

</div>

<div id="ref-wiki:tagbanwa" class="csl-entry" markdown="1">

“Tagbanwa (Unicode Block) — Wikipedia, the Free Encyclopedia.” n.d.
Accessed September 1, 2017.
<https://en.wikipedia.org/w/index.php?title=Tagbanwa_(Unicode_block)&oldid=775143371>.

</div>

</div>

[^1]: Ingénieur français, né le 11 septembre 1845, mort le 28 mars 1903.

[^2]: Le consortium qui conçoit et publie Unicode possède un site web
    <https://unicode.org/>.
