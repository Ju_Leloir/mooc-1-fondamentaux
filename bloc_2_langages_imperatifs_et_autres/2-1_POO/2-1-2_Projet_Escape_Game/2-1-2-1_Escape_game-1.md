#  Escape game "Le château" 1 / 4


1. **Escape game "Le château" 1 /4**
2. [Escape game "Le château" 2 /4](2-1-2-2_Escape_game-2.md) 
3. [Escape game "Le château" 3 / 4](2-1-2-3_Escape_game-3.md)
4. [Escape game "Le château" 4 /4](2-1-2-4_Escape_game-4.md)

####

* **Sébastien Hoarau**. Comment réaliser le jeu du "château" de type escape game en mode objet

[![Vidéo 1 B2-M1-S2 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B2-M1-S2-video1.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B2-M1-S2-video1.mp4)

#### <a href="https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/raw/master/__supports-de-cours/B2-M1/B2-M1-S2-video2a.srt" target="_blank">Sous-titre de la vidéo</a> - 
