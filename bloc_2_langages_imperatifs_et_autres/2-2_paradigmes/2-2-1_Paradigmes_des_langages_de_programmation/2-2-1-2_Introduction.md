# Texte : introduction

  Dans cette partie, nous nous concentrons sur *les langages de programmation et leur [paradigmes]( https://fr.wikipedia.org/wiki/Paradigme_(programmation) )*.


## Introduction

Avant de discuter des *paradigmes des langages de  programmation*, nous rappelons brièvement, pour que vous les ayez sous la main, les concepts de base utilisés pour définir et étudier les langages de programmation.

 Le module 0 est a priori spécifiquement destinée aux enseignants.-->

> La présentation et le plan de ce module ne suivent pas une démarche d'apprentissage de la programmation — c'est l'objet d'autres cours prérequis à celui-ci — ni de description progressive d'un langage de programmation, mais une organisation par thèmes et concepts de la **théorie des langages de programmation**.

## Rappels, définitions de base, et autres notions utilisées

### Algorithme vs programme

L'informatique est la science du traitement automatique de l'information — par «traitement», on entend notamment acquisition, codage, stockage, communication, modification, restitution... de « données » — par une machine. Cette machine peut être une abstraction théorique (une [machine de Turing](https://fr.wikipedia.org/wiki/Machine_de_Turing), un [automate fini](https://fr.wikipedia.org/wiki/Automate_fini), une [architecture de von Neumann](https://fr.wikipedia.org/wiki/Architecture_de_von_Neumann), ...) ou un ordinateur particulier, avec ses caractéristiques matérielles et logicielles.

Ainsi, «programmer» c'est imaginer une séquence d'opérations, dites «élémentaires», codant un traitement souhaité par la machine visée.

Ceci signifie deux choses :

- La conception d'une méthode de traitement, ce qui constitue un « [algorithme](https://fr.wikipedia.org/wiki/Algorithme) ». La discipline informatique correspondante est l'algorithmique ; celle-ci étudie les méthodes efficaces de résolution automatique de problèmes, ainsi que les structures de données, l'organisation de celles-ci adaptée à ce traitement.
- Le codage fidèle et efficace — donc exact, correct — de cet algorithme en fonction du contexte matériel et logiciel particulier d'exécution du programme.

Un [programme](https://fr.wiktionary.org/wiki/programme#:~:text=Fran%C3%A7ais-,%C3%89tymologie,%C2%AB%20ordre%20du%20jour%20%C2%BB) est donc une traduction particulière, concrète d'un algorithme ; il en constitue une instance contextualisée et codée. Celle-ci peut être mise à l'épreuve, testée lors d'une exécution du programme par un ordinateur traitant des données particulières, judicieusement choisies.

### Définition : langage de programmation

Les premiers langages de programmation sont apparus avec les premiers exemples d'assembleurs (1954, 1955, etc.), FORTRAN (en gestation entre 1954 et 1956, mais véritablement opérationnel en 1957), puis COBOL (conçu de 1957 à 1959).

Un programme est un texte, avec ses conventions d'écriture. Il s'agit bien d'un langage écrit, au sens commun, mais il doit toujours avoir un sens univoque et non contextuel, ce qui n'est jamais le cas des langues vernaculaires. Il faut que la formulation textuelle d'un programme soit :
- suffisamment proche d'un code réel, conforme à une famille d'ordinateurs particuliers ;
- standardisée et générale pour permettre une adaptation immédiate et automatique — on parle de « portabilité » — à d'autres contextes similaires ;
- parfaitement univoque, non ambiguë, puisque destinée à un traitement automatique ;
- intelligible par un être humain.



### Familles et niveaux de langages

Vu le grand nombre de langages existants, une classification s'est fait jour. On regroupe en général les langages en « familles » selon le paradigme de programmation auquel ils sont (le mieux) adaptés.

Un paradigme de programmation est un concept assez difficile à définir précisément ; Wikipedia nous dit qu'un [paradigme de programmation](https://fr.wikipedia.org/wiki/Paradigme_(programmation)) est une façon d'approcher la programmation informatique et de traiter les solutions aux problèmes et leur formulation dans un langage de programmation approprié.


Deux grandes familles occupent une place prépondérante dans ce paysage : les langages impératifs, dont sont issus les langages orientés-objet, même s'ils sont étudiés de façon spécifique, et les langages déclaratifs dont font partie les langages fonctionnels que nous aborderons brièvement.

D'autre part, les langages peuvent également être présentés en fonction de leur généalogie. En effet, la majorité d'entre eux ont été créés comme évolution ou variante d'un ou plusieurs langages qui leur préexistaient.

Nous aborderons généalogie des langages et les paradigmes des langages plus loin.
Dans un premier temps, nous regarderons les langages impératifs qui définissent le comportement de base de beaucoup de langages comme  Python, C, C++, Fortran, Cobol, C++, Java, ..., même si certains d'entres-eux incluent dans leur conception le paradigme orienté-objet (c'est le cas de Python, C++, Java).

Pour ces langages, il est généralement admis que l'on travaille sur des modèles d'ordinateurs qui suivent une [architecture de von Neumann](https://fr.wikipedia.org/wiki/Architecture_de_von_Neumann).

#### Architecture de von Neumann

L'architecture de von Neumann décompose l'ordinateur en 4 parties distinctes :

- l'unité arithmétique et logique (UAL ou ALU en anglais) ou unité de traitement : son rôle est d'effectuer les opérations de base ;
- l'unité de contrôle, chargée du « séquençage » des opérations ;
- la mémoire qui contient à la fois les données et le programme qui indiquera à l'unité de contrôle quels sont les calculs à faire sur ces données. La mémoire se divise entre mémoire volatile (programmes et données en cours de fonctionnement) et mémoire permanente (programmes et données de base de la machine) ;
- les dispositifs d'entrée-sortie, qui permettent de communiquer avec le monde extérieur.

