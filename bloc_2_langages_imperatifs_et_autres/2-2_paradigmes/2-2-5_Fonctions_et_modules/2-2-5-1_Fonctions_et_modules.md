# Fonctions et modules

1. Paradigmes des langages de programmation
2. Compilateur et interpréteur
3. Les langages impératifs
4. Typage des langages de programmation
5. **Fonctions et modules**
6. Histoire et taxonomie des langages de programmation

####

* **Thierry Massart**.  Le terme de fonction est utilisé pour désigner toutes les constructions des langages permettant la découpe du code en blocs de code appelables dans différents contextes.

[![Vidéo 1 B2-M2-S5 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B2-M2-S5.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B2-M2-S5.mp4)

#### <a href="https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/raw/master/__supports-de-cours/B2-M2/B2-M2-S5.srt" target="_blank">Sous-titre de la vidéo</a> -  <a href="https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/raw/master/__supports-de-cours/B2-M2/B2-M2-S5-script.md" target="_blank">Transcription de la vidéo </a>

## Texte complémentaire 

<a href="https://mooc-nsi-snt.gitlab.io/portail/3_Les_Fondamentaux/Le_Mooc/bloc_2_langages_imperatifs_et_autres/2-2_paradigmes/2-2-5_Fonctions_et_modules/2-2-5-2_Fonctions%26Modules.html" target="_blank">Texte complément à la video</a>

