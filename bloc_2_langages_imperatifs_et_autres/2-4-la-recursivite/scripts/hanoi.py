def hanoi(n, init, final):
    """
    Résolution du problème de la tour de Hanoi
    Renvoie la chaîne de caractères qui donne la séquence des mouvements
    à opérer pour résoudre le problèmes de la tour de Hanoi avec les paramètres donnés
    :param n: (int) hauteur de la tour
    :param init: (0 ≤ int ≤ 2) placement initial
    :param final: (0 ≤ int ≤ 2) placement final
    :return: (str) séquence des déplacements (un par ligne)
    Hypothèse : initial ≠ final
    """
    res = ''
    if n > 0:
        autre = 3 - init - final
        res += hanoi(n - 1, init, autre)
        res += 'déplacer ' + str(n) + ' de ' + str(init) + ' vers ' + str(final) + '\n'
        res += hanoi(n - 1, autre, final)
    return res

n = int(input('n = '))
print(hanoi(n, 0, 2))
