====================================
Notion de complexité et grand *O*
====================================

.. seealso:: Référence:  livre de `Thomas Cormen, Charles Leiserson, Ronald Rivest, Clifford Stein - Algorithmique`_

.. _Thomas Cormen, Charles Leiserson, Ronald Rivest, Clifford Stein - Algorithmique:

    Thomas Cormen, Charles Leiserson, Ronald Rivest, Clifford Stein - 
    Algorithmique - 3ème édition - Cours avec 957 exercices et 158 problèmes, 
    Dunod,  2010, ISBN: 978-2-10-054526-1
    http://www.books-by-isbn.com/2-10/2100545264-Algorithmique-Cours-avec-957-exercices-et-158-problemes-2-10-054526-4.html


Motivation
===========

Montrer qu'une version d'un programme est plus *efficace* qu'une autre
n'est, a priori, pas aisé. La succession ou l'imbrication des tests
et des boucles et la multitude des possibilités fait qu'il n'est
généralement pas possible ou raisonnable d'évaluer l'efficacité de
l'algorithme pour chaque cas possible.

Dans ce chapitre, nous donnerons un aperçu des techniques et méthodes
d'estimation de l'efficacité d'un algorithme. En particulier nous définirons
la notation *grand O* qui permettra de classer les algorithmes selon
leur type d'efficacité sans devoir détailler le nombre exact d'instructions
exécutées par l'algorithme.

Critères de choix d'un algorithme
----------------------------------

Généralement, il existe plusieurs méthodes pour résoudre un même
problème. Il faut alors en choisir une et concevoir un algorithme pour cette
méthode de telle sorte que cet algorithme satisfasse le mieux possible aux
exigences. 

Parmi les critères de sélection d'une méthode et en conséquence d'un algorithme,
deux critères prédominent: la *simplicité* et l'*efficacité* de cet
algorithme. 

Si parfois ces critères vont de paire, bien souvent ils sont contradictoires;
un algorithme efficace est, en effet, bien souvent compliqué et fait appel à
des méthodes  fort élaborées. Le concepteur doit alors choisir entre la
simplicité et l'efficacité. 

Si l'algorithme devra être mis en oeuvre sur un nombre limité de données qui
ne demanderont qu'un nombre limité de calculs, cet algorithme doit de
préférence être simple. En effet un algorithme simple est plus facile à
concevoir et a moins de chance d'être erroné que ne le sera un algorithme
complexe. 

Si, par contre, un algorithme est fréquemment exécuté pour une masse
importante de données, il doit être le plus efficace possible.

Au lieu de parler de l'efficacité d'un
algorithme, on parlera généralement de la notion opposée, à savoir, de la
complexité d'un algorithme.

La complexité d'un algorithme ou d'un programme peut être mesurée de diverses
façons. Généralement, le temps d'exécution est la mesure principale de
la complexité d'un algorithme. 

D'autres mesures sont possibles dont:

  - la quantité d'espace mémoire occupée par le programme et en
    particulier par ses variables;
  - la quantité d'espace disque nécessaire pour que le
    programme s'exécute;
  - la quantité d'information qui doit être transférée (par lecture ou
    écriture) entre le programme et les disques ou entre le programme et des serveurs externes via un réseau;
  -  ...

Nous n'allons pas considérer de programme qui échange un grand nombre
d'informations avec des disques ou un autre ordinateur. 

En général nous analyserons le temps d'exécution d'un programme pour évaluer
sa complexité. La mesure de l'espace mémoire occupé par les variables sera
un autre critère qui pourra être utilisé ici.

De façon générale, la complexité d'un algorithme pour un ensemble de
ressources donné est une mesure de la quantité de ces ressources
utilisées par cet algorithme.

Le critère que nous utiliserons dans la suite est un nombre d'actions
élémentaires exécutées.  Nous prendrons soin, avant toute chose, de
préciser le type d'actions prises en compte et si nous voulons une complexité
minimale, moyenne ou maximale.

Nous verrons que, généralement, la complexité d'un algorithme est exprimée par
une fonction qui dépend de la taille du problème à résoudre.

Exemple: la recherche du minimum
---------------------------------

Le travail nécessaire pour qu'un programme s'exécute dépend de la 'taille'
du jeu de données fourni. 

Par exemple, si nous analysons la fonction ``indice_min(s)`` donnée au début du chapitre,
(recherche l'indice de l'élément de valeur minimale dans une liste ``s``, il est
intuitivement normal que cet algorithme prenne un temps proportionnel
à ``len(s)``, et soit noté, par exemple,
*T(len(s))*. 

Dénotons *len(s) = n*.  *n* est la taille de la liste et donc par
extension, également la taille du problème.

Redonnons ici la partie traitement de cet algorithme en
ayant eu soin de transformer le *for* en *while* pour mieux
détailler chaque étape d'exécution de l'algorithme et nous supposons ``n = len(s)`` connu dans le programme :

.. code-block:: python

  res = 0                      # 1
  i = 1                        # 2
  while  i< n :                # 3
     if  s[i][0] < s[res][0]:  # 4
        res = i                # 5
     i = i+1                   # 6
  return res                   # 7

Pour obtenir des résultats qui restent valables quel que soit l'ordinateur
utilisé, il faut réaliser des calculs simples donnant une approximation
dans une unité qui soit indépendante du processeur utilisé.
On peut, par exemple, faire l'approximation que chaque assignation de donnée simple ou 
test élémentaire représente une unité de temps d'exécution.

D'autres hypothèses auraient pu être faites.  Par exemple, on aurait pu ne
considérer que les assignations et rechercher la complexité en nombre
d'assignations, ou ne considérer que les tests et rechercher la complexité en
nombres de tests effectués par l'algorithme.  

La complexité dépend donc fortement de l'unité prise qui doit être clairement
précisée.

Avec nos hypothèses, les instructions des lignes 1, 2 et 7
prennent chacune une unité de temps d'exécution. 

La boucle *while* de la ligne 3 à 6 s'exécute *n-1* fois, mais le
test s'effectue *n* fois. La ligne 3 prend *n* unités et la ligne 6,
*n-1* unités.

Le test de la ligne 4 prend une unité; il est effectué *n-1* fois et donc la ligne 4 utilisera au total *n-1* unités.

L'assignation de la ligne 5 prend une unité. Elle n'est effectuée que lorsque le test du ``if`` est vérifié.
Si l'on ne désire pas uniquement faire une seule mesure sur un jeu de
données bien précis, il faut donc expliciter si l'on désire connaître le temps
d'exécution dans *le meilleur des cas*  *T_min(n)*, *le pire des cas* *T_MAX(n))*, ou 
*en moyenne*  *T_moyen(n)*. Ici:

 - *T_min(n)* est donné dans le cas où le test du ``if`` n'est jamais
    vérifié, ce qui signifie que le minimum se trouve à la première
    composante. On a alors: T_min(n) = 1+1+n+(n-1)+(n-1)+1=3n+1

 - *T_MAX(n)* est donné dans le cas où le test du ``if`` est à chaque
    fois vérifié, ce qui correspond au cas où toutes les valeurs de
    ``s`` sont distinctes et triées en ordre décroissant. On a alors:
    T_MAX(n) = 1+1+n+(n-1)+(n-1)+(n-1)+1= 4n

Le calcul de *T_moyen(n)* est généralement beaucoup plus difficile à
effectuer. Pour cela, on peut faire des hypothèses sur les jeux de
données possibles et leur distribution statistique; ces hypothèses
doivent être le plus réaliste possible.  Pour que le calcul de
*T_moyen(n)* ne soit pas trop compliqué, on tolère généralement
certaines hypothèses simplificatrices.  Nous pouvons, par exemple,
supposer que toutes les valeurs dans ``s`` sont distinctes et faire
l'hypothèse que la distribution des valeurs est uniforme, la liste
étant non triée a priori.

Pour faire ce calcul, séparons les actions dues au ``while``, au
``if`` et à l'assignation finale, des actions d'assignation de la
variable ``res`` et posons:

*C(n)* = le nombre
moyen d'assignations à ``res`` pour une liste de taille *n*

Si nous posons *R(n)* = le nombre d'actions dues au ``while``, au
``if`` et à l'assignation finale, ce nombre est connu et vaut: 
*R(n)* = 3n.

Nous avons: T_moyen(n) = C(n) + R(n) 

Pour exprimer la valeur de *C(n)*, regardons d'abord sa valeur pour *n* valant
*1,2, ...* 

Pour *n* valant 1, *C(1)=1* puisqu'il y a  une assignation en début, et que
le corps de la boucle ``while`` ne s'exécute jamais. 

Pour *n* valant 2, la liste contient une valeur maximale *Max* et une valeur
minimale *min*. Deux possibilités équiprobables peuvent se présenter:

1. ``s`` =  min  suivi de Max
2. ``s`` =  Max  suivi de min

Le cas 1. donne une assignation et le cas 2. deux assignations, soit
en moyenne: C(2) = 3/2

Essayons d'avoir une expression générale pour *C(n)*: *s[0]* a une
chance sur *n* d'être le minimum, une chance sur *n* d'être le
deuxième minimum, ....  Donc *s[0]* a *1/n* chance d'être le ième
minimum et cela pour tout *i* entre 1 et *n*.

Si nous posons *C(0) = 0* et si dans la sous-liste qu'il reste à
traiter il y a *j* éléments plus petits que le minimum provisoire, le
nombre d'assignations qu'il faudra encore effectuer vaudra, d'après la
définition donnée, *C(j)*, car les nombres plus grands que le minimum
provisoire n'impliqueront aucune assignation supplémentaire. Ce
raisonnement est valable pour tout *i >= 1*.

En conséquence:

.. image:: images/eq81.png
   :align: center

le 1 étant dû à la première assignation (c'est-à-dire à l'instruction 1). 

Pour avoir une idée précise de la valeur de *C(n)*, il faut éliminer les *C(j)*
du membre de droite. 

Pour cela exprimons cette équation  d'une autre façon en le multipliant
par *i*

.. image:: images/eq82.png
   :align: center

et de même si on multiplie *C(i+1)* par *i+1*:

.. image:: images/eq83.png
   :align: center

En soustrayant la seconde équation  de la troisième  nous obtenons:

.. image:: images/eq84.png
   :align: center

Ce qui peut s'écrire:

.. image:: images/eq85.png
   :align: center

Pour *C(1)* la formule reste vrai *(C(1)=1)* 

Et donc:

.. image:: images/eq86.png
   :align: center

Notons que nous aurions pu déduire ce résultat plus rapidement en exprimant
que *C(i+1)* vaut le nombre moyen d'assignations dû à la recherche du
minimum pour les *i* premiers nombres, c'est-à-dire *C(i)*, plus la probabilité
que le nombre *s[i]* soit le minimum, c'est-à-dire *1/i+1*,
ce qui nous redonne l'équation précédente. 

Pour effectuer une approximation de *C(n)*, évaluons les aires *S1* et *S2*
données en grisé dans la figure suivante où la fonction *f(x) = 1/x*.

.. image:: images/fig81.png
   :align: center

Ayant 

.. image:: images/eq86a.png
   :align: center

on voit que

.. image:: images/eq86b.png
   :align: center

Comme

.. image:: images/eq86c.png
   :align: center

et ceci pour tout *n >= 1*. 

On a donc:

.. image:: images/eq86d.png
   :align: center

De ce fait:

.. image:: images/eq86e.png
   :align: center
 
Et finalement, comme *ln(n) < ln(n+1)*, on obtient:

.. image:: images/eq87.png
   :align: center
 
 
Cette dernière équation permet de borner *T_moyen(n)* (rappelons-nous que
*T_moyen(n) = C(n) + R(n)* avec *R(n) = 3n*) 

Nous obtenons donc:

.. image:: images/eq88.png
   :align: center
 
Autre exemple: la recherche d'un élément
------------------------------------------

Calculons les *T_min(n)*, *T_Max(n)* et *T_moyen(n)* de l'algorithme de
recherche d'un élément dans une liste ``s``.

Redonnons ici la partie traitement de cet algorithme en supposant que ``n = len(s)``:

.. code-block:: python

   i = 0                                  #1
   while i < n and s[i][0] != x:          #2
      i = i+1                             #3
   if i == n:                             #4
      i = -1 # pas trouvé                 #5
   return i                               #6

*T_min(n)* est donné lorsque l'on trouve directement l'élément. 

Si l'on suppose ici encore que toute assignation et tout test élémentaire
prend une unité de temps et que le test du while est composé de deux tests
élémentaires (le temps pour le and étant omis), on obtient: 
*T_min(n) = 5  (c'est-à-dire 1 en #1 + 2  en #2  + 1 en #4 + 1 en #6)*

*T_MAX(n)* est donné dans le cas où l'élément ``x`` n'est pas dans ``s``:
T_MAX(n) = 3n+5

*T_moyen(n)* dépend de la probabilité *p* que ``x`` soit dans ``s``. 

En supposant que si ``x`` est dans ``s``, il a la même probabilité d'être dans
n'importe quelle composante *s[i]* (pour *0 <= i < n*), en énumérant les cas
possibles on obtient:

=================================  ================
Cas possibles                      Nombre d'unités
=================================  ================
 *1*: on trouve ``x`` en *s[0]*      *3+2* 
 *2*: on trouve ``x`` en *s[1]*      *6+2* 
 ...
 *n*: on trouve ``x`` en *s[n-1]*    *3n+2* 
=================================  ================

Si ``x`` est dans ``s``:

.. image:: images/rech-mu.png
   :align: center
 
Si ``x`` n'est pas dans ``s``:

*min = moyen = MAX = 3n+5*

Finalement 

.. image:: images/rech-mu2.png
   :align: center
 
Le grand *O*
================

Les exemples précédents montrent qu'il est souvent très difficile de
calculer le temps moyen (ou maximal) d'exécution d'un algorithme.  Ces calculs
précis sont, de plus, inutiles puisqu'ils se basent sur des
approximations grossières du temps d'exécution des actions
élémentaires; évaluations qui ne tiennent pas non plus compte, par
exemple, de l'ordinateur et du compilateur ou de l'interpréteur utilisés.

Une évaluation du temps d'exécution d'un algorithme
ne devient généralement intéressante que lorsque la taille du jeu de
données, c'est-à-dire de *n* dans nos exemples précédents, est grande. En effet
pour *n* petit, les algorithmes s'exécutent généralement très rapidement.

Supposons que la complexité maximale d'un algorithme *A = 100.n*, que
la complexité maximale d'un algorithme *B* = 15.n^2 , celle de *C =
n^4* et celle de *D = 2^n*.

La table suivante donne les valeurs de complexité pour *n =
1,10,100,1000, 10^6* et *10^9*; les figures qui suivent donnent les
valeurs des fonctions respectivement pour n variant entre 0 et 12, et
0 et 18.


.. image:: images/fig82.png
   :align: center
 
.. centered:: Valeur de *100.n*, *15.n^2*, *n^4* et *2^n* pour différentes valeurs de *n*


.. image:: images/epsf12.png
   :align: center

.. centered:: Valeur des fonctions A = --, B = - -, C = -.- et D = ... pour *n* allant de 0 à 12


.. image:: images/epsf18.png
   :align: center

.. centered:: Valeur des fonctions A = --, B = - -, C = -.- et D = ... pour *n* allant de 0 à 18

Ces valeurs doivent être divisées par le nombre d'instructions
élémentaires par seconde pour obtenir un temps exprimé en seconde.

Les micro ou mini ordinateurs actuels permettent d'exécuter de l'ordre
de *10^9* instructions par seconde soit environ *10^14* instructions
par jour.

Cela signifie qu'en un jour, si aucune autre limitation ne perturbe
l'exécution des algorithmes (telle que l'espace
mémoire disponible par exemple), on peut résoudre avec l'algorithme

  -  *A* , un problème pour *n*  valant approximativement *10^12* 
  -  *B* , un problème pour *n*  valant approximativement *10^7* 
  -  *C* , un problème pour *n*  valant approximativement *3000*
  -  *D* , un problème pour *n*  valant approximativement *50*

et donc *A* est meilleur que *B*, *B* est meilleur que *C* et *C* est
meilleur que *D* (sauf pour de petites valeurs de *n*).

En vertu des remarques précédentes et de ces chiffres, on préfère donner
un ordre de grandeur permettant d'avoir une idée du type d'algorithme. 
Pour cela, on utilise la notation grand *O* définie ci-dessous, qui permet:

  - de déterminer si un algorithme a une chance de s'exécuter pour un
    *n* donné,
  - connaissant le temps d'exécution pour un *n* donné, de faire une
    approximation de ce temps pour une autre valeur de *n*.

Définition
-----------

La notation grand *O* est définie comme suit:

.. centered::   Définition (*O*) : Une complexité *T(n)* est dite en grand *O*  de *f(n)* (en *O(f(n))* s'il existe un entier *N* et une constante *c > 0* tels que pour tout entier *n > N* nous avons *T(n) <=  c.f(n)*

Cette définition permet d'effectuer de nombreuses simplifications dans les
calculs. En effet de cette définition il résulte que:

  * Les facteurs constants ne sont pas importants. En effet, si par
    exemple *T(n)=n* ou si *T(n)=100n*, *T(n)* est toujours en *O(n)*
  * Les termes d'ordres inférieurs sont négligeables. Ainsi si *T(n) =
    n^3 + 4n^2 + 20n + 100*, on peut voir que pour *n > N = 5*

    -  *n^3  >  4n^2* 
    -  *n^3  >  20n* 
    -  *n^3  >  100*

et donc pour *n>5*, *T(n) = n^3 + 4n^2 + 20.n + 100 < 4n^3*. 

De ce fait *T(n)* est en *O(n^3)*.

Calcul du grand *O*
-----------------------

Classes de complexité
.....................

Si on peut calculer qu'un algorithme est un *O(n^2 )*, il n'est pas
intéressant, même si cela est trivialement vrai, d'exprimer le fait que *T(n)*
est en *O(n^3 )*, *O(n^4 )*, .... 

Lorsque l'on évalue le grand *O* d'une complexité, c'est la meilleure
estimation "simple" que l'on cherche à obtenir. 

Ainsi on classe généralement les complexités d'algorithmes selon leur grand
*O* comme donné par la table suivante :

=============   ====================
*O*             Classe d'algorithmes
=============   ====================
*O(1)*          constant 
*O(log n)*      logarithmique 
*O(n)*          linéaire 
*O(n log n)*    *n log n* 
*O(n^2 )*       quadratique 
*O(n^3 )*       cubique 
*O(2^n )*       exponentiel en base 2 
*O(3^n )*       exponentiel en base 3
 \.\.\.          
*O(n^n )*       exponentiel en base n 
 \.\.\.          
=============   ====================

**Classes de complexité**

Dans la suite, nous essayerons toujours de donner une approximation de la
complexité des algorithmes vus. 

Remarquons qu'en vertu du fait que *pour tout a,b* positif : log_a n = (log_a b).(log_b n)
 
et comme *log_a b* est une constante pour *a* et *b* fixés, si *T(n)* est en
*O(log_a n)* il est également en *O(log_b n)*. (la base
du logarithme n'a donc pas d'importance pour exprimer un grand *O*).  Notons
encore que ceci n'est pas vrai pour la base des complexités exponentielles.

Donnons ici un petit nombre de règles permettant d'évaluer la
complexité d'un algorithme.  Ces règles ne donnent en général qu'une
surapproximation parfois grossière; une estimation plus précise devant
tenir compte du fonctionnement de l'algorithme.

Règles de calcul du grand O
..............................

Règle sur l'unité : 

=======================================================================

.. admonition:: Règle 1 (unité)

 Il faut clairement définir l'unité utilisée et les éléments pris en
 compte.  Ainsi si l'on tient compte des tests élémentaires, des
 assignations, des lectures de données et des écritures de résultats,
 une assignation à une variable simple, une instruction input ou print
 d'une donnée simple, ou l'évaluation d'une expression (ou d'une
 condition) simple ne donnant pas lieu à l'exécution de fonctions,
 prend un temps constant fixé et est donc en *O(1)*.  L'assignation à
 des objets plus complexes ou des input ou print de données plus
 complexes peut ne plus être en *O(1)* en fonction de leur taille qui
 peut dépendre de la taille du problème (*n*).

=======================================================================

Règle de la séquence : 

=======================================================================

.. admonition:: Règle 2 (séquence)

  Si un traitement 1 prend un temps *T_1(n)* qui est en O(f_1(n)) et
  un traitement 2 prend un temps *T_2(n)* qui est en *O(f_2(n))* alors
  le traitement 1 suivi du traitement 2 prend *T_1(n) + T_2(n)* et
  est en
  
  *O(f_1(n) + f_2(n)) = O (max(f_1(n) , f_2(n)))*
  
  où max prend la fonction qui croît le plus vite c'est-à-dire de plus grand
  ''ordre''.

=======================================================================

Par exemple si *T_1(n)* est en *O(n^2 )* et *T_2(n)* est en *O(n)*,
*T_1(n) + T_2(n)* est en *O(n^2 + n) = O(n^2)*
 
En particulier une séquence d'instructions simples (dont on tient
compte) ne faisant aucun appel à une procédure ou à une fonction ne
dépend pas de *n* et sont donc en *O(1)*.


Règle du if : 

=======================================================================

.. admonition:: Règle 3 (if)

  Pour un ``if condition: instruction_1 else: instruction_2`` 

  où 

  - ``instruction_1`` est en *O(f_1(n))* 
  - ``instruction_2`` est en *O(f_2(n))* 
  - l'évaluation de la ``condition`` est en *O(g(n))*
  
  suivant le test, le ``if`` sera en *O(max(f_1(n),g(n)))* ou en
  *O(max(f_2(n),g(n)))* et peut être borné par:
  
  *O(max(f_1(n), f_2(n), g(n)))* 

=======================================================================

Notons que souvent *g(n)* est en *O(1)*.

Cette règle peut être généralisée pour une instruction ``if`` ayant une
ou des parties ``elsif``.

Règle du while : 

=======================================================================

.. admonition:: Règle 4 (while)

  Pour un ``while``, sachant que le corps de la boucle est en
  *O(f_1(n))* et que l'évaluation de la condition est en *O(f_2(n))*,
  si on a une fonction en *O(g(n))* qui donne une borne supérieure du
  nombre de fois que le corps sera exécuté, alors le ``while`` est en
  *O(f(n).g(n))* avec *f(n) = max(f_1(n),f_2(n))*.

=======================================================================

Règle du for : 

=======================================================================

.. admonition:: Règle 5 (for)

  Pour une boucle ``for``, il suffit de ''traduire'' le  ``for`` en ``while``.

=======================================================================

Par exemple :

.. code-block:: python
   
    for i in range(n):
       print(i)

se traduit en:

.. code-block:: python
   
    i=0
    while i < n: 
       print(i)
       i=i+1

La fonction iter() appliquée sur une séquence ou un range, permet de pouvoir ensuite utiliser la fonction next() sur l'objet produit.

Par exemple :

.. code-block:: python
   
  st = "bonjour"  
  for i in st:
       print(i)

se traduit en:

.. code-block:: python
   
    st = "bonjour"
    i = iter(st)
    s=next(i)
    while s != None:
       print(s)
       s=next(i)


Donnons de simples exemples de complexité avec des for où ici aussi *n* est la taille du problème :

.. code-block:: python
 
  for i in range(10):
    traitement 

est en *O(10.f(n))* où *O(f(n))* est la complexité d'une exécution du
``traitement`` et donc le code complet est également en *O(f(n))*

.. code-block:: python
 
  for i in range(n):
    traitement 

est en *O(n.f(n))* où *O(f(n))* est la complexité d'une exécution du
``traitement``.

Ainsi le produit de matrice (*M.L* par *L.N*) est en *O(M.L.N)*.



Règle de la fonction : 

=======================================================================

.. admonition:: Règle 6 (fonction)

  L'appel à une fonction est en *O(f(n))* correspondant à la
  complexité du traitement de cette fonction pour les paramètres
  effectifs donnés.

=======================================================================

On peut raffiner ces calculs, selon que l'on essaye de trouver une
complexité minimale, moyenne ou maximale.  Ce raffinement se situera
dans le nombre de fois qu'une instruction sera répétée, ayant par
exemple certaines hypothèses sur la probabilité que certaines
conditions de ``if`` ou de boucles sont vérifiées.

Application des règles de calcul
================================

Ayant ces règles d'évaluation du grand *O* pour chaque type d'instruction, le
calcul de la complexité d'un algorithme se fait en partant des complexités des
instructions simples, et en calculant, de proche en proche, les complexités des
instructions non simples à partir des résultats déjà calculés. Ces calculs
procèdent en quelque sorte par des regroupements en suivant la structure de
l'algorithme.

Complexité de la recherche du minimum
--------------------------------------------------

Prenons l'exemple de la recherche du minimum c'est-à-dire du
traitement suivant (après traduction en utilisant un ``while`` en ayant ``n = len(s)``:


.. code-block:: python

   res = 0                    #1
   i = 1                      #2
   while i < n     :          #3  
      if s[i][0] < s[res][0]: #4   
         res = i              #5
      i = i+1                 #6
   return res                 #7

Nous supposons que chaque instruction simple est prise en compte pour
le calcul de la complexité.

Le traitement, comme tout algorithme, peut être décomposé sous forme
d'*arbre (informatique)* comme donné par la figure suivante où chaque
*noeud* *représente* une instruction *Python* simple ou composée.

.. image:: images/rechmin.png
   :align: center


.. centered:: Décomposition d'un traitement en suivant la structure

Les noeuds sans *fils*, appelés les *feuilles* de l'arbre,
correspondant aux instructions 1,
2, 5, 6 et 7, représentent des instructions d'assignations simples ou
``return`` qui, d'après la règle 1, sont en *O(1)*.

De ce fait, d'après la règle 3, le noeud 4-5 correspondant à
l'instruction ``if`` est en *O(1)*.

D'après la règle 2, la complexité de la séquence d'instructions 4-6
est en *O(1)*.

On peut maintenant évaluer la complexité du ``while`` (noeud 3-6),
il s'exécute *n-1* fois et la complexité du corps de la boucle est,
comme on vient de le voir, en *O(1)*. Donc d'après la règle 4, la
complexité du ``while`` est en *O(n)*.

Finalement, d'après la règle 2, la complexité de tout le traitement est en
*O(1+1+n+1)* qui peut être simplifié par *O(n)*.

.. Note::

  - Si, par exemple, un algorithme *A* est en *O(n)* et un algorithme
    *B* est en *O(n^2 )*, *A* est 'meilleur', en terme, de complexité
    que *B*.   
   
    Par contre si *A* et *B* sont tous les deux en *O(f(n))*, il faut
    détailler le calcul de la complexité pour déterminer lequel est
    plus complexe que l'autre.

  - D'après la définition du *O*, l'algorithme de recherche du
    minimum est en *O(n)* et donc trivialement également en *O(n^2)*,
    *O(n^3)*, ..., *O(n^n)*.  Lorsque l'on cherche la complexité d'un
    algorithme, on essaye bien évidemment de donner la valeur la plus
    précise (Exemple: *O(n)* pour la recherche du minimum)

  - Rappelons que l'on peut évaluer plusieurs complexités
    différentes.  Par exemple, pour les algorithmes de tris de liste,
    on peut regarder la complexité en terme de nombre d'instructions ou
    seulement regarder les instructions qui effectuent des déplacements
    d'éléments (le déplacement d'un élément peut prendre beaucoup plus
    de temps que les autres instructions).  Certains tris sont en
    *O(n^2)* si l'on regarde toutes les instructions, mais en *O(n)* en
    terme de déplacement d'informations.

Complexité de la recherche séquentielle et dichotomique
--------------------------------------------------------

Il est facile de voir que l'algorithme de **recherche séquentielle** est
également en *O(n)* où *n* est la longueur de la liste et en supposant
que les éléments sont testés en *O(1)*.

Pour calculer la complexité de la **recherche dichotomique**, en
utilisant les règles vues précédemment, on peut assez rapidement
observer que la complexité moyenne ou maximale en nombre d'actions
élémentaires de cet algorithme est en *O(f(n))* où *f(n)* est le
nombre respectivement moyen *(f_moyen(n))* ou maximum *(f_max(n))*
de fois que la boucle ``while`` est exécutée.

Pour évaluer *f_max(n)*, commençons par analyser le comportement de
l'algorithme. Nous pouvons tout d'abord remarquer qu'à chaque tour de boucle,
au pire des cas, l'intervalle de recherche est divisé par deux, quelle que soit
la valeur de ``x``. 

La figure suivante donne une évolution possible de l'intervalle de
recherche dans ``s`` pour une liste à 100 éléments en supposant que ``x`` n'est
pas dans ``s`` mais que ``s[40] < x < s[41]``

.. image:: images/fdicho.png
   :align: center

.. centered:: Evolution de l'intervalle de recherche lors d'une recherche dichotomique

Le ``while`` s'exécute donc au maximum 7 fois. De façon générale on peut
voir que:

.. image:: images/eqdicho.png
   :align: center

et donc que *f_max(n) <= \log_2(n) + 1* 

La recherche dichotomique a donc une complexité maximale en *O(ln n)*.
On peut calculer que la complexité moyenne (en supposant ou non que
``x`` est dans ``s``) est également en *O(ln n)*.  

Complexité du tri par sélection
--------------------------------------------------------

Pour calculer la complexité maximale du tri par sélection,
décomposons l'algorithme sous forme d'arbre en suivant sa structure; ceci est
donné en figure suivante:

.. image:: images/decsel.png
   :align: center

.. centered:: Décomposition du tri par sélection

En supposant que toutes les instructions et les traitements de
passages de paramètres interviennent dans ce calcul, en suivant les
règles pour calculer cette complexité, nous obtenons :

 -  ``1, 5, 8, 11`` et ``13`` , sont en *O(1)*
 -  le ``if`` (``10-11``) est en *O(1)*
 -  le ``for`` (``9-11``) imbriqué est en *O(n-i)* et peut être borné
    par *O(n)*
 -  le ``for`` global (``6-13``) est en *O(n^2)*
 -  la procédure ``tri_selection`` (``1-13``) est en *O(n^2)*


Complexité du tri par insertion
--------------------------------------------------------

En analysant l'algorithme de tri par insertion, on peut voir que sa
complexité maximale est également en *O(n^2)*.  On peut voir que la
complexité minimale est rencontrée dans le cas où la liste est déjà
triée et vaut *O(n)*


Complexité du tri Bulle
--------------------------------------------------------

Ici aussi la complexité maximale est en *O(n^2)*.  La complexité
minimale est également en *O(n^2)*.

Complexité du tri par énumération
--------------------------------------------------------

Si *m < n*, la complexité moyenne et maximale de ce tri est en *O(n)* ce qui
est remarquable.

Quelques exemples intéressants
-------------------------------

Les exemples suivants viennent d'interrogation ou d'examens précédents où l'on considère que m,l,n sont des paramètres entiers positifs.

 .. code-block:: python

   i = 1
   while i < n**3 :
      i=i*2

 .. code-block:: python

   for i in range(n):
     for j in range(m):
        for k in range(l):
            print('hello')

 .. code-block:: python

   i = 2
   while i < n:
      i=i*i


Complexité des méthodes de manipulation de séquences
====================================================

Voir le site wiki python pour plus de détails:  

http://wiki.python.org/moin/TimeComplexity

On suppose que *n* est le nombre d'éléments dans la liste et *k* la
valeur (ex: longueur) du paramètre.

La complexité moyenne suppose que les paramètres sont générés
uniformément de façon aléatoire.


Complexité des opérations sur les listes
----------------------------------------

De façon interne, une liste est représentée sous forme de vecteur où
les composantes sont contiguës en mémoire et avec, en moyenne, de la
place libre pour allonger la liste.  La pire opération est donc une
insertion ou suppression en début de liste car toutes les composantes
doivent bouger.  Malheureusement, au pire des cas, un simple
``s.append(x)`` peut demander de recopier toute la liste s'il n'y a
plus de place libre juste après la liste ``s``.


=============== ==================  ====================
Opération       Complexité moyenne  Complexité maximale
=============== ==================  ====================
Copy            O(n)                O(n)
Append          O(1)                O(n)
Insert          O(n)                O(n) 
Get Item        O(1)                O(1) 
Set Item        O(1)                O(1)
Delete Item     O(n)                O(n)
Iteration       O(n)                O(n) 
Get Slice       O(k)                O(k) 
Del Slice       O(n)                O(n)
Set Slice       O(k+n)              O(n+k) 
Extend          O(k)                O(n+k)
Sort            O(n log n)          O(n log n)
Multiply        O(nk)               O(nk) 
x in s          O(n)                O(n)
min(s), max(s)  O(n)                O(n)
len(s)          O(1)                O(1) 
=============== ==================  ====================

Complexité des opérations sur les ensembles
-----------------------------------------------

L'implémentation des ensembles est similaire à celle des dictionnaires.

================== ====================  =======================
Opération          Complexité moyenne    Complexité maximale
================== ====================  =======================
x in s             O(1)                  O(n)        
s | t              O(len(s)+len(t))      O(len(s) * len(t))        
s & t              O(max(len(s),len(t))  O(len(s) * len(t))        
s - t              O(max(len(s),len(t))  O(len(s) * len(t))        
s ^ t              O(max(len(s),len(t))  O(len(s) * len(t))        
================== ====================  =======================

Complexité des opérations sur les dictionnaires
------------------------------------------------

Avec l'interpréteur que nous utilisons,  un dictionnaire Python est stocké dans une table (zone mémoire contiguë).
Pour simplifier supposant que c'est une table à ``MAX`` composantes (par exemple pour un petit dictionnaire ``MAX = 100``.

L'accès à une composante du dictionnaire (Get Item) se fait par hashing (``hash(cle) % 100``) qui calcule un indice entre ``0`` et ``MAX-1``.
En moyenne cette indice correspond à l'emplacement dans la table où l'élément (clé et valeur) va être stocké.
Cette opération (hashing, modulo et accès à la table) prend un temps constant ``O(1)``.

La complexité moyenne suppose que les collisions sont rares avec la fonction de hashage (ce qui est généralement la cas).

Le cas moyen suppose que les paramètres sont choisis uniformément aléatoirement parmi les clés possibles.

Malheureusement, il peut y avoir des collisions.  Au pire des cas,
pour un dictionnaire de ``n``éléments, l'accès à un élément aura
``n-1`` collisions plus le dernier hashing et prend un temps (complexité maximale) en
``O(n)``.

En terme de complexité, placer un élément (Set Item) revient à y accéder suivit du stockage de l'élément qui est en ``O(1)`` en moyenne et ``O(n)`` en complexité maximal, soit la même complexité que l'accès à un élément.

================ ====================  ====================
Opération        Complexité moyenne    Complexité maximale
================ ====================  ====================
Copy             O(n)                  O(n)         
Get Item         O(1)                  O(n)         
Set Item         O(1)                  O(n)         
Delete Item      O(1)                  O(n)         
Iteration        O(n)                  O(n)         
================ ====================  ====================

