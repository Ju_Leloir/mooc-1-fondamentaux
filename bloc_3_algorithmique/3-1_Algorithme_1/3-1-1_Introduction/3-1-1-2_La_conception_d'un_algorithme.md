## La conception d'un algorithme 2/4

1. Algorithme, vie courante, première approche 1/4
2. **La conception d'un algorithme 2/4**
3. L'expression d'un algorithme 3/4
4. Algorithmique positionnement et références 4/4

- exemple du crêpier
- formalisation "informatique", preuve, complexité


[![Vidéo 2a B3-M1-S1 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B3-M1-S1-Video2a.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B3-M1-S1-Video2a.mp4)

#### <a href="https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/raw/master/__supports-de-cours/B3-M1/B3-M1-S1/B3-M1-S1-video2a.srt" target="_blank">Sous-titre de la vidéo</a>

[![Vidéo 2b B3-M1-S1 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B3-M1-S1-Video2b.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B3-M1-S1-Video2b.mp4)

#### <a href="https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/raw/master/__supports-de-cours/B3-M1/B3-M1-S1/B3-M1-S1-video2b.srt" target="_blank">Sous-titre de la vidéo</a>

[![Vidéo 2c B3-M1-S1 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B3-M1-S1-Video2c.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B3-M1-S1-Video2c.mp4)

#### <a href="https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/raw/master/__supports-de-cours/B3-M1/B3-M1-S1/B3-M1-S1-video2c.srt" target="_blank">Sous-titre de la vidéo</a>

## Supports de présentation (diapos)

[Supports de la présentation](https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/raw/master/__supports-de-cours/B3-M1/B3-M1-S1/B3-M1-S1-video2c-handout.pdf)
