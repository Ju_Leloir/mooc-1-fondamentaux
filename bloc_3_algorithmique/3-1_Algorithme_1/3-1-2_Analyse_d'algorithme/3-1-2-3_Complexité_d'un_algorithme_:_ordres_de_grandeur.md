## Complexité d'un algorithme : ordres de grandeur

1. Analyse d'algorithme
2. Complexité d'un algorithme : calcul de la complexité
3. **Complexité d'un algorithme : ordres de grandeur**
4. Preuve d'algorithme

[![Vidéo 3 part 1 B3-M1-S2 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B3-M1-S2-video1e.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B3-M1-S2-video1e.mp4)

#### <a href="https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/raw/master/__supports-de-cours/B3-M1/B3-M1-S2/B3-M1-S2-video3a.srt" target="_blank">Sous-titre de la vidéo</a>

[![Vidéo 3 part 2 B3-M1-S2 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B3-M1-S2-video1f.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B3-M1-S2-video1f.mp4)

#### <a href="https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/raw/master/__supports-de-cours/B3-M1/B3-M1-S2/B3-M1-S2-video3b.srt" target="_blank">Sous-titre de la vidéo</a>

[![Vidéo 3 part 3 B3-M1-S2 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B3-M1-S2-video1g.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B3-M1-S2-video1g.mp4)

#### <a href="https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/raw/master/__supports-de-cours/B3-M1/B3-M1-S2/B3-M1-S2-video3c.srt" target="_blank">Sous-titre de la vidéo</a>

## Supports de présentation (diapos)

[Support de la présentation des vidéos 2 à 7](https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/raw/master/__supports-de-cours/B3-M1/B3-M1-S2/B3-M1-S2-part1.pdf)
