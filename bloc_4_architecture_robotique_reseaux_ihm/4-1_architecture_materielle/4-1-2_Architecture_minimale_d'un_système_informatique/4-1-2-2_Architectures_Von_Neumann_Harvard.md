## Architecture minimale d'un système informatique: Introduction. Du transistor au processeur

La première partie du chapitre, la plus longue, commence au fonctionnement du transistor pour arriver à celui d'un processeur minimaliste.
Ensuite, nous verrons comment écrire des programmes pour ces processeurs avec leur langage : l’assembleur
Enfin, nous présenterons la différence entre les architectures Von Neumann et Harvard et expliquerons comment on passe d'un langage haut niveau comme python ou le langage C au langage assembleur du processeur.

1. Introduction. Du transistor au processeur (3 videos)
2. **Architectures Von Neumann / Harvard**
3. Le langage de la CPU, l’assembleur
4. Du langage haut niveau à l’assembleur


[![Vidéo 4 B4-M1-S2 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B4-M1-S2-V4.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B4-M1-S2-V4.mp4)


#### <a href="https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/raw/master/__supports-de-cours/B4-M1/NSI-B4-M1-S2-script-Archi_Mini.md" target="_blank">Script videos</a>


## Supports de présentation (diapos)

#### <a href="https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/raw/master/__supports-de-cours/B4-M1/NSI-B4-M1-S2-diapos.pdf" target="_blank">Supports de présentation des vidéos</a>
