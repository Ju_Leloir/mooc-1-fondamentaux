# Introduction aux réseaux 2/5: Notions générales sur les réseaux

1. Différents réseaux pour différents usages
2. **Notions générales sur les réseaux**
3. Des transmissions essentiellement numériques
4. Introduction à Ethernet/TCP/IP
5. le modèle OSI

####

- Dans cette vidéo, **Anthony Juton** détaille le **vocabulaire** utilisé pour parler réseaux.

[![Vidéo 2 B4-M3-S1 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B4-M3-S1-Video2.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B4-M3-S1-Video2.mp4)

#### <a href="https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/raw/master/__supports-de-cours/B4-M3/B4-M3-S1-script-videos1-4.md" target="_blank">Script vidéos 1 à 4</a>


## Supports de présentation (diapos)

#### <a href="https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/raw/master/__supports-de-cours/B4-M3/B4-M3-S1_IntroductionAuxReseaux-v2.pdf" target="_blank">Supports de présentation des vidéos 1 à 4</a>
