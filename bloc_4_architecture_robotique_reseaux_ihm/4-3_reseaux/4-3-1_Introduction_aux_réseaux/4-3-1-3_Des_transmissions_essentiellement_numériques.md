# Introduction aux réseaux 3/5: Des transmissions essentiellement numériques

1. Différents réseaux pour différents usages
2. Notions générales sur les réseaux
3. **Des transmissions essentiellement numériques**
4. Introduction à Ethernet/TCP/IP
5. le modèle OSI

####

- Dans cette vidéo, **Anthony Juton** nous parle des réseaux informatiques en comparant les réseaux numériques aux réseaux analogiques qu'ils tendent à remplacer.

[![Vidéo 3 B4-M3-S1 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B4-M3-S1-Video3.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B4-M3-S1-Video3.mp4)

#### <a href="https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/raw/master/__supports-de-cours/B4-M3/B4-M3-S1-script-videos1-4.md" target="_blank">Script vidéos 1 à 4</a>


## Supports de présentation (diapos)

#### <a href="https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/raw/master/__supports-de-cours/B4-M3/B4-M3-S1_IntroductionAuxReseaux-v2.pdf" target="_blank">Supports de présentation des vidéos 1 à 4</a>

