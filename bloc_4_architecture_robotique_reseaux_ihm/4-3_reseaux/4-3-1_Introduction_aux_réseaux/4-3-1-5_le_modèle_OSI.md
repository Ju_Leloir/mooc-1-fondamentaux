# Introduction aux réseaux 5/5: le modèle OSI

1. Différents réseaux pour différents usages
2. Notions générales sur les réseaux
3. Des transmissions essentiellement numériques
4. Introduction à Ethernet/TCP/IP
5. **le modèle OSI**

####

- Dans cette vidéo, **Anne Josiane Kouam** nous présente le **modèle OSI (Open Systems Interconnection)** qui tend à **normaliser** les communications de tous les systèmes informatiques comme les PC, les tablettes, smartwatch, ...

[![Vidéo 5 B4-M3-S1 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B4-M3-S1-video5_Modele_OSI.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B4-M3-S1-video5.mp4)

#### <a href="https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/raw/master/__supports-de-cours/B4-M3/B4-M3-S1-script-video5.md" target="_blank">Transcription vidéo 5</a>

## Supports de présentation (diapos)

#### <a href="https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/raw/master/__supports-de-cours/B4-M3/B4-M3-S1-video5-Modele_OSI.pdf" target="_blank">Supports de présentation des vidéo 5</a>

