# Couches physiques, Ethernet 2/3: Le rôle de la couche physique

1. La couche physique
2. **Le protocole Ethernet**
3. Relier des hôtes : le hub et le switch

####

- Dans cette video, **Anne Josiane Kouam** nous présente  plus amplement les notions de trame du protocole Ethernet, ce qu'est une adresse MAC et enfin l'algorithme de CRC. 


[![Vidéo 2 B4-M3-S2 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B4-M3-S2_video2_protocole_Ethernet.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B4-M3-S2_video2_protocole_Ethernet.mp4)

#### <a href="https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/raw/master/__supports-de-cours/B4-M3/B4-M3-S2-script-video2.md" target="_blank">Transcription vidéo 2</a>

## Supports de présentation (diapos)

#### <a href="https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/raw/master/__supports-de-cours/B4-M3/B4-M3-S2-video2-diapos.pdf" target="_blank">Supports de présentation de la vidéo -2</a>


