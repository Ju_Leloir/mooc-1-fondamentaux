# Routage 5/5: Le routage statique

1. Le routage statique
2. Le protocole NAT
3. Le routage dynamique - Le protocole RIP
4. Le routage dynamique - Le protocole OSPF
5. **Le routage dynamique - Le protocole BGP**

####

- Dans cette vidéo, **Anthony Juton** présente les grandes lignes du protocole de routage externe BGP.

[![Vidéo 3part3 B4-M3-S4 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B4-M3-S4_video3-3_BGP.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B4-M3-S4_video3-3_BGP.mp4)

#### <a href="https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/raw/master/__supports-de-cours/B4-M3/B4-M3-S4-script-videos3-5.md" target="_blank">Script vidéos 3 à 5</a>

## Supports de présentation (diapos)

#### <a href="https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/raw/master/__supports-de-cours/B4-M3/B4-M3-S4_3_RoutageDynamique.pdf" target="_blank">Supports de présentation des vidéos 3 à 5</a>
