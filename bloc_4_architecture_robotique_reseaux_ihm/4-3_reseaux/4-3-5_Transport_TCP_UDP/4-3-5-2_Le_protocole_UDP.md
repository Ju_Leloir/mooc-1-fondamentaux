# Transport TCP/UDP 2/4: Le protocole UDP

1. Présentation de la couche transport
2. **Le protocole UDP**
3. Le protocole TCP
4. La gestion des congestions via TCP


####

- **Anthony Juton** présente en détail le protocole UDP

[![Vidéo 2 B4-M3-S5 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B4-M3-S5_video2_UDP.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B4-M3-S5_video2_UDP.mp4)

## Supports de présentation (diapos)

#### <a href="https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/raw/master/__supports-de-cours/B4-M3/B4-M3-S5_1_TCP_UDP.pdf" target="_blank">Supports de présentation des vidéos 1 à 3</a>